let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.react('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

// mix.browserSync('localhost');
 

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.react('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css')
//     .styles(['resources/assets/css/semantic-ui.css',
//         'resources/assets/css/animate.css'],'public/css/all.css');

mix
.copy('resources/assets/js/lib/ie.js', 'public/js', false)  
.copy('resources/assets/js/lib/bluebird.min.js', 'public/js', false) 
.react('resources/assets/js/app.js', 'public/js')    
.js('resources/assets/js/lib/jquery.min.js', 'public/js/app.js')   
.js('resources/assets/js/lib/bootstrap.bundle.js', 'public/js/app.js') 
.js('resources/assets/js/lib/clean-blog.js', 'public/js/app.js') 
.sass('resources/assets/sass/app.scss', 'public/css/app.css')
.babel('resources/assets/js/lib/es5/audioplayer.js', 'public/js/es5/audioplayer.js') 
.babel('resources/assets/js/lib/esm/audioplayer.js', 'public/js/esm/audioplayer.js') 
.copy('resources/assets/css/audioplayer.css','public/css/audioplayer.css')
.copy( 'resources/assets/images', 'public/images', false );
 