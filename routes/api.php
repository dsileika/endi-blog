<?php 

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: '.env("URL_ALLOW_ORIGIN"));
header('Access-Control-Allow-Credentials: true');




Route::middleware(['cors','ajax','api'])->group(function(){
    

    Route::group(['prefix' => 'password'],function() {
        Route::post('/email', 'Auth\ForgotPasswordController@getResetToken');
        Route::post('/reset', 'Auth\ResetPasswordController@reset');
    });

    Route::group(['prefix'=> 'auth'],function(){
        Route::post('/register','Auth\RegisterController@register');
        Route::post("/login",'Auth\LoginController@login');
        Route::post('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|');
    });

    // Route::group(['prefix'=> 'posts'],function(){
        Route::post('/posts','Api\PostController@index'); 
    // });
    

  

    Route::middleware(['jwt.auth'])->group(function(){
        
        Route::get('/check',function(){
            return "OK";
        }); 
        
        Route::get('/categorys','Api\PostController@getCategorys'); 
        
        Route::middleware(['admin'])->group(function(){
            Route::post('/post/create', 'Api\PostController@createPost');
            Route::post('/post/update', 'Api\PostController@updatePost');
            Route::delete('/post/destroy/{id}','Api\PostController@deletePost');
        });
    });
});
    

