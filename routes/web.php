<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/redirect/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google');

Route::get('/post/{id}', 'RootController@postSEO'); 

// Route::get('{slug}', function() {
 
//     return view('home');
// })
// ->where('slug', '(?!api)([A-z\d-\/_.]+)?'); // Get error 
// ->where('slug', '([A-z\d-\/_.]+)?'); // Not tested
// ->where('slug', '.*');


Route::get('{slug}', function() {
    return view('home');
})
// ->where('slug','!/api/([A-z\d-\/_.]+)');
// ->where('slug','^([0-9A-Za-z\-]+)?api([0-9A-Za-z\-]+)?');
->where('slug','(?!api)([0-9A-Za-z\/-_.]+)?');



