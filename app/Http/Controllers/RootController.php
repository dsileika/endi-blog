<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use App\Models\Posts;

class RootController extends Controller
{
    private $posts;
    public function __construct(Posts $posts){
        $this->posts = $posts;
    } 

    function postSEO($id){
        
       
        if($id>0) { 
        $res = $this->posts
        ->leftJoin('users', 'users.id', '=', 'post.author_id')
        ->leftJoin('category', 'category.id', '=', 'post.category_id')
        ->where('post.id','=',$id)
        ->where('post.isPublic',1)  
        ->select(
            'post.*', 
            'users.name as authors', 
            'category.title as cat',
            'category.bgColor as cat_color',
            'category.id as cat_id'
        )
        ->orderBy('post.updated_at', 'desc')
        ->get()->first();

        if(isset($res) && !empty($res)){
        $meta = [
            'fb:app_id' => '251127288963211', 
            'og:type' => 'article', 
            'og:title' => $res->title,
            'og:url' => env("APP_URL").'/post/'.$id, 
            'og:description' => $res->authors." @ ".$res->cat,
            'twitter:card' => 'summary_large_image',
            'twitter:title' => $res->title,
            'twitter:description' => $res->authors." @ ".$res->cat,
        ];

        if(isset($res->bgImage) && !empty($res->bgImage)) $meta['og:image'] = $res->bgImage; else $meta['og:image'] = env("APP_URL")."/images/forestbridge.jpg";
        if(isset($res->bgImage) && !empty($res->bgImage)) $meta['twitter:image'] = $res->bgImage;  else $meta['twitter:image'] = env("APP_URL")."/images/forestbridge.jpg";
        
        } else {
            $res = [];
            $meta = []; 
        }

        } else { 
            $res = [];
            $meta = [];
        }
        
        return view('home', ['meta' => $meta]);
    }
}
