<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DB;
use App\Models\Posts;
use App\Models\Category;
// use App\User;
// use Tymon\JWTAuth\Exceptions\JWTException;
// use Tymon\JWTAuth\Facades\JWTAuth;


class PostController extends Controller
{
    private $posts;
    public function __construct(Posts $posts, Category $cat){
        $this->posts = $posts;
        $this->cat = $cat;
    } 

    public function index(Request $request){

        // print_r($request->json()->all());
        $data = $request->json()->all();

        $isPublic = (isset($data['isAdmin']) 
                    && !empty($data['isAdmin']) 
                    && $data['isAdmin'] == true) ? [0,1] : [1];
        $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : 0;
        $exceptPost = (isset($data['exceptPost']) && !empty($data['exceptPost']) && $id != $data['exceptPost']) ? $data['exceptPost'] : 0;
        
        // print_r($id);
        if($id>0) 
        return response()->
        json(['status'=>true,
        'data'=> $this->posts
            ->leftJoin('users', 'users.id', '=', 'post.author_id')
            ->leftJoin('category', 'category.id', '=', 'post.category_id')
            ->where('post.id','=',$id)
            ->whereIn('post.isPublic',$isPublic) 
            // ->where('post.id','!=',$exceptPost) 
            ->select(
                'post.*', 
                'users.name as authors', 
                'category.title as cat',
                'category.bgColor as cat_color',
                'category.id as cat_id'
            )
            ->orderBy('post.updated_at', 'desc')
            ->get()->first()
        ]); 
            else 
        return response()->
        json(['status'=>true,
        'data'=> $this->posts
            ->leftJoin('users', 'users.id', '=', 'post.author_id')
            ->leftJoin('category', 'category.id', '=', 'post.category_id')
            ->whereIn('post.isPublic',$isPublic)
            ->where('post.id','!=',$exceptPost) 
            ->select(
                'post.*', 
                'users.name as authors', 
                'category.title as cat',
                'category.bgColor as cat_color',
                'category.id as cat_id'
            )
            ->orderBy('post.updated_at', 'desc')
            ->get()
        ]); 
    }  
    
    // Create new post
    public function createPost(Request $request){ 
        
        $data = $request->json()->all();  

        $post = Posts::where('title','LIKE',$data['title'])->first();

        if(empty($post)){ 
            Posts::create(
                [
                    'author_id' => \Auth::user()->id,
                    'title' => $data['title'],
                    'note' => $data['note'],
                    'bgColor' => $data['bgColor'],
                    'bgImage' => $data['bgImage'],
                    'category_id' => $data['cat_id'],
                    'content' => $data['content'],
                    'isPublic' => $data['isPublic'],
                ] 
            );    
            return response()->json(['status'=>true,'message'=>'Post created successfully']);
        } else {
            return response()->json(['status' => false,'message'=>'Post exists'], 422);
        }
    }

    // Update post
    public function updatePost(Request $request){


       
        // dd(json_decode($request->getContent(), true)); 
        $data = $request->json()->all();  

        if(empty($data['id']) && empty(\Auth::user()->id)) return response()->json(['Missing id'], 422);
        // $filds = (!empty($data['id'])) ? ['id' => $data['id']] : []; 

        $post = Posts::findOrFail($data['id']);
 
        if(!empty($post)){ 
            $post->author_id = \Auth::user()->id;
            $post->title = $data['title'];
            $post->note = $data['note'];
            $post->bgColor = $data['bgColor'];
            $post->bgImage = $data['bgImage'];
            $post->category_id = $data['cat_id'];
            $post->content = $data['content'];
            $post->isPublic = $data['isPublic'];
            $post->save(); 

            return response()->json(['status'=>true,'message'=>'Post updated successfully']);
        } else {
            return response()->json(['status'=>false,'message'=>'Post is not exists']);
        }
    }

    public function deletePost($id){
        $post = Posts::find($id);    
        $post->forceDelete(); 
    }

    public function getCategorys(){
        return response()->
        json(['status'=>true,
        'data'=> $this->cat   
            ->orderBy('title')
            ->get()
        ]); 
    }
}
