<?php

namespace App\Http\Middleware;

use Closure;

class AllowOnlyAjaxRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    // Function to get the client IP address
    private function get_client_ip() {
        $ipaddress = '';
        $ls = '<br/><small>';
        $rs = '</small>';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = $ls = $rs = '';
        return $ls.$ipaddress.$rs;
    }

    private function yt(){
        // <iframe width="70%" height="70%" style="display: none;" id="hi" src="https://ytcropper.com/embed/8z5af4afef7de81/loop/autoplay/" frameborder="0" allowfullscreen></iframe>
            return '
            <iframe width="60%" height="70%" style="display: none;" id="hi" src="https://www.youtube.com/embed/_yFC1pP4GmQ?controls=0&autoplay=1&rel=0&showinfo=0&version=3&loop=1&playlist=7j-F095HyRs" frameborder=0 allowfullscreen ></iframe> 
            <script>
                document.getElementById("hi").style.display = "block";
            </script>
        <noscript><img width="70%" src="https://j.gifs.com/PZEpXy.gif"></noscript>';
    }

    public function handle($request, Closure $next)
    {     
        if(!in_array($request->getClientIp() ?: '0.0.0.0', ['::1','127.0.0.1']))
        if(!$request->ajax() || !$request->wantsJson()) {
            // Handle the non-ajax request
            return response($this->get_client_ip().'( •_•) ( •_•)>⌐■-■ (⌐■_■) #Yeeeaaahhh <br/><br/>'.$this->yt(), 405);
        }

        return $next($request);
    }
}