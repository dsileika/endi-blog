<?php namespace App\Http\Middleware;

use Closure;  
use Illuminate\Routing\Redirector;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

class CheckForMaintenanceMode extends Middleware {

    protected $request;
    protected $app;

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
    }

    public function handle($request, Closure $next)
    {
        if ($this->app->isDownForMaintenance() && 
            // !in_array($this->request->getClientIp(), ['::1','another_IP'])) 
            $this->request->is('/')
        )
        {
            throw new HttpException(503);
        }

        return $next($request);
    }

}