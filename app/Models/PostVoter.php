<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostVoter extends Model
{
    protected $table = 'post_voter';

    protected $hidden = [
        
    ];
}
