<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCommentVoter extends Model
{
    protected $table = 'post_comment_voter';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
