/**
 * Created by Sumit-Yadav on 06-10-2017.
 */
import React from 'react'  
import {NavLink, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Validator } from 'ree-validate'
import ApiService from '../services'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert  } from 'reactstrap';
import Recaptcha from 'react-recaptcha';

// import {
//     Button,
//     Container,
//     Dropdown,
//     Divider,
//     Image,
//     Menu,
//     Responsive,
//     Segment
// } from 'semantic-ui-react';
import * as actions from '../store/actions'
import { Switch, Case, Default } from 'jsx-switch';  

  
const recaptcha_key = "6Ld2S1cUAAAAAMgp0czs5pPvWnjX-jSHCars_WWq";

 class AuthModal extends React.Component {

    constructor(props) {
        super(props);     
        
        
        // this.validator = new Validator({
        //     email: 'required|email',
        //     password: 'required|min:6'
        // });
        // this.authservicer = ((cred) => {return ApiService.login(cred)});
        
        this.state = {
            credentials: {
                email: '',
                password: ''
            },
            responseError: {
                isError: false,
                code: '',
                text: ''
            },
            responseSuccess: {
                isSuccess: false, 
                text: ''
            },
            validator: new Validator({
                email: 'required|email',
                password: 'required|min:6'
            }),
            authservicer: ((cred) => {return ApiService.login(cred)}),
            isLoading: false,
            // errors: validator.errorBag,
            modal: false,
            choice: this.props.choice,
            modalTitle: 'Prisijungimas',
            visible: true,
            dissmisTime: 10000,
            isModal: this.props.isModal,
            isHuman: false
        };
 

        this.toggle = this.toggle.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.onDismiss = this.onDismiss.bind(this); 
        this.updateDimensionse = this.updateDimensions.bind(this);
    }
  

    updateDimensions() { 
        const {modal} = this.state; 
        // if($(window).width() < 991) {
        if(modal){
            this.setState({modal: false});
            this.setModal(this, 1);
        }  
    }

    componentWillMount(){ 
        
        let { choice } = this.state;
        this.updateDimensions();
        this.setModal(choice);
    }
    componentDidMount(){ 
        this._mounted = true;
        window.addEventListener("resize", this.updateDimensionse, false);
        this.setState({
            isLoading: false
        });
    }

    componentWillUnmount(){
        this._mounted = false;
        window.removeEventListener("resize", this.updateDimensionse, false);
    }



    handleLogout(event) {
        event.preventDefault();
        this.props.dispatch(actions.authLogout());
        this.setState({
            modal: false
          });
    }

    toggle() {
        this.setState({
          modal: !this.state.modal,
          responseError: {
            isError: false,
            code: '',
            text: ''
          },
          responseSuccess: {
            isSuccess: false, 
            text: ''
          },
          state: 1
        });
    }

    handleSubmit(event) {
        const { validator, authservicer, isHuman } = this.state;  

        event.preventDefault();
        const {credentials} = this.state;
        // if(isHuman) {
        validator.validateAll(credentials)
            .then(success => {
                if (success) {
                    // this.setState({
                    //     isLoading: true
                    // });
                    this.submit(credentials);
                }
            });
        // } else {
            // alert("Patvirtinkite, kad nesate robotas");
        // }
    }

    submit(credentials) {
        const { authservicer, dissmisTime, isModal } = this.state;  
        let self = this;
        this.props.dispatch(authservicer(credentials)) 
                .then(res => {  
                    if(this._mounted){
                        self.setState({ 
                            responseSuccess: {
                                isSuccess: true, 
                                text: res.message
                            }, 
                            responseError: {
                                isError: false,
                                code: '',
                                text: ''
                            },
                            visible: true
                        });

                        setTimeout( function(){ 
                            self.setState({visible: false});
                        }, dissmisTime );
                        self.setModal(1, res);
                    } 
                })
                .catch(({error, statusCode}) => { 
                    if(this._mounted){
                        self.setState({
                        responseError: {
                            isError: true,
                            code: statusCode,
                            text: error
                        },
                        responseSuccess: {
                            isSuccess: false, 
                            text: ''
                        }, 
                        isLoading: false,
                        visible: true
                    });
                    
                    setTimeout( function(){ 
                        self.setState({visible: false});
                    }, dissmisTime ); 
                }
            })

    }

    setModal(choice = 1, res = {}){ 
        let self = this;
        let modalTitle = "",
        validator = null,
        authservicer = null,
        credentials = null,
        responseSuccess = {
            isSuccess: false, 
            text: ''
        },
        responseError = {
            isError: false,
            code: '',
            text: ''
        };  
 
        switch(choice){
            case 1: 
                modalTitle = "Prisijungimas";
                validator = new Validator({
                    email: 'required|email',
                    password: 'required|min:6'
                }); 
                authservicer = ((cred) => {return ApiService.login(cred)});
                credentials = {
                    email: '',
                    password: ''
                };
                
                // console.log(res.data.message);
                let mes;
                if(res.message)  mes = res.message;
                if(res.data && res.data.message) mes = res.data.message;
                if(res.message || (res.data && res.data.message)) {
                        responseSuccess = {
                        isSuccess: true, 
                        text: mes
                    }
                }
                break;
            case 2: 
                modalTitle = "Registracija";
                validator = new Validator({
                    name: 'required|min:3',
                    email: 'required|email',
                    password: 'required|min:6',
                    password_confirmation: 'required|min:6|confirmed:password'
                }); 
                authservicer = ((cred) => {return ApiService.register(cred)});
                credentials = {
                    name: '',
                    email: '',
                    password: '',
                    password_confirmation: ''
                };
                break;
            case 3: 
                modalTitle = "Naujas slaptažodis";
                validator = new Validator({
                    email: 'required|email',
                }); 
                authservicer = ((cred) => {return ApiService.resetPassword(cred)});
                credentials = {
                    email: '',
                };
                break;
            case 4:  
                modalTitle = "Naujas slaptažodis";
                validator = new Validator({
                    password: 'required|min:6',
                    password_confirmation: 'required|min:6|confirmed:password',
                    token: 'required',
                    email: 'required'
                });
                authservicer = ((cred) => {return ApiService.updatePassword(cred)});
                let { changeParams } = self.props;
                 credentials = {
                        password: '',
                        password_confirmation: '',
                        token: changeParams.match.params.token,
                        email: changeParams.match.params.email.replace("29gnmLTv686QsnV","@")
                        // email: this.props.changeParams.match.params.email
                    }   
                break;
            default: 
                modalTitle = "";
                validator = null;
                authservicer = null;
                credentials = null;
                responseSuccess = {
                    isSuccess: false, 
                    text: ''
                };
                responseError = {
                    isError: false,
                    code: '',
                    text: ''
                };
                break;
        } 
        
        self.setState({
            choice,
            modalTitle,
            validator,
            authservicer,
            credentials,
            responseError,
            responseSuccess,
        });
        /*   Remove this comments when moving to server
        $.post( "/login", function( data ) {
                if(data == 1){
                    window.location.replace("/home");            
                } else {
                     shakeModal(); 
                }
            });
        */
    
    /*   Simulate error message from the server   */ 
    }

    shakeModal(){
        $('#loginModal .modal-dialog').addClass('shake');
                 $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
                 $('input[type="password"]').val('');
                 setTimeout( function(){ 
                    $('#loginModal .modal-dialog').removeClass('shake'); 
        }, 1000 ); 
    }

    onSocialClick(event, data) {
        window.location.assign(`redirect/${event.as}`);
         // this.PopupCenter(`redirect/${event.as}`, "google", 545, 433);
     }

     
    handleChange(event) {
        const { validator, authservicer, credentials } = this.state;  
        const name = event.target.name;
        const value = event.target.value;
 
        credentials[name] = value;
        validator.validate(name, value)
            .then(() => {
                // const {errorBag} = this.validator;
                // this.setState({errors: errorBag, credentials})
            })
    }

    onDismiss() {
        this.setState({ visible: false });
    }

    
    verifyCallback(response) {
        if(response){
            this.setState({isHuman: true});
        } else {
            this.setState({isHuman: false});
        }
    }

    expiredCallback(){
        this.setState({isHuman: false});
    }
  

    render() {

        const { isAuthenticated, isModal } = this.props; 
        const { choice, modalTitle, visible, isLoading } = this.state;   

        if(isModal == false) return (
                <div>

            { isLoading && 
              <div id="loader-wrapper">
                <div id="loader" />
                <div className="loader-section section-left" />
                <div className="loader-section section-right" />
              </div> 
            } 

                
                <h2>{modalTitle}</h2>

                {this.state.responseError.isError && 
                        <div>
                            <Alert color="danger" isOpen={visible}>
                                {this.state.responseError.text}
                            </Alert> 
                        </div> 
                }
                {this.state.responseSuccess.isSuccess &&
                        <div>
                            <Alert color="success" isOpen={visible}>
                                {this.state.responseSuccess.text}
                            </Alert> 
                        </div>
                }
                
                <Switch>

                    <Case expr={choice === 1}>
                    <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                <div className="control-group">
                <div className="form-group floating-label-form-group controls">
                    <label>El. paštas</label>
                    <input type="email" className="form-control" onChange={this.handleChange}  placeholder="El. paštas" name='email' required data-validation-required-message="Please enter your email address." />
                    <p className="help-block text-danger" />
                </div>
                </div>
                <div className="control-group">
                <div className="form-group floating-label-form-group controls">
                    <label>Slaptažodis</label>
                    <input type="password" className="form-control" onChange={this.handleChange} placeholder="Slaptažodis" name='password' required data-validation-required-message="Please enter your name." />
                    <p className="help-block text-danger" />
                </div>
                </div>
                <br />
                <div id="success" />
                <div className="form-group">
                {/* <Link to='/register' replace><button className="btn btn-primary">Registruotis</button></Link> */} 
                <Button type="submit" style={{display: 'none' }} />
                <Link to='#' onClick={this.setModal.bind(this, 3)} replace>Pamiršai slaptažodį?</Link> 
               
                </div>
            </form>
            <div className="form-group">
                <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Prisijungti</Button>{' '} 
                <Button color="secondary" style={{lineHeight: 0.3}} onClick={this.setModal.bind(this, 2)}>Registruotis</Button>
                <hr/>  
                    <Button onClick={this.onSocialClick.bind(this,{as:'google'})}> 
                        <span className="fa-stack fa-lg">
                            {/* <i className="fa fa-circle fa-stack-2x" /> */}
                            <i className="fa fa-google-plus fa-stack-2x fa-inverse" />
                        </span>
                    </Button>
            </div>
                    </Case> 

                    <Case expr={choice === 2}>
                    <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                        <div className="control-group">
                        <div className="form-group floating-label-form-group controls">
                            <label>Vardas</label>
                            <input type="email" className="form-control" onChange={this.handleChange}  placeholder="Vardas" name='name' required  />
                            <p className="help-block text-danger" />
                        </div>
                        </div>
                        <div className="control-group">
                        <div className="form-group floating-label-form-group controls">
                            <label>El. paštas</label>
                            <input type="email" className="form-control" onChange={this.handleChange}  placeholder="El. paštas" name='email' required  />
                            <p className="help-block text-danger" />
                        </div>
                        </div>
                        <div className="control-group">
                        <div className="form-group floating-label-form-group controls">
                            <label>Slaptažodis</label>
                            <input type="password" className="form-control" onChange={this.handleChange} placeholder="Slaptažodis" name='password' required  />
                            <p className="help-block text-danger" />
                        </div>
                        </div>
                        <div className="control-group">
                        <div className="form-group floating-label-form-group controls">
                            <label>Pakartotinas slaptažodis</label>
                            <input type="password" className="form-control" onChange={this.handleChange} placeholder="Pakartotinas slaptažodis" name='password_confirmation' required  />
                            <p className="help-block text-danger" />
                        </div>
                        </div> 
                        <Button type="submit" style={{display: 'none' }} /><br/>
                    </form>
                    <div className="form-group">
                    <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Registruotis</Button>{' '}  
                        <Button color="secondary" style={{lineHeight: 0.3}} onClick={this.setModal.bind(this, 1)}> Prisijungti</Button>
                    </div>

                    </Case>
                    <Case expr={choice === 3}>
                    <span>Įveskite savo el. pašto adresą, kad galėtume jums nusiųsti slaptažodžio pakeitimo nuorodą.</span>
                    <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                        <div className="control-group">
                        <div className="form-group floating-label-form-group controls">
                            <label>El. paštas</label>
                            <input type="email" className="form-control" onChange={this.handleChange} placeholder="El. paštas" name='email' required  />
                        
                            <p className="help-block text-danger" />
                        </div>
                        </div> 
                        <br />
                        <div id="success" />
                       
                        <Button type="submit" style={{display: 'none' }} />
                    </form>
                    <div className="form-group">
                        <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Gauti nuorodą</Button>{' '}  
                        <Button color="secondary" style={{lineHeight: 0.3}} onClick={this.setModal.bind(this, 1)}>Prisijungti</Button>{' '}  
                    </div>

                    </Case>
                    <Case expr={choice === 4}>
                    <span>Užpildykite laukus</span>
                    <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>Naujas slaptažodis</label>
                        <input type="password" className="form-control" onChange={this.handleChange} placeholder="Naujas slaptažodis" name='password' required  />
                        <p className="help-block text-danger" />
                      </div>
                    </div>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>Pakartotinas slaptažodis</label>
                        <input type="password" className="form-control" onChange={this.handleChange} placeholder="Pakartotinas slaptažodis" name='password_confirmation' required  />
                        <p className="help-block text-danger" />
                      </div>
                    </div> 
                        <br />
                        <div id="success" />
                        
                        <Button type="submit" style={{display: 'none' }} />
                    </form>
                    <div className="form-group">
                    <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Pakeisti slaptažodį</Button>{' '}  
                    <Link to="/login"><Button color="secondary" style={{lineHeight: 0.3}}>Prisijungti</Button>{' '}</Link>  
                    </div>
                   

                    </Case>
                    <Default>
                        <Modal onEnter={this.setModal.bind(this, 1)} />
                    </Default>
                </Switch>    

                {/* <Recaptcha
                        sitekey={recaptcha_key}
                        render="explicit"
                        onloadCallback={this.expiredCallback.bind(this)}
                        verifyCallback={this.verifyCallback.bind(this)} 
                        expiredCallback={this.expiredCallback.bind(this)}
                    />      */}
        </div>
        )
        else return (
               <div>

                    {/* Login buttons in header */}
                    {/* <Link to="/login" className="loginBtnResp">
                        <Button  className="btn-outline-secondary-custom" style={{lineHeight: 0.3}}>Prisijungti</Button>{' '}
                    </Link> */}

                    {/* Non respnonsive  */} 
                    {/* <Button color="secondary" className="loginBtn btn-outline-secondary-custom"  style={{lineHeight: 0.3}} onClick={this.toggle} >Prisijungti</Button> */}

                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} onClosed={this.setModal.bind(this, 1)}>
                    <ModalHeader toggle={this.toggle}>{modalTitle}</ModalHeader>
                    <ModalBody> 
                    {this.state.responseError.isError && 
                             <div>
                                <Alert color="danger" isOpen={visible}>
                                    {this.state.responseError.text}
                                </Alert> 
                             </div> 
                    }
                    {this.state.responseSuccess.isSuccess &&
                             <div>
                                <Alert color="success" isOpen={visible}>
                                    {this.state.responseSuccess.text}
                                </Alert> 
                             </div>
                    }
                    
                    <Switch>

                        <Case expr={choice === 1}>
                        <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>El. paštas</label>
                        <input type="email" className="form-control" onChange={this.handleChange}  placeholder="El. paštas" name='email' required data-validation-required-message="Please enter your email address." />
                        <p className="help-block text-danger" />
                      </div>
                    </div>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>Slaptažodis</label>
                        <input type="password" className="form-control" onChange={this.handleChange} placeholder="Slaptažodis" name='password' required data-validation-required-message="Please enter your name." />
                        <p className="help-block text-danger" />
                      </div>
                    </div>
                    <br />
                    <div id="success" />
                    <div className="form-group">
                      {/* <Link to='/register' replace><button className="btn btn-primary">Registruotis</button></Link> */} 
                       <Button type="submit" style={{display: 'none' }} />
                      <Link to='#' onClick={this.setModal.bind(this, 3)} replace>Pamiršai slaptažodį?</Link>
                      <hr/>  
                        <Button onClick={this.onSocialClick.bind(this,{as:'google'})}> 
                             <span className="fa-stack fa-lg">
                {/* <i className="fa fa-circle fa-stack-2x" /> */}
                <i className="fa fa-google-plus fa-stack-2x fa-inverse" />
              </span>
                        </Button>
                    </div>
                  </form>
                        </Case> 

                        <Case expr={choice === 2}>
                        <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group controls">
                                <label>Vardas</label>
                                <input type="email" className="form-control" onChange={this.handleChange}  placeholder="Vardas" name='name' required  />
                                <p className="help-block text-danger" />
                            </div>
                            </div>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group controls">
                                <label>El. paštas</label>
                                <input type="email" className="form-control" onChange={this.handleChange}  placeholder="El. paštas" name='email' required  />
                                <p className="help-block text-danger" />
                            </div>
                            </div>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group controls">
                                <label>Slaptažodis</label>
                                <input type="password" className="form-control" onChange={this.handleChange} placeholder="Slaptažodis" name='password' required  />
                                <p className="help-block text-danger" />
                            </div>
                            </div>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group controls">
                                <label>Pakartotinas slaptažodis</label>
                                <input type="password" className="form-control" onChange={this.handleChange} placeholder="Pakartotinas slaptažodis" name='password_confirmation' required  />
                                <p className="help-block text-danger" />
                            </div>
                            </div> 
                            <Button type="submit" style={{display: 'none' }} />
                        </form>
                        </Case>
                        <Case expr={choice === 3}>
                        <span>Įveskite savo el. pašto adresą, kad galėtume jums nusiųsti slaptažodžio pakeitimo nuorodą.</span>
                        <form name="sentMessage" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group controls">
                                <label>El. paštas</label>
                                <input type="email" className="form-control" onChange={this.handleChange} placeholder="El. paštas" name='email' required  />
                            
                                <p className="help-block text-danger" />
                            </div>
                            </div> 
                            <br />
                            <div id="success" />
                            <div className="form-group">  
                            </div>
                            <Button type="submit" style={{display: 'none' }} />
                        </form>

                        </Case>
                        <Default>
                            <Modal onEnter={this.setModal.bind(this, 1)} />
                        </Default>
                    </Switch>
                    {/* <Recaptcha
                        sitekey={recaptcha_key}
                        render="explicit"
                        onloadCallback={this.expiredCallback.bind(this)}
                        verifyCallback={this.verifyCallback.bind(this)} 
                        expiredCallback={this.expiredCallback.bind(this)}
                    />  */}
                    </ModalBody>
                    <ModalFooter>
                    <Switch>
                        <Case expr={choice === 1}>
                            <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Prisijungti</Button>{' '} 
                            <Button color="secondary" style={{lineHeight: 0.3}} onClick={this.setModal.bind(this, 2)}>Registruotis</Button>
                        </Case>
                        <Case expr={choice === 2}>
                            <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Registruotis</Button>{' '}  
                            <Button color="secondary" style={{lineHeight: 0.3}} onClick={this.setModal.bind(this, 1)}> Prisijungti</Button>
                        </Case>
                        <Case expr={choice === 3}>
                            <Button color="primary" style={{lineHeight: 0.3}} onClick={this.handleSubmit}>Gauti nuorodą</Button>{' '}   
                            <Button color="secondary" style={{lineHeight: 0.3}} onClick={this.setModal.bind(this, 1)}>Prisijungti</Button>{' '} 
                        </Case>
                    </Switch>
                    </ModalFooter>
                    </Modal>              
            </div>
        );
    }
}

AuthModal.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    choice: PropTypes.number.isRequired,
    isModal: PropTypes.bool.isRequired
};

AuthModal.defaultProps = {
    choice: 1,
    isModal: true,
    changeParams: {} 
};

export default AuthModal;