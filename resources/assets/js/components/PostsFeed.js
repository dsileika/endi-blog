/**
 * Created by Sumit-Yadav on 06-10-2017.
 */
import React, {Component} from 'react'  
import {NavLink, Link, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Validator } from 'ree-validate'
import ApiService from '../services'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert  } from 'reactstrap'; 

// import {
//     Button,
//     Container,
//     Dropdown,
//     Divider,
//     Image,
//     Menu,
//     Responsive,
//     Segment
// } from 'semantic-ui-react';
import * as actions from '../store/actions'
import { Switch, Case, Default } from 'jsx-switch';   
import { relative } from 'path';
import {connect} from 'react-redux'

 class PostsFeed extends Component {

    constructor(props) {
        super(props);     
        this.state = {
            isLoading: true,
            data: [] 
        }; 
    }

    componentWillReceiveProps(nextProps) {
        // this.getData(nextProps.location.pathname);

        // this.getData(nextProps.match.params.id);
        // console.log(nextProps.exceptPost);
        // this.setState({exceptPost: nextProps.exceptPost})
        if(nextProps.exceptPost > 0) this.getData(nextProps.exceptPost);
      }


    componentWillUnmount(){ 
    //    clearInterval(this.timer);
    } 
  
    componentDidMount() {  
        var _this = this; 
        let isAdmin = (this.props.isAdmin) ? this.props.isAdmin : false;
        setTimeout(() => { if(_this.props.exceptPost > 0 && !isAdmin) _this.getData(_this.props.exceptPost); else _this.getData(); },1000);
    }

    getData(exceptPost = 0){ 

       let method = (this.props.isAdmin) ? ApiService.posts({isAdmin: true}) : ApiService.posts({exceptPost: exceptPost});
       this.props.dispatch(method)
                .then((res) => {
                    if(res.data.length>0){ 
                        // let arr = []; 
                        // (res.data).forEach(el => {
                            //    if(exceptPost != el.id) arr.push(el);
                            // }); 
                        let arr = res.data; 
                        this.setState({isLoading: false, data: arr}); 
                    } else   this.setState({isLoading: false});
                })
                .catch(({error, statusCode}) => { 
                // const responseError = {
                //     isError: true,
                //     code: statusCode,
                //     text: error
                // };
                this.setState({isLoading: false}); 
            }) 

                        //    this.timer = setInterval(function() { 
            //         ApiService.posts()
            //                 .then((res) => {
            //                     if(res.data.length>0) this.setState({isLoading: false, data: res.data}); else this.setState({isLoading: false});
            //                 })
            //                 .catch(({error, statusCode}) => { 
            //                 // const responseError = {
            //                 //     isError: true,
            //                 //     code: statusCode,
            //                 //     text: error
            //                 // };
            //                 this.setState({isLoading: false}); 
            //             }) 
            //     }.bind(this), 30000); // 30 sec
    }

    deletePost(args = {}){
        if(args.title && args.id){
        var result = confirm(`Ar tikrai norite ištrint "${args.title}" ?`);
            if (result) {
               if(args.id>0) this.props.dispatch(ApiService.deletePost(args.id)).then(() => {
                this.getData();
               })
                .catch(({error, statusCode}) => {
                    // const responseError = {
                    //     isError: true,
                    //     code: statusCode,
                    //     text: error
                    // };
                    // if(this._mouted) this.setState({responseError});
                    // if(this._mouted) this.setState({
                    //     isLoading: false
                    // });
                })
            }
        }
    }
 

        render() {

            const { data, isLoading } = this.state;               
            const { isAdmin } = this.props;               
             
            
            let content = (data && data.length > 0) ? data.map((item, index) => (
                <div className="post-link col-md-11 col-lg-6 col-xl-3 p-1" key={index}> 
                <Link to={"/post/"+item.id}>
                    <div  className="post-bg" style={{backgroundImage: `url("${item.bgImage}")`, backgroundColor: `${item.bgColor}` }}>
                            <div className="post-preview"  style={{minHeight: `15em` }}>    
                            <div className="post-overlay" />
                                <h4 className="post-title">
                                    {item.title}
                                    <br/>
                                    <small className="post-subtitle">
                                        {item.description}
                                    </small>
                                </h4>
                                <small className="post-meta">
                                        {item.authors},<span>&nbsp;</span>
                                            <span>&nbsp;</span> 
                                        {item.created_at} 
                                        
                                </small>
                                
                        
                            {/* {(data.length - 1) != index && <hr /> }  */}
                            </div> 
                    </div> 
                </Link> 
                {isAdmin &&  
                    <div style={{position: `static` }}>
                    <Link to={"/post/"+item.id+"/edit"}>
                        <button type="button" className="btn btn-success" style={{width: `50px`}}><i className="fa fa-pencil"></i></button>
                    </Link>
                    <a href="#">
                        <button type="button" className="btn btn-danger" style={{width: `50px`}} onClick={this.deletePost.bind(this, {id: item.id, title: item.title})}><i className="fa fa-trash"></i></button>
                    </a>
                    </div>
                }
                </div>
            )) : (
                <div>
                    <div className="post-preview">
                        <center> <p>Nėra jokių post'ų</p> </center>
                    </div> 
                </div>
            );

            return (
            <div className="d-lg-flex justify-content-lg-start flex-lg-wrap flex-lg-row">
            
            {isAdmin && <div className="post-link col-md-11 col-lg-6 col-xl-3 p-1" > 
                  
                <Link to={"/new/post"} >
                            <div  className="post-bg" style={{ backgroundColor: `#6c757d`, backgroundImage: `url("/images/about-bg.jpg")` }}>
                                    <div className="post-preview"  style={{minHeight: `15em` }}>    
                                    <div className="post-overlay" />
                                        <h4 className="post-title">
                                        Sukurti post'ą 
                                        </h4>
                                        <small className="post-meta"><span>&nbsp;</span>
                                                
                                                    <span>&nbsp;</span> 
                                        </small> 
                                    {/* {(data.length - 1) != index && <hr /> }  */}
                                    </div> 
                            </div> 
                    </Link> 
                
                </div> 
            }
                { content } 
            </div>
            );
    }
}

PostsFeed.propTypes = {
    exceptPost: PropTypes.number.isRequired, 
    external: PropTypes.bool.isRequired,
    rerender: PropTypes.func
};

PostsFeed.defaultProps = {
    exceptPost: 0,
    external: false,
    rerender: function(){}
};

//// 
const keys = ["ASXc5uZPDJAjrfznHW63hMPS3Pm","NM3GAXK6BfeEpw44JyqAX5MgtbS","zrYrJc3Q4U5LSU9h5k3c83rNEgN"];
 
const mapStateToProps = (state) => {

    if( state.Auth.ADVE2Lyh5FfSws94WK3s && keys.includes(state.Auth.ADVE2Lyh5FfSws94WK3s) ) { 
        return { 
            isAuthenticated : state.Auth.isAuthenticated,
            isAdmin: true
        } 
    } else { 
        return { 
            isAuthenticated : state.Auth.isAuthenticated,
            isAdmin: false
        }
    }
};

export default connect(mapStateToProps)(PostsFeed); 