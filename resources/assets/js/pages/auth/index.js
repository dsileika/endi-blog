import {connect} from 'react-redux'
import Auth from './Auth'

const mapStateToProps = (state) => {
    return {
        isAuthenticated : state.Auth.isAuthenticated 
    }
};

export default connect(mapStateToProps)(Auth)