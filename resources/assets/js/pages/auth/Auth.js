import React from 'react'
// import {
//     Button,
//     Divider,
//     Dimmer,
//     Form,
//     Grid,
//     Header,
//     Icon,
//     Loader,
//     Message,
//     Segment} from 'semantic-ui-react'
import { Button, Alert } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
import { Validator } from 'ree-validate'
import ApiService from '../../services'
import PageHeader from '../../common/pageHeader'
import AuthModal from '../../components/AuthModal';
import RootReducer from '../../store/reducers';
 
class Auth extends React.Component {
    constructor(props) {
        super(props);  
        this.state = { 
            isLoading: true, 
            redirect: false,
            choice: 1
        };
        // this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
        this.resizeinge = this.resizeing.bind(this);  
    } 
    
    getRoute(route){  
        let choice;
        if(route){ 
            switch(route){
                case 'login': choice = 1; break;
                case 'register': choice = 2; break;
                case 'forgot-password': choice = 3; break;
                case 'reset-password': choice = 4; break;
                default: choice = 1; break;
            }
            
        } else {
            choice = 1;
        }
        this.setState({choice: choice});
    }

    resizeing() {
        let {redirect} = this.state;  
            // if(!isTurn && $(window).width() > 991) {
            if(!redirect && $(window).width() > 991) {
                this.setState({redirect: true});  
            }  
    } 
 
    componentWillMount() { 
        
    }
    componentDidMount(){
        var _this = this;
        
        const { isAuthenticated } = this.props; 
        if(!isAuthenticated){
            window.addEventListener("resize", this.resizeinge, false);
            this.getRoute(this.props.location.pathname.split('/')[1]);  
        }
        setTimeout(() => {
        const social = _this.props.match.params.social
        const params = _this.props.location.search;
        // setTimeout(function() { 

        if (params && social) {
            _this.props.dispatch(ApiService.socialLogin({ params, social }))
        
                .catch(({error, statusCode}) => {
                // const responseError = {
                //     isError: true,
                //     code: statusCode,
                //     text: error
                // };
                 
                // this.setState({responseError});
                // this.setState({
                //     isLoading: false
                // });
                // this.setState({ redirect: true });
                _this.setState({
                    isLoading: false
                });
            }) 
             
            // if(this.props.isAuthenticated) this.setState({ redirect: true });
        } else {
            _this.setState({
                isLoading: false
            });
        }

    }, 500);
        // }.bind(this), 1000); 
            
    }

    componentWillUnmount(){  
        window.removeEventListener("resize", this.resizeinge, false);
    }

   
    

    render() { 
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { isAuthenticated, dispatch } = this.props; 
        const { redirect, errors, choice, isLoading } = this.state;  

      

   
        if (isAuthenticated || redirect == true) {
            
            return (
                <Redirect to={from}/>
            )
        } else

        return (
            <div>

              { isLoading && 
              <div id="loader-wrapper">
                <div id="loader" />
                <div className="loader-section section-left" />
                <div className="loader-section section-right" />
              </div> 
          } 
               
               {/* Page Header */}
            <header className="masthead" style={{backgroundImage: 'url("/images/forestbridge.jpg")'}}>
            <div className="overlay" />
            <div className="container">
                <div className="row">
                <div className="col-lg-8 col-md-10 mx-auto">
                    <div className="" style={{padding: '41px'}}>  
                    </div>
                </div>
                </div>
            </div>
            </header>
            {/* Main Content */}
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        <AuthModal isAuthenticated={isAuthenticated} dispatch={dispatch} isModal={false} choice={choice} changeParams={this.props}  />
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

Auth.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    changeParams: PropTypes.object.isRequired
};

Auth.defaultProps = {
    choice: 1,
    isModal: true,
    changeParams: {}
};

export default Auth;
