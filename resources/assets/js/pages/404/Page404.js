import React from 'react'

class Page404 extends React.Component {
    constructor(props){
        super(props);

    } 
    render() {
        return(
            <div>
               <div>
  {/* Page Header */}
  <header className="masthead" style={{backgroundImage: 'url("/images/404.png")',paddingTop: '7em'}}>
    <div className="overlay" />
    <div className="container">
      <div className="row">
        <div className="col-lg-8 col-md-10 mx-auto">
          <div className="post-heading">
            <h1>404</h1>
            <h2 className="subheading">Nepavyko rasti puslapio</h2> 
          </div>
        </div>
      </div>
    </div>
  </header> 
</div>

            </div>
        );
    }
}

export default Page404;