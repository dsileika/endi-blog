import React from 'react';
// import { convertFromHTML, EditorState, convertToRaw, ContentState } from 'draft-js';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
// import {stateFromHTML} from 'draft-js-import-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
// import draftToHtml from 'draftjs-to-html';
// import {stateToHTML} from 'draft-js-export-html';

// import htmlToDraft from 'html-to-draftjs';
// import ApiService from '../../../../services' 
import ColorPic from './controls/ColorPic'   
import PropTypes from 'prop-types' 

import getBlockRenderFunc from './renderer';
import Embedded from './renderer/Embedded';
import getImageComponent from './renderer/Image';

import {convertToHTML, convertFromHTML} from 'draft-convert';
import { Inline, Block, Entity } from './constants';

class TextEditor extends React.Component {
  
    constructor(props){
      super(props); 
      
      this.setDomEditorRef = ref => this.domEditor = ref;
      
      this.state = {
        editorState: EditorState.createEmpty(), 
      }  

      this.blockRendererFn = getBlockRenderFunc(
        {
          isReadOnly: this.isReadOnly.bind(this),
          isImageAlignmentEnabled: this.isImageAlignmentEnabled.bind(this),
          getEditorState: this.getEditorState.bind(this),
          onChange: this.onChange.bind(this)
        } 
        
      );

  }

  getEditorState() {  const { editorState } = this.state; return editorState; }

  isReadOnly() { return this.props.readOnly; }

  isImageAlignmentEnabled(){ return this.state.toolbar.image.alignmentEnabled; }

  onChange() {
    const { readOnly, onEditorStateChange } = this.props;
    const { editorState } = this.state;
    if (
      !readOnly &&
      !(
        getSelectedBlocksType(editorState) === "atomic" &&
        editorState.getSelection().isCollapsed
      )
    ) {
      if (onEditorStateChange) {
        onEditorStateChange(editorState, this.props.wrapperId);
      }
      if (!hasProperty(this.props, "editorState")) {
        this.setState({ editorState }, this.afterChange(editorState));
      } else {
        this.afterChange(editorState);
      }
    }
  };

  myBlockRenderer(block, config) {
    const type = block.getType(); 

    if (type === 'atomic') {
      const contentState = config.getEditorState().getCurrentContent();
      const entity = contentState.getEntity(block.getEntityAt(0));
      if (entity && entity.type === 'IMAGE') {
        return {
          component: getImageComponent(config),
          editable: false,
        };
      } else if (entity && entity.type === 'EMBEDDED_LINK') { 
        return {
          component: Embedded,
          editable: false,
        };
      }
    } 
  }
 
  

  getData(res){
    
              if(res && res.content) { 
                // var contentState = stateFromHTML(res.data[0].content); 
                // convert HTML to ContentState with blue text, links, and at-mentions
                   
                var contentState = convertFromHTML({
                  htmlToStyle: (nodeName, node, currentStyle) => {
                    
                    node.className.split(' ').forEach((className) => {
                      // DEBUG
                      // console.log('className:', className);
                      switch (className) {
                        case `md-inline-${Inline.ITALIC.toLowerCase()}`: {
                          currentStyle.add(Inline.ITALIC);
                          break;
                        }
                        case `md-inline-${Inline.BOLD.toLowerCase()}`: {
                          currentStyle.add(Inline.BOLD);
                          break;
                        }
                        case `md-inline-${Inline.STRIKETHROUGH.toLowerCase()}`: {
                          currentStyle.add(Inline.STRIKETHROUGH);
                          break;
                        }
                        case `md-inline-${Inline.UNDERLINE.toLowerCase()}`: {
                          currentStyle.add(Inline.UNDERLINE);
                          break;
                        }
                        case `md-inline-${Inline.HIGHLIGHT.toLowerCase()}`: {
                          currentStyle.add(Inline.HIGHLIGHT);
                          break;
                        }
                        case `md-inline-${Inline.CODE.toLowerCase()}`: {
                          currentStyle.add(Inline.CODE);
                          break;
                        }
                      }
                    });

                    return currentStyle;
                  },
                  htmlToEntity: (nodeName, node, createEntity) => {
 
                    
                      if (nodeName === 'img') { 
                        return createEntity(
                            'IMAGE',
                            'MUTABLE',
                            {src: node.src}
                          )
                      }
                      if (nodeName === 'a') {
                          return createEntity(
                              'LINK',
                              'MUTABLE',
                              {url: node.href}
                          )
                      }
                      if (nodeName === 'iframe') { 
                            return createEntity(
                                'EMBEDDED_LINK',
                                'MUTABLE',
                                { src: node.src }
                            )
                      }
                      
                  },
                  textToEntity: (text, createEntity) => {
                      const result = [];
                      text.replace(/\@(\w+)/g, (match, name, offset) => {
                          const entityKey = createEntity(
                              'AT-MENTION',
                              'IMMUTABLE',
                              {name}
                          ); 
                          result.push({
                              entity: entityKey,
                              offset,
                              length: match.length,
                              result: match
                          });
                      });
                      return result;
                  },
                  htmlToBlock: (nodeName, node, lastList, inBlock) => {
                      if (nodeName === 'figure') {
                        return 'atomic'; 
                      }  
                      // if (nodeName === 'figure' && node.firstChild.nodeName === 'iframe' || (nodeName === 'iframe' && inBlock !== BLOCK_TYPE)) {
                      //   return BLOCK_TYPE;
                      // } 
                      // if (nodeName === "iframe" || nodeName === 'figure' && node.firstChild.nodeName === 'img' || (nodeName === 'img' && inBlock !== BLOCK_TYPE)) {
                      //   return BLOCK_TYPE;
                      // }
                      if (nodeName === 'blockquote') {
                          return {
                              type: 'blockquote',
                              data: {}
                          };
                      } 
                  }
                })(res.content);
                


                let editorState = EditorState.createWithContent(contentState);  
                
                this.setState({ editorState}); 


                 // as per method in https://github.com/jpuri/react-draft-wysiwyg/blob/master/src/Editor/index.js
                this.domEditor.focusEditor(); 
                 
                // EditorState.set(editorState, {decorator: this.compositeDecorator});
              }
              // else 
              // this.setState({isLoading: false, redirect: true});
               
  }

  componentDidMount() {
    let _this = this;
    let { data, postid } = this.props;
    // console.log(this.props);
    // setTimeout(() => {
      if (data) {
        _this.getData(data);
      } else {
        _this.setState({isLoading: false, editorState: EditorState.createEmpty()});
      }
    // }, 5000);    
    
  } 
  componentWillUnmount(){
    
  }

  

  onEditorStateChange(editorState) {

  
    // let content = stateToHTML(editorState.getCurrentContent(), options);
    
    function getId(url) {
      var regExp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
      var match = url.match(regExp); 
      if (match && match[1]) {
          return match[1];
      } else {
          return '';
      }
    }
  
    // convert to HTML with blue text, paragraphs, and links
    let content = convertToHTML({
      styleToHTML: (style) => { 
        if (style === 'SUPERSCRIPT') {
          return {
            start: '<sup>',
            end: '</sup>'
          };
        }
        if (style === 'SUBSCRIPT') {
          return {
            start: '<sub>',
            end: '</sub>'
          };
        }
        if (style === 'STRIKETHROUGH') {
          return {
            start: '<del>',
            end: '</del>'
          };
        }

        if (style === 'BOLD') {
          return {
            start: '<b>',
            end: '</b>'
          };
        }

        if (style === 'ITALIC') {
          return {
            start: '<i>',
            end: '</i>'
          };
        } 

        if (style === 'UNDERLINE') {
          return {
            start: '<u>',
            end: '</u>'
          };
        }   

        if(style.split('-')[0].indexOf('fontsize') || style.split('-')[0].valueOf('color')){  
          let customstyle = "";
      
          switch(style.split('-')[0]){
            case "fontsize": customstyle += "font-size:"+style.split('-')[1]+"px;"; break;
            case "color": customstyle += "color: "+style.split('-')[1]+";"; break;
            default: customstyle = "";
          }
          // return <span style={customstyle} />
          return {
            start: `<span style='${customstyle}'>`,
            end: "</span>"
          };
        }  
      
        
      },
      blockToHTML: (block) => { 
        const blockType = block.type.toLowerCase(); 

        if(blockType === 'unstyled' && block.data['text-align']){
          return {
            start: `<p style="text-align: ${block.data['text-align']}" >`,
            end: "</p>"
          };
        }

        if(blockType === 'code'){
          return {
            start: "<code>",
            end: "</code>",
            empty: "" 
          };
        }
        
        if (blockType === "atomic") { 
          return {
              start: "<figure>",
              end: "</figure>",
              empty: "" 
          };
        } 
 
      },
      entityToHTML: (entity, originalText) => {
        // Link
        const entityType = entity.type.toLowerCase();  
        if (entityType === 'link') { 
          let {targetOption, url } = entity.data;
          let target = (targetOption) ? targetOption : "_self";
          return <a href={url} target={target}>{originalText}</a>;
        }
        // Iframe
        if(entityType === 'embedded_link'){

          let { src, height, width } = entity.data; 

          let style = (!width || !height || width == "auto" || height == "auto") ? "embed-responsive embed-responsive-16by9 rounded mx-auto" : "";
          if (src.indexOf("youtube") > 0){
            let videoid = getId(src);
            src = `//www.youtube.com/embed/${videoid}`; 
          } 

          return `<div class="${style}"><iframe src=${src} frameBorder="0" allowFullScreen title="Video"></iframe></div>`;
        }
        // Image
        if (entityType === 'image') {
          let data = entity.data;
          let style = "";

          if(data.width && data.width != "auto" && data.height && data.height != "auto") 
          style = `
            height: ${data.height};
            width: ${data.width};
          ` 
          let align = "d-block"; 
          switch (data.alignment){
            // case "right": align = "float-right"; break;
            // case "left": align = "float-left"; break;
            default: align = "d-block"; break;
          }

          return `<img src="${data.src}" style="${style}" class="img-fluid rounded mx-auto ${align}" />`;

        }

        return originalText;
      }
    })(editorState.getCurrentContent());
  
 
    // Iframe end 
    this.props.handler( content );         

    this.setState({
      editorState,
    }); 

    // var content = draftToHtml( convertToRaw(editorState.getCurrentContent()) );
    // let content = stateToHTML(editorState.getCurrentContent() );

  }; 

  // Update the editor state when new props are received.
 componentWillReceiveProps(nextProps) {
    // this.setState({ editorState: this.createEditorState(nextProps.value) });
}

  render() {
    const { editorState } = this.state;

    const customToolbar = {
      options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'colorPicker', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
      inline: {
        inDropdown: false, 
        options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript', 'subscript'], 
      },
      embedded: { 
        // className: "embed-responsive-16by9", 
        // popupClassName: undefined,
        // defaultSize: {
        //   height: 'auto',
        //   width: 'auto',
        // },
      },
      colorPicker: { 
        component: ColorPic 
      },
    }

     
    return (
      <div>
        <Editor
          // defaultEditorState={editorState}
          ref={this.setDomEditorRef}
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor" 
          onEditorStateChange={this.onEditorStateChange.bind(this)} 
          toolbar={customToolbar}
          // blockRendererFn={this.blockRendererFn.bind(this)}
          customBlockRenderFunc={this.myBlockRenderer.bind(this)}  
        />  

      
        {/*  <textarea disabled value={draftToHtml(convertToRaw(editorState.getCurrentContent()))} ></textarea>*/}

      </div>
    );
  }
}
 
export default TextEditor;