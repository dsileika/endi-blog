import React from 'react'; 
import { BlockPicker } from 'react-color';
import PropTypes from 'prop-types' 

export default class ColorPic extends React.Component { 

  stopPropagation(event) {
    event.stopPropagation();
  };

  onChange(color) {
    const { onChange } = this.props;
    onChange('color', color.hex);
  }

  renderModal()  {
    const { color } = this.props.currentState;
    return (
      <div
        onClick={this.stopPropagation}
        className="rdw-colorpicker-custom-modal"
      >
        <BlockPicker color={color} onChangeComplete={this.onChange.bind(this)} />
      </div>
    );
  };

  render() {
    const { expanded, onExpandEvent, config } = this.props;
    return (
      <div
        aria-haspopup="true"
        aria-expanded={expanded}
        className="rdw-colorpicker-wrapper"
        aria-label="rdw-color-picker"
      >
        <div className="rdw-option-wrapper"
          onClick={onExpandEvent}
        >

          <img src={config.icon} alt="" />
        
        </div>
        {expanded ? this.renderModal() : undefined}
      </div>
    );
  }
}

// ColorPic.propTypes = {
//     expanded: PropTypes.bool,
//     onExpandEvent: PropTypes.func,
//     onChange: PropTypes.func,
//     currentState: PropTypes.object,
// }; 

// ColorPic.defaultProps = { 
//     // onChange: PropTypes.func,
//     // currentState: PropTypes.object,
// }