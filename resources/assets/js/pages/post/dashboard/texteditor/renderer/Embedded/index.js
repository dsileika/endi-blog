import React from 'react';
import PropTypes from 'prop-types';

const Embed = ({ block, contentState }) => {
  const entity = contentState.getEntity(block.getEntityAt(0));
  // let { src, height, width } = entity.getData(); 
  // // var mySrc = src;

  function getId(url) {
    var regExp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    var match = url.match(regExp); 
    if (match && match[1]) {
        return match[1];
    } else {
        return '';
    }
  }
   
  let { src, height, width } = entity.data; 
  
  let style = (!width || !height || width == "auto" || height == "auto") ? "embed-responsive embed-responsive-16by9 rounded mx-auto" : "";
 
  if (src.indexOf("youtube") > 0){
    let videoid = getId(src);
    src = `//www.youtube.com/embed/${videoid}`; 
  } 

  return (<div className={style}><iframe src={src} frameBorder="0" allowFullScreen title="Video"></iframe></div>);
  
  

};

Embed.propTypes = {
  block: PropTypes.object.isRequired,
  contentState: PropTypes.object.isRequired,
};

export default Embed;
