import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { EditorState } from 'draft-js';
import classNames from 'classnames';
// import Option from '../../components/Option';
import './styles.css';

const getImageComponent = config => class Image extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      hovered: false,
    };
  }

  setEntityAlignmentLeft() {
    this.setEntityAlignment('left');
  };

  setEntityAlignmentRight() {
    this.setEntityAlignment('right');
  };

  setEntityAlignmentCenter() {
    this.setEntityAlignment('none');
  };

  setEntityAlignment(alignment) {
    const { block, contentState } = this.props;
    const entityKey = block.getEntityAt(0);
    contentState.mergeEntityData(
      entityKey,
      { alignment },
    );
    config.onChange(EditorState.push(config.getEditorState(), contentState, 'change-block-data'));
    this.setState({
      dummy: true,
    });
  };

  toggleHovered() {
    const hovered = !this.state.hovered;
    this.setState({
      hovered,
    });
  };

  renderAlignmentOptions(alignment) {
    // return (
    //   <div
    //     className={classNames(
    //       'rdw-image-alignment-options-popup',
    //       {
    //         'rdw-image-alignment-options-popup-right': alignment === 'right',
    //       },
    //     )}
    //   >
    //     <a 
    //       onClick={this.setEntityAlignmentLeft.bind(this)}
    //       className="rdw-image-alignment-option"
    //     >
    //       Kaire
    //     </a>
    //     <a 
    //       onClick={this.setEntityAlignmentCenter.bind(this)}
    //       className="rdw-image-alignment-option"
    //     >
    //       Centras
    //     </a>
    //     <a 
    //       onClick={this.setEntityAlignmentRight.bind(this)}
    //       className="rdw-image-alignment-option"
    //     >
    //       Desine
    //     </a>
    //   </div>
    // );
  }

  render() {
    const { block, contentState } = this.props;
    const { hovered } = this.state;
    const { isReadOnly, isImageAlignmentEnabled } = config;
    const entity = contentState.getEntity(block.getEntityAt(0));
    const { src, alignment, height, width, alt } = entity.getData();

    return (
      <span
        onMouseEnter={this.toggleHovered.bind(this)}
        onMouseLeave={this.toggleHovered.bind(this)}
        className={classNames(
          'rdw-image-alignment',
          {
            'rdw-image-left': alignment === 'left',
            'rdw-image-right': alignment === 'right',
            'rdw-image-center': !alignment || alignment === 'none',
          },
        )}
      >
        <span className="rdw-image-imagewrapper">
          <img
            src={src}
            // alt={alt}
            className="img-fluid rounded mx-auto"
            // style={{
            //   height,
            //   width,
            // }}
          />
          {/* {
            !isReadOnly() && hovered && isImageAlignmentEnabled() ?
              this.renderAlignmentOptions(alignment)
              :
              undefined
               
          
          } */}
        </span>
      </span>
    );
  }
};

export default getImageComponent;

getImageComponent.propTypes = {
  block: PropTypes.object,
  contentState: PropTypes.object,
};