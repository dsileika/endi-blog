import React from 'react'
// import {
//     Button,
//     Container,
//     Grid,
//     Header,
//     Icon,
//     Responsive,
//     Segment,
//     Step
// } from 'semantic-ui-react'
// import {
//     Collapse,
//     Navbar,
//     NavbarToggler,
//     NavbarBrand,
//     Nav,
//     NavItem,
//     NavLink,
//     UncontrolledDropdown,
//     DropdownToggle,
//     DropdownMenu,
//     DropdownItem } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom'
import ApiService from '../../../services' 
import PostsFeed from '../../../components/PostsFeed'; 
import TextEditor from './texteditor';   
import { Validator } from 'ree-validate'

// Load the initial value from Local Storage or a default.
// const initialValue = localStorage.getItem('content') || '<p></p>';

class PostAuth extends React.Component {
    constructor(props) {
        super(props);
        
        this.validator = new Validator({
          'title': 'required|min:2',
          'note': 'min:3',
          'bgColor': 'min:6',
          'bgImage': 'min:1',
          'cat_id': 'required|min:1',
          'content': 'required|min:8',
          'isPublic': 'required|min:1',
        });     
        this.state = { 
          isLoading: true, 
          redirect: false,
          contentHeight: 0,
          postid: null,
          data: "",
   
          parseCategorys: [{}],
          credentials: {
            'title': '',
            'note': '',
            'bgColor': '#6c757d',
            'bgImage': '',
            'cat_id': '',
            'content': '',
            'isPublic': '0'
          }
        } 
 
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }
    

    componentDidCatch(error, info) { 
      // this.setState({redirect: true});
    }
    

    getData(){
   
          // this.setState({isLoading: false, contentHeight: 0,  redirect: true});
          if(this.props.match && this.props.match.params.id){
            let method = (this.props.isAdmin) ? {id: this.props.match.params.id, isAdmin: true} : {id: this.props.match.params.id}; 
            this.props.dispatch(ApiService.posts(method))
              .then((res) => { 
                this.setState({postid: this.props.match.params.id, credentials: {
                  'title': res.data.title ? res.data.title : '',
                  'note': res.data.note ? res.data.note : '',
                  'bgColor': res.data.bgColor ? res.data.bgColor : '#6c757d',
                  'bgImage': res.data.bgImage ? res.data.bgImage : '',
                  'cat_id': res.data.cat_id ? res.data.cat_id : '',
                  'content': res.data.content ? res.data.content : '',
                  'isPublic': res.data.isPublic ? res.data.isPublic : '0'
                }, isLoading: false});
            })
            .catch(({error, statusCode}) => { 
            // this.setState({isLoading: false, redirect: true}); 
            if(this._mouted) this.setState({isLoading: false}); 
            });

          } else {
            if(this._mouted) this.setState({postid: null, isLoading: false});
          }

          this.props.dispatch(ApiService.categorys())
              .then((res) => { 
                if(this._mouted)    this.setState({parseCategorys: res, isLoading: false});
            })
            .catch(({error, statusCode}) => { 
            // this.setState({isLoading: false, redirect: true}); 
            if(this._mouted)  this.setState({isLoading: false}); 
          });
          
          // this.setState({isLoading: false});
        // } 
    }
 
    componentDidMount() {
         this._mouted = true;
          if(!this.props.isAuthenticated || !this.props.isAdmin) this.setState({redirect: true});
          this.getData();

    } 
    componentWillUnmount(){
      this._mouted = false;
    }

    handler(data) { 
      // e.preventDefault() 
      if(this._mouted) if(data !== `<p></p>`)  this.setState({
        data,
        credentials: { ...this.state.credentials, content: data } 
      }); else this.setState({
        data,
        credentials: { ...this.state.credentials, content: "" } 
      });
    }


    handleChange(event) {
        let name = event.target.name;
        let value = event.target.value; 

        const {credentials} = this.state;
        credentials[name] = value;
 
        this.validator.validate(name, value)
            .then(() => {
                // const {errorBag} = this.validator;
                // this.setState({errors: errorBag, credentials})
            })
    }

    handleSubmit(event) {
        // const { validator, authservicer, isHuman } = this.state;  
        event.preventDefault(); 
        const {credentials} = this.state;  
        
        this.validator.validateAll(credentials)
            .then(success => {
                if (success) {
                  // if(this._mouted) this.setState({
                  //       isLoading: true
                  //   });
                    this.submit(credentials);
                }
            }); 
    }

    submit(credentials) { 

    let method = (this.props.match && this.props.match.params.id) ? ApiService.updatePost({ ...credentials, id: this.props.match.params.id}) : ApiService.createPost(credentials);

      this.props.dispatch(method).then((res) => {
         if(this._mouted) this.setState({  redirect: true   });
      })
          .catch(({error, statusCode}) => {
              const responseError = {
                  isError: true,
                  code: statusCode,
                  text: error
              };
              if(this._mouted) this.setState({responseError});
              if(this._mouted) this.setState({
                  isLoading: false
              });
          })

  }

    render() {
      let { redirect, isLoading, postid, data, credentials, parseCategorys } = this.state;
      
      let { from } = this.props.location.state || { from: { pathname: '/' } };   

     
      let categorys = (parseCategorys.data && parseCategorys.data.length > 0) ? (parseCategorys.data).map((cat, index) => (
        <option value={cat.id} selected={cat.id == credentials.cat_id} key={index}>{cat.title}</option>
      )) : "";
       
      if(isLoading){
        return (
          <div id="loader-wrapper">
            <div id="loader" />
            <div className="loader-section section-left" />
            <div className="loader-section section-right" />
          </div>
        )
      } 

      if(!redirect) return (     
             <div>
  {/* Page Header */}
  <header className="masthead" style={{backgroundImage: 'url("/images/about-bg.jpg")'}}>
    <div className="overlay" />
    <div className="container">
      <div className="row">
        <div className="col-lg-8 col-md-10 mx-auto">
          <div  style={{padding: '41px'}}> 
            {/* <span className="subheading">This is what I do.</span> */}
          </div>
        </div>
      </div>
    </div>
  </header>
  {/* Main Content */}
  <div className="container">
    <div className="row">
      {/* <div className="col-lg-10 col-md-10 mx-auto"> */}
      <div className="col-lg-6 col-md-10 mx-auto">
        
      <form name="postEditor" id="contactForm" onSubmit={this.handleSubmit} noValidate>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group-skip controls floating-label-form-group-with-value">
                                <label htmlFor="title" className="required">Antraštė</label>
                                <input type="text" className="form-control" defaultValue={credentials.title} onChange={this.handleChange} name='title' id='title' required  />
                                {/* <p className="help-block text-danger" /> */}
                            </div>
                            </div> 
                            <div className="control-group">
                            <div className="form-group floating-label-form-group-skip floating-label-form-group-with-value" style={{borderBottom: 'none'}}>
                                <label htmlFor='title-color' className="required">Antraštės fono spalva</label> 
                                <input type="color" defaultValue={(credentials.bgColor) ? credentials.bgColor : "#6c757d"} className="form-control" onChange={this.handleChange} id='title-color' name='bgColor' required  />
                            </div>
                            </div> 
                            
                            <div className="control-group">
                            <div className="form-group floating-label-form-group-skip controls floating-label-form-group-with-value">
                                <label htmlFor='title-url'>Antraštės fono nuotraukos nuorada</label>
                                <input type="url" className="form-control" defaultValue={credentials.bgImage} onChange={this.handleChange} id='title-url'  name='bgImage'  />
                                {/* <p className="help-block text-danger" /> */}
                            </div>
                            </div> 

                            <div className="control-group">
                            <div className="form-group floating-label-form-group-skip controls floating-label-form-group-with-value">
                                <label htmlFor='note'>Pastaba</label>
                                <input type="text" className="form-control" name='note' defaultValue={credentials.note} onChange={this.handleChange} id='note'   />
                                {/* <p className="help-block text-danger" /> */}
                            </div>
                            </div>
                            <div className="control-group">
                            <div className="form-group floating-label-form-group-skip controls floating-label-form-group-with-value" style={{borderBottom: 'none'}}>
                                <label htmlFor='category' className="required">Kategorija</label>
                                <select  className="form-control" name='cat_id' onChange={this.handleChange} id='category'  required>
                                  <option value="">Pasirinkite kategoriją</option>
                                  {categorys}
                                </select>
                                {/* <p className="help-block text-danger" />  */}
                            </div>
                            </div>
                            {/* Implemented editor */}
                            <div className="control-group">
                                <div className="floating-label-form-group-skip floating-label-form-group-with-value" style={{borderBottom: 'none'}}>
                                    <label htmlFor='editor' className="required">Turinys</label>
                                </div>
                                    
                                    {!isLoading && <TextEditor handler ={this.handler.bind(this)} data = {credentials} {...this.props} id="editor" /> }
                                    
                                    {/* <p className="help-block text-danger" />  */}

                            </div>
                            <div className="form-group floating-label-form-group-skip controls floating-label-form-group-with-value" style={{borderBottom: 'none'}}>
                                <label htmlFor='isPublic' className="required">Ar publikuoti post'ą?</label>
                                <select  className="form-control" name='isPublic' defaultValue={(credentials.isPublic) ? credentials.isPublic : "0"} onChange={this.handleChange} id='isPublic'  required>
                                  <option value="0">Ne</option>
                                  <option value="1">Taip</option>
                                </select>
                                {/* <p className="help-block text-danger" />  */}
                            </div>
                            {(this.props.match && this.props.match.params.id) &&
                              <button className="btn btn-primary" type="submit">Atnaujinti</button>
                            }
                            {(!this.props.match || !this.props.match.params.id) &&
                              <button className="btn btn-primary" type="submit">Pateikti</button>
                            }
                            {/* <button type="submit" style={{display: 'none' }} /> */}
                        </form>                         
      </div>
      {/* {(data && data.length > 8) &&  <div className="col-lg-10 col-md-10 mx-auto wordwrap"> */}
      {(data && data.length > 8) &&  <div className="col-lg-6 col-md-10 mx-auto wordwrap" style={{height:`41em`,overflow:`auto`}}>
            <center>Post'o peržiūra</center><hr/>
            <div  dangerouslySetInnerHTML={{__html: data }} />
          </div>
        }
    </div>
  </div>
</div>

      ); 
      else return (
          <Redirect to={from} />
        );
      }

       

     
}

export default PostAuth;