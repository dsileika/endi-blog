import React from 'react'
// import {
//     Button,
//     Container,
//     Grid,
//     Header,
//     Icon,
//     Responsive,
//     Segment,
//     Step
// } from 'semantic-ui-react'
// import {
//     Collapse,
//     Navbar,
//     NavbarToggler,
//     NavbarBrand,
//     Nav,
//     NavItem,
//     NavLink,
//     UncontrolledDropdown,
//     DropdownToggle,
//     DropdownMenu,
//     DropdownItem } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom'
import ApiService from '../../services' 
import PostsFeed from '../../components/PostsFeed';
import PropTypes from 'prop-types';
 
 

class Post extends React.Component {
  
    componentDidCatch(error, info) {
        // this.setState({redirect: true}); 
        
    }
 
    constructor(props) { 
        super(props);   
        this.state = { 
          isLoading: true,
          data: [{}],
          redirect: false,
          contentHeight: 0
        };

        
      } 
      
 
    componentDidMount() { 
      this._isMounted = true;
        // setTimeout(() => {   
            if(!isNaN(this.props.match.params.id) && this.props.match && this.props.match.params.id) { 
              let method = (this.props.isAdmin) ? {id: this.props.match.params.id, isAdmin: true} : {id: this.props.match.params.id};
              this.getData(method); }
              else {      
            this.setState({redirect: true});
              }
          // }, 1000);
    } 
    componentWillUnmount(){
      this._isMounted = false;
    }

    
    getData(args = {}) { 
      // const postid = this.props.match.params.id  
      console.log(args);
      if (args.id) {  

        this.props.dispatch(ApiService.posts({...args}) )
        .then((res) => { 
          if(res.data) {
          if(this._isMounted)  this.setState({isLoading: false, data: [res.data], contentHeight: $('div.postcontent').height() }); 
            
            // Comments count set
            // $('.return-to-comments .post-badge').text(`${res.data.votes}`);

          } else { 

            if(this._isMounted) this.setState({isLoading: false, contentHeight:0, redirect: true});

          }
           // if($('div.postcontent').height() > 900){
                //     $('.return-to-bottom').fadeIn(300);   // Else fade out the arrow
                // } else {
                //   $('.return-to-bottom').fadeOut(300);
                // }

          window.scrollTo(0, 0) 

          $(window).scroll(function() { 
            if($('#down').offset() && $(this).scrollTop() <= ($('#down').offset().top - $(this).height()) /* && $('div.postcontent').height() > 900 */ && $(this).scrollTop() >= 400){
              $('.return-to-bottom').fadeIn(200);    // Fade in the arrow
            } else {
              $('.return-to-bottom').fadeOut(300);   // Else fade out the arrow
            }
          });


          let contentHeight = $('div.postcontent').height();

          if(this._isMounted) this.setState({contentHeight: contentHeight});

          // Comments icon
          // $('.return-to-comments').fadeIn(200);
          
 
        })
        .catch(({error, statusCode}) => { 
          // const responseError = {
          //     isError: true,
          //     code: statusCode,
          //     text: error
          // };
          if(this._isMounted)  this.setState({isLoading: false, contentHeight: 0, redirect: true}); 
      }) 
         
      } else {
        if(this._isMounted) this.setState({isLoading: false, contentHeight: 0,  redirect: true});
      }
    }
  

    componentWillReceiveProps(nextProps) { 
      if(!isNaN(nextProps.match.params.id) && nextProps.match &&  nextProps.match.params.id) { 
        let method = (this.props.isAdmin) ? {id:  nextProps.match.params.id, isAdmin: true} : {id:  nextProps.match.params.id};
        this.getData(method); 
      } else {      
        this.setState({redirect: true});
      }

    //  if(nextProps.match && nextProps.match.params.id) this.getData(nextProps.match.params.id)
    }
  

    render() {
      let { data, redirect, isLoading, contentHeight, redirectPath } = this.state;
      let { from } = this.props.location.state ||  { from: { pathname: '/' } }; 
      
      if(redirect) return (
        <Redirect to={from} />
      );
      
      const { match, location, dispatch, isAdmin } = this.props;  

      if(isLoading){
        return (
          <div id="loader-wrapper">
            <div id="loader" />
            <div className="loader-section section-left" />
            <div className="loader-section section-right" />
          </div>
        )
      } 

    return data.map(
        (item, index) => (
            <div key={index}> 
               <div>
               
  {/* Page Header */}
  <header className="masthead" style={{backgroundImage: `url("${item.bgImage}")`, backgroundColor: `${item.bgColor}` }}>
    <div className="overlay" />
    <div className="container">
      <div className="row">
        <div className="col-lg-8 col-md-10 mx-auto">
          <div className="post-heading">
            <center>
            <h1>{item.title}</h1>
            <h2 className="subheading">{item.description}</h2>
            <span className="meta"><a href="#">{item.authors}</a>,<br/> {item.created_at}</span>
            <br/> 
                       {/* {contentHeight > 100 && <center><a href="#post" className="btn btn-circle">
                            <i className="fa fa-angle-double-down animated"></i>
                        </a></center>
                       } */}
                       <a href="#post" className="btn btn-circle">
                            <i className="fa fa-angle-double-down animated"></i>
                        </a>
              </center>
          </div>
        </div>
      </div>
    </div>
  </header>
  {/* Post Content */}
  <article id="post">
    <div className="container postcontainer">
      <div className="row">
        <div className="col-lg-8 col-md-10 mx-auto postcontent" dangerouslySetInnerHTML={{__html: item.content}}> 
        </div>
      </div>
    </div>
  </article>
  <hr />
</div>
<div id="down"/>
<div className="posts-feed">
  <div className="container">
    <div className="row">
      <div className="col-md-11 col-lg-11 col-xl-11 mx-auto ">  
          <div>  
              <PostsFeed dispatch={dispatch} exceptPost={item.id} {...this.props} />
          </div>
      </div>
    </div>
  </div>
  </div>

           
      </div>
        )
      );
       
      }
}

export default Post;

// Post.propTypes = {
//   match: PropTypes.shape({
//     params: PropTypes.shape({
//       id: PropTypes.number 
//     })
//   }), 
// } 

// Post.defaultProps = {
//   match: PropTypes.shape({
//     params: PropTypes.shape({
//       id: 0
//     })
//   }), 
// } 
