import React from 'react'
// import {
//     Button,
//     Container,
//     Grid,
//     Header,
//     Icon,
//     Responsive,
//     Segment,
//     Step
// } from 'semantic-ui-react'
// import {
//     Collapse,
//     Navbar,
//     NavbarToggler,
//     NavbarBrand,
//     Nav,
//     NavItem,
//     NavLink,
//     UncontrolledDropdown,
//     DropdownToggle,
//     DropdownMenu,
//     DropdownItem } from 'reactstrap'; 
import {Link, Redirect} from 'react-router-dom'
import ApiService from '../../services'  
import PropTypes from 'prop-types'

import PostsFeed from '../../components/PostsFeed';

class Page extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            redirect: false
        };
        
    }
 
 
    toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }

      
    componentWillUnmount(){
      this._mouted = false;
      
    }

    componentDidMount() {
        this._mouted = true;
        const social = this.props.match.params.social
        const params = this.props.location.search; 

        
        if(this._mouted){ 
            if (params && social) {
                this.props.dispatch(ApiService.socialLogin({ params, social }))
                    .then((res) => {
                        this.setState({ isLoading: false, redirect: true });
                    })  
                    .catch(({error, statusCode}) => {
                    const responseError = {
                        isError: true,
                        code: statusCode,
                        text: error
                    };
                    
                    this.setState({responseError});
                    this.setState({
                        isLoading: false
                    });
                    this.setState({ redirect: true });
                })
                
                if(this.props.isAuthenticated) this.setState({ isLoading: false, redirect: true });
            } 
 


        }
    }

    onChange(e) {
        // console.log(e.target.value);
        // this.setState({ inputValue: e.target.value });
    }

    render() { 
        // const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { redirect, isLoading } = this.state;
        const { dispatch, isAuthenticated, userName } = this.props; 
  
        if(isLoading){
            return (
              <div id="loader-wrapper">
                <div id="loader" />
                <div className="loader-section section-left" />
                <div className="loader-section section-right" />
              </div>
            )
          } 

        if (redirect) {
            // return <Redirect to={from} />;
        } 

        let headerTxt = (userName) ? `<h1>Labas,</h1><h3 className="subheading">${userName}</h3>` : `<h1>Blog'as</h1><span className="subheading">Atrask kažką naujo</span>`;

        return (
            <div>
               {/* Page Header */}
            <header className="masthead" style={{backgroundImage: 'url("/images/forestbridge.jpg")'}} >
            <div className="overlay" />
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        <div className="site-heading" >
                        <div dangerouslySetInnerHTML={{__html: headerTxt }} /> 
                        <br/>
                        <a href="#news"  className="btn btn-circle">
                            <i className="fa fa-angle-double-down animated"></i>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
            </header>

            <div id="news">
  {/* Main Content */}
  <div className="posts-feed">
  <div className="container">
    <div className="row">
      <div className="col-md-11 col-lg-11 col-xl-11 mx-auto ">  
          <div>
            
           <PostsFeed dispatch={dispatch} {...this.props} />

            {/* Pager */}
            {/* <div className="clearfix">
                <a className="btn btn-primary float-right" href="#">Older Posts →</a>
            </div> */} 
            </div>
      </div>
    </div>
  </div>
  </div>
  <hr />
</div>


                {/* <Segment
                    inverted
                    textAlign='center'
                    className='home-header'
                    vertical
                >
                    <Container text>
                        <Responsive minWidth={769}>
                            <Header
                                as="h2"
                                content="Something Cool"
                                inverted
                                className="pretitle"
                            />
                        </Responsive>
                        <Header
                            as='h1'
                            content='Tagline Here'
                            inverted
                            className="main-heading"
                        />
                        <Header
                            as='p'
                            content='sub heading'
                            inverted
                            className="sub-heading"
                        />
                        <Button color="teal" size='huge' className="free-signup-button">
                            <Link to='/register' replace>Register</Link>
                        </Button>
                    </Container>
                </Segment>
                <div className="course-tour">
                    <Container textAlign="center" style={{padding: '2em 0em'}}>
                        <Header as="h3" content="About your company"/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </Container>
                    <Container className="step-container">
                        <Responsive minWidth={992}>
                            <Grid columns={1} padded="horizontally">
                                <Grid.Row>
                                    <Grid.Column>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem beatae
                                            ducimus eius eos fugit ipsam, nemo rem repellendus suscipit unde? Aliquam
                                            aliquid consequatur consequuntur deleniti nisi quos, ratione repudiandae
                                            sint!
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum dicta dolores
                                            dolorum eligendi, esse, facilis fugit hic impedit ipsam libero nisi
                                            obcaecati pariatur placeat soluta voluptatum. Aliquid officia quod
                                            veritatis!</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum dicta dolores
                                            dolorum eligendi, esse, facilis fugit hic impedit ipsam libero nisi
                                            obcaecati pariatur placeat soluta voluptatum. Aliquid officia quod
                                            veritatis! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum
                                            dicta dolores
                                            dolorum eligendi, esse, facilis fugit hic impedit ipsam libero nisi
                                            obcaecati pariatur placeat soluta voluptatum. Aliquid officia quod
                                            veritatis!</p>

                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum dicta dolores
                                            dolorum eligendi, esse, facilis fugit hic impedit ipsam libero nisi
                                            obcaecati pariatur placeat soluta voluptatum. Aliquid officia quod
                                            veritatis!</p>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Responsive>
                    </Container>
                </div> */}
            </div>
        );
    }
}

Page.propTypes = { 
    dispatch: PropTypes.func.isRequired 
};

export default Page;