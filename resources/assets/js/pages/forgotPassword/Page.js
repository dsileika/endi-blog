import React from 'react'
import {   
    Loader,
    Message } from 'semantic-ui-react'
import { Button } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
import { Validator } from 'ree-validate'
import ApiService from '../../services'
import PageHeader from '../../common/pageHeader'

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.validator = new Validator({
            email: 'required|email',
        });

        this.state = {
            credentials: {
                email: '',
            },
            responseError: {
                isError: false,
                code: '',
                text: ''
            },
            isSuccess: false,
            isLoading: false,
            errors: this.validator.errorBag
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;

        const {credentials} = this.state;
        credentials[name] = value;
        this.validator.validate(name, value)
            .then(() => {
                const {errorBag} = this.validator;
                this.setState({errors: errorBag, credentials})
            })
    }

    handleSubmit(event) {
        event.preventDefault();
        const {credentials} = this.state;
        this.validator.validateAll(credentials)
            .then(success => {
                if (success) {
                    this.setState({
                        isLoading: true
                    });
                    this.submit(credentials);
                }
            });
    }

    submit(credentials) {
        this.props.dispatch(ApiService.resetPassword(credentials))
            .then((result)  => {
                this.setState({
                    isLoading: false
                });
                this.setState({
                    isSuccess: true,
                });
            })
            .catch(({error, statusCode}) => {
                const responseError = {
                    isError: true,
                    code: statusCode,
                    text: error
                };
                this.setState({responseError});
                this.setState({
                    isLoading: false
                });
            })
    }

    componentDidMount(){
        this.setState({
            isLoading: false
        });
    }

    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { isAuthenticated } = this.props;

        if (isAuthenticated) {
            return (
                <Redirect to={from}/>
            )
        }
        const {errors} = this.state;

        return (
            <div>
            {/* Page Header */}
         <header className="masthead" style={{backgroundImage: 'url("/images/forestbridge.jpg")'}}>
         <div className="overlay" />
         <div className="container">
             <div className="row">
             <div className="col-lg-8 col-md-10 mx-auto">
                 <div className="site-heading" style={{padding: '41px'}}>  
                 </div>
             </div>
             </div>
         </div>
         </header>
          {/* Main Content */}
          <div className="container">
              <div className="row">
                <div className="col-lg-8 col-md-10 mx-auto">
                  <h2>Naujas slaptažodis</h2>
                  <span>Įveskite savo el. pašto adresą, kad galėtume jums nusiųsti slaptažodžio pakeitimo nuorodą.</span>
                {/* Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. */}
                {/* WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! */}
                {/* To use the contact form, your site must be on a live web host with PHP! The form will not work locally! */}
                
                        {this.state.responseError.isError && <Message negative>
                             <Message.Content>
                             <br/> {this.state.responseError.text} <br/> 
                             </Message.Content>
                         </Message>}
                         {this.state.isSuccess && <Message positive>
                             <Message.Content>
                             <br/> Jeigu el. pašto nuoradą pateikite teisingai, gausite slaptažodžio pakeitimo nuorodą.<br/> 
                            </Message.Content>
                         </Message>} 
                <form name="sentMessage" id="contactForm" noValidate>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>El. paštas</label>
                        <input type="email" className="form-control" onChange={this.handleChange} placeholder="El. paštas" name='email' required  />
                      
                        <p className="help-block text-danger" />
                      </div>
                    </div> 
                    <br />
                    <div id="success" />
                    <div className="form-group">
                      <button className="btn btn-primary" onClick={this.handleSubmit}>Gauti nuorodą</button>
                      <br/>
                      <Link to='/register' replace>Registruotis</Link> arba <Link to='/login' replace>Prisijungti</Link> 
                    </div>
                  </form>
                </div>
              </div>
            </div> 
         </div>
            // <div>
            //     <PageHeader heading="login"/>
            //     <Segment className='page-loader' style={{display: this.state.isLoading ? 'block' : 'none'}}>
            //         <Dimmer active inverted>
            //             <Loader size='large'>Resetting Password...</Loader>
            //         </Dimmer>
            //     </Segment>

            //     <Grid
            //         textAlign='center'
            //         verticalAlign='middle'
            //         className='login-form'
            //     >
            //         <Grid.Column style={{maxWidth: '450px'}}>
            //             <Header as='h2' color='teal' textAlign='center'>
            //                 Reset your password
            //             </Header>
            //             {this.state.responseError.isError && <Message negative>
            //                 <Message.Content>
            //                     {this.state.responseError.text}
            //                 </Message.Content>
            //             </Message>}
            //             {this.state.isSuccess && <Message positive>
            //                 <Message.Content>
            //                     If the email you entered exists, a reset link has been sent !
            //                 </Message.Content>
            //             </Message>}
            //             <Form size='large'>
            //                 <Segment stacked>
                                // <Form.Input
                                //     fluid
                                //     icon='user'
                                //     iconPosition='left'
                                //     name='email'
                                //     placeholder='E-mail address'
                                //     onChange={this.handleChange}
                                //     error={errors.has('email')}
                                // />
                                // {errors.has('email') && <Header size='tiny' className='custom-error' color='red'>
                                //     {errors.first('email')}
                                // </Header>}
            //                     <Button color='teal' fluid size='large' onClick={this.handleSubmit}>Reset Password</Button>
            //                 </Segment>
            //             </Form>
            //             <Message>
            //                 New to us? <Link to='/register' replace>Register</Link>
            //             </Message>
            //         </Grid.Column>
            //     </Grid>
            // </div>
        );
    }
}

Page.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
};

export default Page;
