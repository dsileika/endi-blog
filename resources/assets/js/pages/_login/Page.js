import React from 'react'
// import {
//     Button,
//     Divider,
//     Dimmer,
//     Form,
//     Grid,
//     Header,
//     Icon,
//     Loader,
//     Message,
//     Segment} from 'semantic-ui-react'
import { Button, Alert } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
import { Validator } from 'ree-validate'
import ApiService from '../../services'
import PageHeader from '../../common/pageHeader'
 
class Page extends React.Component {
    constructor(props) {
        super(props); 
        this.validator = new Validator({
            email: 'required|email',
            password: 'required|min:6'
        });  
        this.state = {
            credentials: {
                email: '',
                password: ''
            },
            responseError: {
                isError: false,
                code: '',
                text: ''
            },
            isLoading: false,
            errors: this.validator.errorBag,
            redirect: false,
            isTurn: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resizeinge = this.resizeing.bind(this);
         
    } 
    
    
    
    resizeing() {
        const {isTurn, redirect} = this.state; 
            if(!isTurn && $(window).width() > 991) {
                this.setState({redirect: true, isTurn: true});
                
            }  
    } 

    componentWillMount() {
        this.resizeinge;
    }
    componentDidMount(){
        const {isTurn} = this.state;
        this.setState({
            isLoading: false
        });
        window.addEventListener("resize", this.resizeinge, false);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.resizeinge, false);
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;

        const {credentials} = this.state;
        credentials[name] = value;
        this.validator.validate(name, value)
            .then(() => {
                const {errorBag} = this.validator;
                this.setState({errors: errorBag, credentials})
            })
    }

    handleSubmit(event) {
        event.preventDefault();
        const {credentials} = this.state;
        this.validator.validateAll(credentials)
            .then(success => {
                if (success) {
                    this.setState({
                        isLoading: true
                    });
                    this.submit(credentials);
                }
            });
    }

    submit(credentials) {
        this.props.dispatch(ApiService.login(credentials))
            .catch(({error, statusCode}) => {
                const responseError = {
                    isError: true,
                    code: statusCode,
                    text: error
                };
                this.setState({responseError});
                this.setState({
                    isLoading: false
                });
            })

    }

    popitup(url,windowName) {
        newwindow=window.open(url,windowName,'height=545,width=433');
        if (window.focus) {newwindow.focus()}
        return false;
    }

    PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;
    
        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    
        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }

    onSocialClick(event, data) {
       window.location.assign(`redirect/${event.as}`);
        // this.PopupCenter(`redirect/${event.as}`, "google", 545, 433);
    }



    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { isAuthenticated } = this.props; 
        const { redirect } = this.state;  

        if (isAuthenticated || redirect == true) {
            return (
                <Redirect to={from}/>
            )
        }
        const {errors} = this.state;

        return (
            <div>
               
               {/* Page Header */}
            <header className="masthead" style={{backgroundImage: 'url("/images/forestbridge.jpg")'}}>
            <div className="overlay" />
            <div className="container">
                <div className="row">
                <div className="col-lg-8 col-md-10 mx-auto">
                    <div className="site-heading" style={{padding: '41px'}}>  
                    </div>
                </div>
                </div>
            </div>
            </header>

            {/* Main Content */}
            <div className="container">
              <div className="row">
                <div className="col-lg-8 col-md-10 mx-auto">
                  <h2>Prisijungimas</h2>
                {/* Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. */}
                {/* WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! */}
                {/* To use the contact form, your site must be on a live web host with PHP! The form will not work locally! */}
                {this.state.responseError.isError &&
                             <div>
                                <Alert color="danger">
                                    {this.state.responseError.text}
                                </Alert> 
                             </div>
                    }
                <form name="sentMessage" id="contactForm" noValidate>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>El. paštas</label>
                        <input type="email" className="form-control" onChange={this.handleChange}  placeholder="El. paštas" name='email' required data-validation-required-message="Please enter your email address." />
                        <p className="help-block text-danger" />
                      </div>
                    </div>
                    <div className="control-group">
                      <div className="form-group floating-label-form-group controls">
                        <label>Slaptažodis</label>
                        <input type="password" className="form-control" onChange={this.handleChange} placeholder="Slaptažodis" name='password' required data-validation-required-message="Please enter your name." />
                        <p className="help-block text-danger" />
                      </div>
                    </div>
                    <br />
                    <div id="success" />
                    <div className="form-group">
                      <button onClick={this.handleSubmit}  className="btn btn-primary" id="sendMessageButton">Prisijungti</button>&nbsp;<Link to='/register' replace><button className="btn btn-secondary" >Registruotis</button></Link>
                      <br/>
                      <Link to='/forgot-password' replace>Pamiršai slaptažodį?</Link>
                      </div>
                      <hr/>
                      <div className="form-group">  
                        <Button onClick={this.onSocialClick.bind(this,{as:'google'})}> 
                             <span className="fa-stack fa-lg">
                {/* <i className="fa fa-circle fa-stack-2x" /> */}
                <i className="fa fa-google-plus fa-stack-2x fa-inverse" />
              </span>
                        </Button>
                    </div>
                  </form>
                </div>
              </div>
            </div> 
          </div> 
            // <div>
            //     <PageHeader heading="login"/>
            //     <Segment className='page-loader' style={{display: this.state.isLoading ? 'block' : 'none'}}>
            //         <Dimmer active inverted>
            //             <Loader size='large'>Authenticating...</Loader>
            //         </Dimmer>
            //     </Segment>

            //     <Grid
            //         textAlign='center'
            //         verticalAlign='middle'
            //         className='login-form'
            //     >
            //         <Grid.Column style={{maxWidth: '450px'}}>
            //             <Header as='h2' color='teal' textAlign='center'>
            //                 Login to your account
            //             </Header>
            //             {this.state.responseError.isError && <Message negative>
            //                 <Message.Content>
            //                     {this.state.responseError.text}
            //                 </Message.Content>
            //             </Message>}
            //             <Form size='large'>
            //                 <Segment stacked>
            //                     <Form.Input
            //                         fluid
            //                         icon='user'
            //                         iconPosition='left'
            //                         name='email'
            //                         placeholder='E-mail address'
            //                         onChange={this.handleChange}
            //                         error={errors.has('email')}
            //                     />
            //                     {errors.has('email') && <Header size='tiny' className='custom-error' color='red'>
            //                         {errors.first('email')}
            //                     </Header>}
            //                     <Form.Input
            //                         fluid
            //                         icon='lock'
            //                         iconPosition='left'
            //                         name='password'
            //                         placeholder='Password'
            //                         type='password'
            //                         onChange={this.handleChange}
            //                         error={errors.has('password')}
            //                     />
            //                     {errors.has('password') && <Header size='tiny' className='custom-error' color='red'>
            //                         {errors.first('password')}
            //                     </Header>}
            //                     <Button color='teal' fluid size='large' onClick={this.handleSubmit}>Login</Button>
            //                     <Link to='/forgot-password' replace>Forgot your password?</Link>
            //                      <div className="ui divider"></div>
            //                      <div>Or login with:</div><br/>
            //                     <Button onClick={this.onSocialClick.bind(this)} as="facebook" className="ui circular facebook icon button">
            //                       <i className="facebook icon"></i>
            //                     </Button>
            //                     <Button onClick={this.onSocialClick.bind(this)} as="twitter" className="ui circular twitter icon button">
            //                       <i className="twitter icon"></i>
            //                     </Button>
            //                     <Button onClick={this.onSocialClick.bind(this)} as="linkedin" className="ui circular linkedin icon button">
            //                      <i className="linkedin icon"></i>
            //                     </Button>
            //                     <Button onClick={this.onSocialClick.bind(this)} as="google" className="ui circular google plus icon button">
            //                       <i className="google plus icon"></i>
            //                     </Button>
            //                 </Segment>
            //             </Form>
            //             <Message>
            //                 New to us? <Link to='/register' replace>Register</Link>
            //             </Message>
            //         </Grid.Column>
            //     </Grid>
            // </div>
        );
    }
}

Page.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
};

export default Page;
