import * as ActionTypes from '../action-types'
import Http from '../../Http'

const user = {
    id: null,
    name: null,
    email: null,
    createdAt: null,
    updatedAt: null
};

const initialState = {
    isAuthenticated : false, 
    userName: ""
};

const keys = ["ASXc5uZPDJAjrfznHW63hMPS3Pm","NM3GAXK6BfeEpw44JyqAX5MgtbS","zrYrJc3Q4U5LSU9h5k3c83rNEgN"];
 

const Auth = (state= initialState,{type,payload = null}) => {
    switch(type){
        case ActionTypes.AUTH_LOGIN:
            return authLogin(state,payload);
        case ActionTypes.AUTH_CHECK:
            return checkAuth(state);
        case ActionTypes.AUTH_LOGOUT:
            return logout(state);
        default:
            return state;
    }
};

const authLogin = (state,payload) => {
    const jwtToken = payload.token;
    const user = payload.user[0];   

    localStorage.setItem('jwt_token',jwtToken); 

    if( payload.ADVE2Lyh5FfSws94WK3s && keys.includes(payload.ADVE2Lyh5FfSws94WK3s) ) {
        localStorage.setItem('ADVE2Lyh5FfSws94WK3s', payload.ADVE2Lyh5FfSws94WK3s); 
        state = Object.assign({}, state, {
            ADVE2Lyh5FfSws94WK3s: payload.ADVE2Lyh5FfSws94WK3s
        });
    }  
     

    Http.defaults.headers.common['Authorization'] = `Bearer ${jwtToken}`;
    state = Object.assign({}, state, {
        isAuthenticated: true, 
        userName: user.name 
    });
 

    return state;

};

const checkAuth = (state) => {   
    if(localStorage.getItem('ADVE2Lyh5FfSws94WK3s')) {  
        state = Object.assign({},state,{
            ADVE2Lyh5FfSws94WK3s: localStorage.getItem('ADVE2Lyh5FfSws94WK3s')
        });
    }  

    state = Object.assign({},state,{
        isAuthenticated : !!localStorage.getItem('jwt_token') 
    });
 
    if(state.isAuthenticated){
        Http.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('jwt_token')}`;
    }
    return state;
};

const logout = (state) => {
    localStorage.removeItem('jwt_token'); 

    if(localStorage.getItem('ADVE2Lyh5FfSws94WK3s')) { 
        localStorage.removeItem('ADVE2Lyh5FfSws94WK3s');
        state = Object.assign({},state,{
            ADVE2Lyh5FfSws94WK3s: null
        }); 
    }  
    
    state = Object.assign({},state,{
        isAuthenticated: false, 
        userName: null
    }); 
    return state;
};

export default Auth;
