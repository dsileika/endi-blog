import {applyMiddleware,createStore,compose} from 'redux'
import logger from 'redux-logger'
import RootReducer from './reducers'
import ReduxThunk from 'redux-thunk'
import {autoRehydrate,persistStore} from 'redux-persist'   


let middleware = [ ReduxThunk ]
if (process.env.NODE_ENV !== 'production') {
  middleware = [ ReduxThunk, logger ]
}  
 
const store = createStore(
    RootReducer,
    compose(
        applyMiddleware(...middleware),
        autoRehydrate()
    )
);
 
persistStore(store); 

export default store;