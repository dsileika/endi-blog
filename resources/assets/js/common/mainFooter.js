/**
 * Created by Sumit-Yadav on 12-10-2017.
 */
import React from 'react'
import {
    Button,
    Container,
    Grid,
    Header,
    Icon,
    List,
    Responsive
} from 'semantic-ui-react'

import {Link} from 'react-router-dom'

class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
              {/* Return to Top */}
                <div className="footerNavBtn">
                <a href="#down" className="btn return-to-bottom">
                  <i className="fa fa-angle-double-down animated"></i>
                </a>
                <a href="#top" className="btn return-to-top">
                  <i className="fa fa-angle-double-up animated"></i>
                </a>
                <a href="#top" className="btn return-to-comments">
                  <i className="fa fa-comments animated"><span className="post-badge badge badge-danger">0</span></i>
                </a> 
                </div>
                {/* Footer */}
                {/* <div id="down"/> */}
<footer>
  <div className="container">
    <div className="row">
      <div className="col-lg-8 col-md-10 mx-auto">
        {/* <ul className="list-inline text-center">
          <li className="list-inline-item">
            <a href="#">
              <span className="fa-stack fa-lg">
                <i className="fa fa-circle fa-stack-2x" />
                <i className="fa fa-twitter fa-stack-1x fa-inverse" />
              </span>
            </a>
          </li>
          <li className="list-inline-item">
            <a href="#">
              <span className="fa-stack fa-lg">
                <i className="fa fa-circle fa-stack-2x" />
                <i className="fa fa-facebook fa-stack-1x fa-inverse" />
              </span>
            </a>
          </li>
          <li className="list-inline-item">
            <a href="#">
              <span className="fa-stack fa-lg">
                <i className="fa fa-circle fa-stack-2x" />
                <i className="fa fa-github fa-stack-1x fa-inverse" />
              </span>
            </a>
          </li>
        </ul> */}
        <p className="copyright text-muted">© enDI 2018</p>
      </div>
    </div>
  </div>
</footer>

                {/* <Container>

                    <Grid columns="equal" verticalAlign="middle" className="foobar" stackable>
                        <Grid.Row>
                            <Grid.Column>
                                <Header as="h5" inverted>Endi @ 2018</Header>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container> */}
            </div>
        );
    }
}

export default Footer;