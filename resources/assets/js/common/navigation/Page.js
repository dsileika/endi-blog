/**
 * Created by Sumit-Yadav on 06-10-2017.
 */
import React from 'react'
import {NavLink, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Validator } from 'ree-validate'
import ApiService from '../../services'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert  } from 'reactstrap';
// import {
//     Button,
//     Container,
//     Dropdown,
//     Divider,
//     Image,
//     Menu,
//     Responsive,
//     Segment
// } from 'semantic-ui-react';
import * as actions from '../../store/actions'
import AuthModal from '../../components/AuthModal';
 

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.validator = new Validator({
            email: 'required|email',
            password: 'required|min:6'
        });

        this.state = {
            credentials: {
                email: '',
                password: ''
            },
            responseError: {
                isError: false,
                code: '',
                text: ''
            },
            isLoading: false,
            errors: this.validator.errorBag,
            modal: false
        }; 

        this.toggle = this.toggle.bind(this);
        this.handleLogout = this.handleLogout.bind(this); 
 
    }

    
    handleLogout(event) {
        event.preventDefault();
        this.props.dispatch(actions.authLogout());
        this.setState({
            modal: false
          });
    }

    toggle() {
        this.setState({
          modal: !this.state.modal,
          responseError: {
            isError: false,
            code: '',
            text: ''
          }
        });
    }  
    
    render() {

        const { isAuthenticated,dispatch } = this.props;  

        return (
            <div>
            <div id="top" />
             {/* Navigation */}
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div className="container">
        <Link className="navbar-brand scroll-trigger" to="/"><h1>enDI</h1></Link>
          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            
            <i className="fa fa-bars" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
              {/* <li className="nav-item">
              <Link className="nav-link scroll-trigger" to="/">Pagrindinis</Link>
              </li> */}
              {/* <li className="nav-item">
              <Link className="nav-link"  to="/about">Apie mus</Link>
              </li>  */}
              <li className="nav-item">
              {isAuthenticated ? 
                <div>
                    {/* <Button color="secondary" className="btn-outline-secondary-custom" style={{lineHeight: 0.3}} onClick={this.handleLogout}> </Button>{' '} */}
                    <div className="dropdown">
                        <a color="secondary" className="btn btn-outline-secondary-custom dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {this.props.userName}
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a className="dropdown-item" href="#" onClick={this.handleLogout}>Atsijungti</a> 
                        </div>
                    </div>

                </div>
                :
                    <AuthModal isAuthenticated={isAuthenticated} dispatch={dispatch} />
                }
              </li>
              {/* <li className="nav-item">
              <NavLink className="nav-link" to="/contact">Parašyk mums</NavLink>
              </li> */}
            </ul>
          </div>
        </div>
      </nav>
                {/* <Responsive as={Segment} inverted maxWidth={768} className="mobile-navbar">
                    <Menu size="large" inverted secondary>
                        <Menu.Item as={Link} to="/" className="logo" replace>
                            <img
                                src={require('../../../images/theme/infotiq-logo.png')} alt="infoTiq"/>
                        </Menu.Item>
                        <Menu.Menu position="right">
                            <Menu.Item>
                                <Dropdown icon="bars" className="collapsible-menu">
                                    <Dropdown.Menu className='bounceIn animated'>
                                        {this.props.isAuthenticated
                                            ?
                                            <Dropdown.Item onClick={this.handleLogout} text="logout" icon='sign out'
                                                           key='logout'/>
                                            :
                                            <div>
                                                <Dropdown.Item as={NavLink} to="/login" text="login"/>
                                                <Divider/>
                                                <Dropdown.Item as={NavLink} to="/register" text="register"/>
                                            </div>
                                        }
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Menu.Item>
                        </Menu.Menu>
                    </Menu>
                </Responsive>
                <Responsive as={Segment} inverted style={{marginBottom: '0', borderRadius: '0', padding: '1em 0em'}}
                            className="navbar" minWidth={769}>
                    <Menu inverted pointing secondary size='large'>
                        <Container>
                            <Menu.Item as={Link} to="/" className="logo" replace><h2>enDI</h2></Menu.Item>
                            <Menu.Item as={NavLink} to="/dashboard">Dashboard</Menu.Item>
                            <Menu.Menu position='right'>
                                {this.props.isAuthenticated
                                    ? <Dropdown trigger={this.avatar} pointing='top right' className='user-dropdown'>
                                        <Dropdown.Menu className='bounceIn animated'>
                                            <Dropdown.Item
                                                text={"Signed in as " + this.props.userName}
                                                disabled key='user'/>
                                            <Dropdown.Item onClick={this.handleLogout} text="logout" icon='sign out'
                                                           key='logout'/>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    : <Button.Group>
                                        <Button as={Link} to="/login" replace positive compact
                                                style={{lineHeight: '2em'}}>Login</Button>
                                        <Button.Or/>
                                        <Button as={Link} to="/register" replace color='blue' compact
                                                style={{lineHeight: '2em'}}>Register</Button>
                                    </Button.Group>
                                }
                            </Menu.Menu>
                        </Container>
                    </Menu>
                </Responsive> */}
            </div>
        );
    }
}

Page.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
};

export default Page;