// import libs
import {connect} from 'react-redux'

// import component
import Page from './Page'

const mapStateToProps = state => {
    console.log(state);
    return {
        isAuthenticated : state.Auth.isAuthenticated,
        userName : state.Auth.userName 
    }
};

export default connect(mapStateToProps)(Page)