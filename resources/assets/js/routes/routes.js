import Home from '../pages/home'
// import Login from '../pages/login'
import Auth from '../pages/auth'
import Register from '../pages/register'
import ForgotPassword from '../pages/forgotPassword'
import ResetPassword from '../pages/resetPassword'
import Dashboard from '../pages/dashboard'

import Post from '../pages/post'
import PostAuth from '../pages/post/dashboard'
import About from '../pages/about'
import Contact from '../pages/contact'
import Page404 from '../pages/404'

const routes = [
    {
        path: '/',
        exact: true,
        auth: false,
        component: Home
    },
    {
        path: '/main',
        exact: true,
        auth: false,
        component: Home
    },
    {
        path: '/auth/:social',
        exact: false,
        auth: false,
        component: Auth
    },
    {
        path: '/login',
        exact: true,
        auth: false,
        component: Auth
    },
    {
        path: '/register',
        exact: true,
        auth: false,
        component: Auth
    },
    {
        path: '/forgot-password',
        exact: true,
        auth: false,
        component: Auth
    },
    {
        path: '/reset-password/:token/:email',
        exact: true,
        auth: false,
        component: Auth
    },
    {
        path: '/dashboard',
        exact: true,
        auth: true,
        component: Dashboard
    },
    // Post route
    {
        path: '/post/:id',
        exact: true,
        auth: false,
        component: Post
    }, 
    // Write new post
    {
        path: '/new/post',
        exact: true,
        auth: true,
        component: PostAuth
    },
    // Edit post
    {
        path: '/post/:id/edit',
        exact: true,
        auth: true,
        component: PostAuth
    },
    // Delete post
    {
        path: '/post/:id/delete',
        exact: true,
        auth: true,
        component: PostAuth
    },
    //
    {
        path: '/about',
        exact: true,
        auth: false,
        component: About
    },
    {
        path: '/contact',
        exact: true,
        auth: false,
        component: Contact
    }, 
    {
        path: '*',
        exact: true,
        auth: false,
        component: Page404
    }
];

export default routes;