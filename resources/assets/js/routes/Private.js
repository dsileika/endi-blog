import React from 'react'
import {Route, Redirect} from 'react-router'
import {connect} from 'react-redux'
import Main from '../Main'


const PrivateRoute = ({component: Component,isAuthenticated, isAdmin, ...rest}) => (
    <Route {...rest} render={props => (
        isAuthenticated ? (
            <Main>
                <Component {...props}/>
            </Main>
        ) : (
            <Redirect to={{
                pathname: '/login',
                state: {from: props.location}
            }}/>
        )
    )}/>
);

let isAdmin = false;
const keys = ["ASXc5uZPDJAjrfznHW63hMPS3Pm","NM3GAXK6BfeEpw44JyqAX5MgtbS","zrYrJc3Q4U5LSU9h5k3c83rNEgN"];
let ADVE2Lyh5FfSws94WK3s = localStorage.getItem('ADVE2Lyh5FfSws94WK3s');

if( ADVE2Lyh5FfSws94WK3s && keys.includes(ADVE2Lyh5FfSws94WK3s) ) {
    isAdmin = true;
}

const mapStateToProps = (state) => {

    return {
        isAuthenticated : state.Auth.isAuthenticated,
        isAdmin: isAdmin
    }
};

export default connect(mapStateToProps)(PrivateRoute);