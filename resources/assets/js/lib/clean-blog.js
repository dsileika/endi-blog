(function($) {
 //your code here
 "use strict"; // Start of use strict 
 
$.fn.hasScrollBar = function() {
  return this.get(0).scrollHeight > this.height();
}


// $("body").on("click", 'a[href^="/"]', function(e) { 
//   $('html, body').animate({
//     scrollTop: 0
//   }, 500, function(){ 
//   }); 
// });

$("body").on("click",'a.scroll-trigger', function(e) {
    $('html, body').animate({
      scrollTop: 0
    }, 500); 
});

$("body").on("click", 'a[href^="#"]', function(e) { 

  // prevent default anchor click behavior
  e.preventDefault();

  // $('.return-to-top').click(function() {      // When arrow is clicked 
  //   $('body,html').animate({
  //       scrollTop : 0                       // Scroll to top of body
  //   }, 500);
  //   // return false;
  // });

if (this.hash.length != 0) { 
  // animate
  $('html, body').animate({
      scrollTop: (this.hash) ? ($(this.hash).offset().top - 130) : 0
    }, 500, function(){

      // when done, add hash to url
      // (default click behaviour)
      // window.location.hash = this.hash;
      // return false;
    }); 
}

});



// Closes responsive menu when a scroll trigger link is clicked
$('.js-scroll-trigger').click(function() {
  $(".navbar-collapse").removeClass("show"); 
});

// var MQL = 992;
var MQL = 500;

 // Floating label headings for the contact form
 $("body").on("input propertychange", ".floating-label-form-group", function(e) {
   $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
 }).on("focus", ".floating-label-form-group", function() {
   $(this).addClass("floating-label-form-group-with-focus");
 }).on("blur", ".floating-label-form-group", function() {
   $(this).removeClass("floating-label-form-group-with-focus");
 });
 

 //primary navigation slide-in effect
 if ($(window).width() > MQL) {

   var headerHeight = $('#mainNav').height();
   $(window).on('scroll', {
       previousTop: 0
     },
     function() {
       var currentTop = $(window).scrollTop();
       //check if user is scrolling down
       if (currentTop < this.previousTop) { 
         //if scrolling up...
         if (currentTop > 0 && $('#mainNav').hasClass('is-fixed')) {
           $('#mainNav').addClass('is-visible');
         } else {
           $('#mainNav').removeClass('is-visible is-fixed');
         }
       } else if (currentTop > this.previousTop) {
         //if scrolling down...
        //  $('#mainNav').removeClass('is-visible');
        $('#mainNav').addClass('is-visible');
         if (currentTop > headerHeight && !$('#mainNav').hasClass('is-fixed')) $('#mainNav').addClass('is-fixed');
       }
       this.previousTop = currentTop;
     });
 }
 



 // Arrow to up
 // ===== Scroll to Top ==== 
 $(window).scroll(function() {
  

   if ($(this).scrollTop() >= 400 ) {        // If page is scrolled more than 400px
       $('.return-to-top').fadeIn(200);    // Fade in the arrow
   } else {
       $('.return-to-top').fadeOut(300);   // Else fade out the arrow
      }  
 
   if ($(this).scrollTop() >= 300) {
       // Close navbar after scroll down
       $(".navbar-collapse").removeClass("show"); 
   } 

  // if($(this).scrollTop() >= ($(this).height() - 150)){
  //   $('.return-to-bottom').fadeOut(300);   // Else fade out the arrow
  // } else {
  //   $('.return-to-bottom').fadeIn(200);    // Fade in the arrow
  // }
   
 });
 $(window).resize(function() { 
    
    // Close navbar after scroll down
    $(".navbar-collapse").removeClass("show");  
 });

$(document).on("click",function() { 
//  $(".navbar-collapse").removeClass("show"); 
});




})(jQuery); // End of use strict

 

 