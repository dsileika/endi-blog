(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("resize-observer-polyfill"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "resize-observer-polyfill"], factory);
	else if(typeof exports === 'object')
		exports["AudioPlayer"] = factory(require("prop-types"), require("react"), require("resize-observer-polyfill"));
	else
		root["AudioPlayer"] = factory(root["PropTypes"], root["React"], root["ResizeObserver"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__0__, __WEBPACK_EXTERNAL_MODULE__1__, __WEBPACK_EXTERNAL_MODULE__5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__0__;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__1__;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2016 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(null, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
}());


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

module.exports = function (arr, predicate, ctx) {
	if (typeof Array.prototype.findIndex === 'function') {
		return arr.findIndex(predicate, ctx);
	}

	if (typeof predicate !== 'function') {
		throw new TypeError('predicate must be a function');
	}

	var list = Object(arr);
	var len = list.length;

	if (len === 0) {
		return -1;
	}

	for (var i = 0; i < len; i++) {
		if (predicate.call(ctx, list[i], i, list)) {
			return i;
		}
	}

	return -1;
};


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



// Older versions of React do not support static getDerivedStateFromProps.
// As a workaround, use cWM and cWRP to invoke the new static lifecycle.
// Newer versions of React will ignore these methods if gDSFP exists.
function componentWillMount() {
  // Call this.constructor.gDSFP to support sub-classes.
  var state = this.constructor.getDerivedStateFromProps(this.props, this.state);
  if (state !== null && state !== undefined) {
    this.setState(state);
  }
}

function componentWillReceiveProps(nextProps) {
  // Call this.constructor.gDSFP to support sub-classes.
  var state = this.constructor.getDerivedStateFromProps(nextProps, this.state);
  if (state !== null && state !== undefined) {
    this.setState(state);
  }
}

// React may warn about cWM/cWRP/cWU methods being deprecated.
// Add a flag to suppress these warnings for this special case.
componentWillMount.__suppressDeprecationWarning = true;
componentWillReceiveProps.__suppressDeprecationWarning = true;

module.exports = function polyfill(Component) {
  if (!Component.prototype || !Component.prototype.isReactComponent) {
    throw new Error('Can only polyfill class components');
  }

  if (typeof Component.getDerivedStateFromProps === 'function') {
    if (typeof Component.prototype.componentWillMount === 'function') {
      throw new Error('Cannot polyfill if componentWillMount already exists');
    } else if (
      typeof Component.prototype.componentWillReceiveProps === 'function'
    ) {
      throw new Error(
        'Cannot polyfill if componentWillReceiveProps already exists'
      );
    }

    Component.prototype.componentWillMount = componentWillMount;
    Component.prototype.componentWillReceiveProps = componentWillReceiveProps;
  }

  return Component;
};


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__5__;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(1);

function ShuffleIcon (props) {
    return React.createElement("svg",props,[React.createElement("path",{"d":"M0 0h48v48H0z","fill":"none","key":0}),React.createElement("path",{"d":"M21.17 18.34L10.83 8 8 10.83l10.34 10.34 2.83-2.83zM29 8l4.09 4.09L8 37.17 10.83 40l25.09-25.09L40 19V8H29zm.66 18.83l-2.83 2.83 6.26 6.26L29 40h11V29l-4.09 4.09-6.25-6.26z","key":1})]);
}

ShuffleIcon.displayName = "ShuffleIcon";

ShuffleIcon.defaultProps = {"width":"48","height":"48","viewBox":"0 0 48 48"};

module.exports = ShuffleIcon;

ShuffleIcon.default = ShuffleIcon;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(1);

function RepeatOneIcon (props) {
    return React.createElement("svg",props,[React.createElement("path",{"d":"M0 0h48v48H0z","fill":"none","key":0}),React.createElement("path",{"d":"M14 14h20v6l8-8-8-8v6H10v12h4v-8zm20 20H14v-6l-8 8 8 8v-6h24V26h-4v8zm-8-4V18h-2l-4 2v2h3v8h3z","key":1})]);
}

RepeatOneIcon.displayName = "RepeatOneIcon";

RepeatOneIcon.defaultProps = {"width":"48","height":"48","viewBox":"0 0 48 48"};

module.exports = RepeatOneIcon;

RepeatOneIcon.default = RepeatOneIcon;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(1);

function RepeatIcon (props) {
    return React.createElement("svg",props,[React.createElement("path",{"d":"M0 0h48v48H0z","fill":"none","key":0}),React.createElement("path",{"d":"M14 14h20v6l8-8-8-8v6H10v12h4v-8zm20 20H14v-6l-8 8 8 8v-6h24V26h-4v8z","key":1})]);
}

RepeatIcon.displayName = "RepeatIcon";

RepeatIcon.defaultProps = {"width":"48","height":"48","viewBox":"0 0 48 48"};

module.exports = RepeatIcon;

RepeatIcon.default = RepeatIcon;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _implementation = __webpack_require__(19);

var _implementation2 = _interopRequireDefault(_implementation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _react2.default.createContext || _implementation2.default;
module.exports = exports['default'];

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external {"root":"React","commonjs":"react","commonjs2":"react","amd":"react"}
var external_root_React_commonjs_react_commonjs2_react_amd_react_ = __webpack_require__(1);
var external_root_React_commonjs_react_commonjs2_react_amd_react_default = /*#__PURE__*/__webpack_require__.n(external_root_React_commonjs_react_commonjs2_react_amd_react_);

// EXTERNAL MODULE: external {"root":"PropTypes","commonjs":"prop-types","commonjs2":"prop-types","amd":"prop-types"}
var external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_ = __webpack_require__(0);
var external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default = /*#__PURE__*/__webpack_require__.n(external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_);

// EXTERNAL MODULE: ./node_modules/classnames/index.js
var classnames = __webpack_require__(2);
var classnames_default = /*#__PURE__*/__webpack_require__.n(classnames);

// EXTERNAL MODULE: ./node_modules/react-lifecycles-compat/index.js
var react_lifecycles_compat = __webpack_require__(4);
var react_lifecycles_compat_default = /*#__PURE__*/__webpack_require__.n(react_lifecycles_compat);

// EXTERNAL MODULE: ./node_modules/array-find-index/index.js
var array_find_index = __webpack_require__(3);
var array_find_index_default = /*#__PURE__*/__webpack_require__.n(array_find_index);

// EXTERNAL MODULE: ./node_modules/create-react-15-context/lib/index.js
var lib = __webpack_require__(9);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// CONCATENATED MODULE: ./src/PlayerContext.js

const PlayerContext_PlayerContext = lib_default()(null);
PlayerContext_PlayerContext.Provider.displayName = 'PlayerContext.Provider';
PlayerContext_PlayerContext.Consumer.displayName = 'PlayerContext.Consumer';
/* harmony default export */ var src_PlayerContext = (PlayerContext_PlayerContext);
// CONCATENATED MODULE: ./src/constants.js
const repeatStrategyOptions = ['none', 'playlist', 'track'];
// CONCATENATED MODULE: ./src/utils/console.js
const log = console.log.bind(console);
const logError = console.error ? console.error.bind(console) : log;
const logWarning = console.warn ? console.warn.bind(console) : log;
// CONCATENATED MODULE: ./src/PlayerPropTypes.js




function PlayerPropTypes_requiredOnlyUnlessHasProp(propType, altPropName) {
  let warnedAboutDefiningBoth = false;

  function validate(props, propName, componentName, ...rest) {
    if (propName in props) {
      if (!warnedAboutDefiningBoth && altPropName in props) {
        logWarning(`Do not define both the '${propName}' and '${altPropName}' props.`);
        warnedAboutDefiningBoth = true;
      }

      return propType.isRequired(props, propName, componentName, ...rest);
    }

    if (!(altPropName in props)) {
      return new Error(`If the '${altPropName}' prop is not defined, '${propName}' must be.`);
    }
  }

  return validate;
}

const PlayerPropTypes_controlKeyword = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.oneOf(['playpause', 'backskip', 'forwardskip', 'volume', 'repeat', 'shuffle', 'progress', 'progressdisplay', 'spacer']);
const PlayerPropTypes_control = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.oneOfType([external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func, PlayerPropTypes_controlKeyword]);
const PlayerPropTypes_crossOriginAttribute = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.oneOf(['anonymous', 'use-credentials']);
const PlayerPropTypes_repeatStrategy = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.oneOf(repeatStrategyOptions);
const PlayerPropTypes_audioSource = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.shape({
  src: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string.isRequired,
  type: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string.isRequired
});
const PlayerPropTypes_mediaSessionAction = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.oneOf(['play', 'pause', 'previoustrack', 'nexttrack', 'seekbackward', 'seekforward']);
const PlayerPropTypes_mediaSessionArtwork = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.shape({
  src: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string.isRequired,
  sizes: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  type: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string
});
const PlayerPropTypes_track = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.shape({
  url: PlayerPropTypes_requiredOnlyUnlessHasProp(external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string, 'sources'),
  sources: PlayerPropTypes_requiredOnlyUnlessHasProp(external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_audioSource.isRequired), 'url'),
  title: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string.isRequired,
  web: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  artist: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  album: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  artwork: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_mediaSessionArtwork.isRequired),
  meta: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object
});
const PlayerPropTypes_progressDirection = external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.oneOf(['left', 'right', 'up', 'down']);
// CONCATENATED MODULE: ./src/controls/AudioControlBar.js



class AudioControlBar_AudioControlBar extends external_root_React_commonjs_react_commonjs2_react_amd_react_["Component"] {
  render() {
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "rrap",
      title: this.props.title
    }, this.props.children);
  }

}

AudioControlBar_AudioControlBar.propTypes = {
  title: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  children: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.node.isRequired
};
/* harmony default export */ var controls_AudioControlBar = (AudioControlBar_AudioControlBar);
// CONCATENATED MODULE: ./src/factories/createCustomAudioElement.js
const loopchange = 'loopchange';
const srcrequest = 'srcrequest';

function createCustomAudioElement(audio = document.createElement('audio')) {
  new MutationObserver(() => {
    audio.dispatchEvent(new Event(loopchange));
  }).observe(audio, {
    attributeFilter: ['loop']
  }); // Don't let the audio src property get modified directly.
  // Instead, when it does get set, dispatch an event to be
  // handled in a way that doesn't conflict with the loaded
  // playlist.

  Object.defineProperty(audio, 'src', {
    get: () => audio.currentSrc,
    set: src => {
      const e = new Event(srcrequest);
      e.srcRequested = src;
      audio.dispatchEvent(e);
    }
  });
  return audio;
}

/* harmony default export */ var factories_createCustomAudioElement = (createCustomAudioElement);
// CONCATENATED MODULE: ./src/utils/ShuffleManager.js
/* ShuffleManager
 *
 * Manages navigation throughout a list which is:
 * - Sourced from another provided list
 * - In random order (except to avoid consecutive duplicates)
 * - Extended endlessly on-the-fly, as needed
 * - Able to have future history overwritten by non-random choices
 * - Able to swap source lists and maintain shuffle order for common members
 */
class ShuffleManager {
  constructor(list, options = {}) {
    this._list = list;
    this._forwardStack = [];
    this._backStack = [];
    this._currentItem = undefined;
    this._allowBackShuffle = Boolean(options.allowBackShuffle);
  }

  findNextItem(currentIndex) {
    if (currentIndex !== undefined) {
      this.setCurrentIndex(currentIndex);
    }

    this._currentItem = _findNextItem(this._list, this._forwardStack, this._backStack, this._currentItem, true);
    return this._currentItem;
  }

  findPreviousItem(currentIndex) {
    if (currentIndex !== undefined) {
      this.setCurrentIndex(currentIndex);
    }

    this._currentItem = _findNextItem(this._list, this._backStack, this._forwardStack, this._currentItem, this._allowBackShuffle);
    return this._currentItem;
  }

  pickNextItem(index, currentIndex) {
    if (currentIndex !== undefined) {
      this.setCurrentIndex(currentIndex);
    }

    if (this._list[index] === undefined) {
      return undefined;
    }

    if (this._currentItem !== undefined) {
      this._backStack.push(this._currentItem);
    }

    this._forwardStack.length = 0;
    this._currentItem = this._list[index];
    return this._currentItem;
  }

  setList(list) {
    this._list = list;
  }

  setOptions(options) {
    for (const o of Object.keys(options)) {
      switch (o) {
        case 'allowBackShuffle':
          this[`_${o}`] = Boolean(options[o]);
          break;

        default:
          break;
      }
    }
  }

  setCurrentIndex(currentIndex) {
    const item = this._list[currentIndex];

    if (this._currentItem !== item) {
      this.clear();
      this._currentItem = item;
    }
  }

  clear() {
    this._forwardStack.length = 0;
    this._backStack.length = 0;
    this._currentItem = undefined;
  }

}

function _goForward(n, forwardStack, backStack, currentItem) {
  let item = currentItem;

  for (let i = 0; i < n; i++) {
    if (!forwardStack.length) {
      // rollback before erroring (note stack reversal)
      _goForward(i, backStack, forwardStack, item);

      throw `Moving ${n} places was not possible!`;
    }

    backStack.push(item);
    item = forwardStack.pop();
  }

  return item;
}

function _allItemsMatch(list, item) {
  if (!list.length) {
    return false;
  }

  for (let i = 0; i < list.length; i++) {
    if (item !== list[i]) {
      return false;
    }
  }

  return true;
}

function _findNextItem(list, forwardStack, backStack, currentItem, allowMore) {
  let item = currentItem;

  if (!list.length) {
    return undefined;
  }

  for (let i = 1; i <= forwardStack.length; i++) {
    if (list.indexOf(forwardStack[forwardStack.length - i]) !== -1) {
      return _goForward(i, forwardStack, backStack, item);
    }
  }

  if (!allowMore) {
    return undefined;
  }

  if (_allItemsMatch(list, item)) {
    // we can serve this as our "next" item but we
    // won't modify our history since it's the same.
    return item;
  }

  let nextItem;

  do {
    nextItem = list[Math.floor(Math.random() * list.length)];
  } while (item === nextItem || nextItem === undefined); // if we're skipping items that aren't in our current list we may
  // have some items in our forwardStack - make sure we move to the front.


  item = _goForward(forwardStack.length, forwardStack, backStack, item);

  if (item !== undefined) {
    backStack.push(item);
  }

  return nextItem;
}

/* harmony default export */ var utils_ShuffleManager = (ShuffleManager);
// CONCATENATED MODULE: ./src/utils/isPlaylistValid.js
function isPlaylistValid(playlist) {
  return Boolean(playlist && playlist.length);
}

/* harmony default export */ var utils_isPlaylistValid = (isPlaylistValid);
// CONCATENATED MODULE: ./src/utils/getTrackSources.js

const getTrackSources_blankSources = [{
  src: ''
}];

function getTrackSources_getTrackSources(playlist, index) {
  if (!utils_isPlaylistValid(playlist)) {
    return getTrackSources_blankSources;
  }

  const _playlist$index = playlist[index],
        sources = _playlist$index.sources,
        url = _playlist$index.url;

  if (sources) {
    return sources.length ? sources : getTrackSources_blankSources;
  }

  return [{
    src: url
  }];
}

/* harmony default export */ var utils_getTrackSources = (getTrackSources_getTrackSources);
// CONCATENATED MODULE: ./src/utils/getSourceList.js
 // collapses playlist into flat list containing
// the first source url for each track

function getSourceList_getSourceList(playlist) {
  return (playlist || []).map((_, i) => utils_getTrackSources(playlist, i)[0].src);
}

/* harmony default export */ var utils_getSourceList = (getSourceList_getSourceList);
// CONCATENATED MODULE: ./src/utils/findTrackIndexByUrl.js


function findTrackIndexByUrl_findTrackIndexByUrl(playlist, url) {
  return array_find_index_default()(playlist, track => {
    if (track.sources) {
      return array_find_index_default()(track.sources, source => source.src === url) !== -1;
    }

    return track.url && url === track.url;
  });
}

/* harmony default export */ var utils_findTrackIndexByUrl = (findTrackIndexByUrl_findTrackIndexByUrl);
// CONCATENATED MODULE: ./src/utils/getDisplayText.js


function getDisplayText(track, textFormat = true) {
  if (!track) {
    return '';
  } // if (track.displayText) {
  //   // TODO: Remove this check when support for the displayText prop is gone.
  //   return track.displayText;
  // } 


  if (track.title && track.web && !textFormat) {
    return React.createElement("a", {
      href: track.web,
      target: "_blank"
    }, track.title);
  }

  if (track.title && track.artist) {
    return `${track.artist} - ${track.title}`;
  }

  return track.title || track.artist || track.album || '';
}

/* harmony default export */ var utils_getDisplayText = (getDisplayText);
// CONCATENATED MODULE: ./src/utils/getRepeatStrategy.js
function getRepeatStrategy(loop, cycle) {
  if (loop) {
    return 'track';
  }

  if (cycle) {
    return 'playlist';
  }

  return 'none';
}

/* harmony default export */ var utils_getRepeatStrategy = (getRepeatStrategy);
// CONCATENATED MODULE: ./src/factories/createControlRenderProp.js
 // Component: A React Component accepting a subset of playerContext props
// propNames: An array listing names of props to pass

function createControlRenderProp_createControlRenderProp(Component, propNames) {
  return function renderControl(playerContext) {
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(Component, propNames.reduce((props, p) => {
      props[p] = playerContext[p];
      return props;
    }, {}));
  };
}

/* harmony default export */ var factories_createControlRenderProp = (createControlRenderProp_createControlRenderProp);
// CONCATENATED MODULE: ./src/controls/PlayPauseButton.js





class PlayPauseButton_PlayPauseButton extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    const _props = this.props,
          paused = _props.paused,
          awaitingResumeOnSeekComplete = _props.awaitingResumeOnSeekComplete,
          onTogglePause = _props.onTogglePause;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("button", {
      className: classnames_default()('rrap__play_pause_button rrap__audio_button', {
        paused: paused && !awaitingResumeOnSeekComplete
      }),
      onClick: onTogglePause
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "play_pause_inner foreground"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "left"
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "right"
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "triangle_1"
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "triangle_2"
    })));
  }

}

PlayPauseButton_PlayPauseButton.propTypes = {
  paused: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  awaitingResumeOnSeekComplete: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  onTogglePause: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const PlayPauseButton_renderPlayPauseButton = factories_createControlRenderProp(PlayPauseButton_PlayPauseButton, ['paused', 'awaitingResumeOnSeekComplete', 'onTogglePause']);
/* harmony default export */ var controls_PlayPauseButton = (PlayPauseButton_PlayPauseButton);
// CONCATENATED MODULE: ./src/controls/common/SkipButton.js




class SkipButton_SkipButton extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    const _props = this.props,
          back = _props.back,
          onClick = _props.onClick;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("button", {
      className: classnames_default()('rrap__skip_button rrap__audio_button', {
        back
      }),
      onClick: onClick
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "skip_button_inner foreground"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "right_facing_triangle"
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "right_facing_triangle"
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "vertical_bar"
    })));
  }

}

SkipButton_SkipButton.propTypes = {
  back: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  onClick: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
/* harmony default export */ var common_SkipButton = (SkipButton_SkipButton);
// CONCATENATED MODULE: ./src/controls/BackSkipButton.js





class BackSkipButton_BackSkipButton extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_SkipButton, {
      back: true,
      onClick: this.props.onBackSkip
    });
  }

}

BackSkipButton_BackSkipButton.propTypes = {
  onBackSkip: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const BackSkipButton_renderBackSkipButton = factories_createControlRenderProp(BackSkipButton_BackSkipButton, ['onBackSkip']);
/* harmony default export */ var controls_BackSkipButton = (BackSkipButton_BackSkipButton);
// CONCATENATED MODULE: ./src/controls/ForwardSkipButton.js





class ForwardSkipButton_ForwardSkipButton extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_SkipButton, {
      onClick: this.props.onForwardSkip
    });
  }

}

ForwardSkipButton_ForwardSkipButton.propTypes = {
  onForwardSkip: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const ForwardSkipButton_renderForwardSkipButton = factories_createControlRenderProp(ForwardSkipButton_ForwardSkipButton, ['onForwardSkip']);
/* harmony default export */ var controls_ForwardSkipButton = (ForwardSkipButton_ForwardSkipButton);
// CONCATENATED MODULE: ./src/utils/getProgressStyle.js
function getProgressStyle(progress, progressDirection) {
  const progressAheadPercentage = `${(1 - (progress || 0)) * 100}%`;
  const style = {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  };

  switch (progressDirection) {
    case 'left':
      style.left = progressAheadPercentage;
      style.willChange = 'left';
      break;

    case 'right':
      style.right = progressAheadPercentage;
      style.willChange = 'right';
      break;

    case 'up':
      style.top = progressAheadPercentage;
      style.willChange = 'top';
      break;

    case 'down':
      style.bottom = progressAheadPercentage;
      style.willChange = 'bottom';
      break;

    default:
      break;
  }

  return style;
}

/* harmony default export */ var utils_getProgressStyle = (getProgressStyle);
// CONCATENATED MODULE: ./src/utils/getHandleStyle.js
function getHandleStyle(progress, progressDirection) {
  const progressPercentage = `${(progress || 0) * 100}%`;
  const style = {
    position: 'absolute',
    transform: 'translate(-50%, -50%)'
  };

  switch (progressDirection) {
    case 'left':
      style.top = '50%';
      style.right = progressPercentage;
      style.willChange = 'right';
      style.transform = 'translate(50%, -50%)';
      break;

    case 'right':
      style.top = '50%';
      style.left = progressPercentage;
      style.willChange = 'left';
      style.transform = 'translate(-50%, -50%)';
      break;

    case 'up':
      style.left = '50%';
      style.bottom = progressPercentage;
      style.willChange = 'bottom';
      style.transform = 'translate(-50%, 50%)';
      break;

    case 'down':
      style.left = '50%';
      style.top = progressPercentage;
      style.willChange = 'top';
      style.transform = 'translate(-50%, -50%)';
      break;

    default:
      break;
  }

  return style;
}

/* harmony default export */ var utils_getHandleStyle = (getHandleStyle);
// CONCATENATED MODULE: ./src/controls/common/ProgressBarDisplay.js
function ProgressBarDisplay_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { ProgressBarDisplay_defineProperty(target, key, source[key]); }); } return target; }

function ProgressBarDisplay_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class ProgressBarDisplay_ProgressBarDisplay extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    const _props = this.props,
          className = _props.className,
          progressClassName = _props.progressClassName,
          style = _props.style,
          progressStyle = _props.progressStyle,
          progress = _props.progress,
          progressDirection = _props.progressDirection,
          handle = _props.handle,
          onMouseDown = _props.onMouseDown,
          onTouchStart = _props.onTouchStart,
          onClick = _props.onClick,
          progressBarRef = _props.progressBarRef;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      ref: progressBarRef,
      className: className,
      style: style,
      onMouseDown: onMouseDown,
      onTouchStart: onTouchStart,
      onClick: onClick
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      style: {
        position: 'relative',
        width: '100%',
        height: '100%',
        touchAction: 'none'
      }
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: progressClassName,
      style: ProgressBarDisplay_objectSpread({}, utils_getProgressStyle(progress, progressDirection), progressStyle || {})
    }), handle && external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      style: utils_getHandleStyle(progress, progressDirection)
    }, handle)));
  }

}

ProgressBarDisplay_ProgressBarDisplay.propTypes = {
  className: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  progressClassName: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  style: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object,
  progressStyle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object,
  progress: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  // progressDirection: PlayerPropTypes.progressDirection.isRequired,
  progressDirection: PlayerPropTypes_progressDirection,
  handle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.element,
  onMouseDown: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func,
  onTouchStart: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func,
  onClick: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func,
  progressBarRef: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func
};
/* harmony default export */ var common_ProgressBarDisplay = (ProgressBarDisplay_ProgressBarDisplay);
// CONCATENATED MODULE: ./src/utils/convertToNumberWithinIntervalBounds.js
function convertToNumberWithinIntervalBounds(number, min, max) {
  min = typeof min === 'number' ? min : -Infinity;
  max = typeof max === 'number' ? max : Infinity;
  return Math.max(min, Math.min(number, max));
}

/* harmony default export */ var utils_convertToNumberWithinIntervalBounds = (convertToNumberWithinIntervalBounds);
// CONCATENATED MODULE: ./src/controls/common/ProgressBar.js






class ProgressBar_ProgressBar extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  constructor(props) {
    super(props);
    this.progressContainer = null; // bind methods fired on React events

    this.setProgressContainerRef = this.setProgressContainerRef.bind(this);
    this.handleAdjustProgress = this.handleAdjustProgress.bind(this); // bind listeners to add on mount and remove on unmount

    this.handleAdjustComplete = this.handleAdjustComplete.bind(this);
  }

  componentDidMount() {
    // add event listeners bound outside the scope of our component
    window.addEventListener('mousemove', this.handleAdjustProgress);
    document.addEventListener('touchmove', this.handleAdjustProgress);
    window.addEventListener('mouseup', this.handleAdjustComplete);
    document.addEventListener('touchend', this.handleAdjustComplete);
  }

  componentWillUnmount() {
    // remove event listeners bound outside the scope of our component
    window.removeEventListener('mousemove', this.handleAdjustProgress);
    document.removeEventListener('touchmove', this.handleAdjustProgress);
    window.removeEventListener('mouseup', this.handleAdjustComplete);
    document.removeEventListener('touchend', this.handleAdjustComplete);
  }

  setProgressContainerRef(ref) {
    this.progressContainer = ref;
  }

  getProgressFromPageCoordinates(pageX, pageY) {
    const _progressContainer$ge = this.progressContainer.getBoundingClientRect(),
          left = _progressContainer$ge.left,
          top = _progressContainer$ge.top,
          width = _progressContainer$ge.width,
          height = _progressContainer$ge.height;

    const _document$body = document.body,
          scrollLeft = _document$body.scrollLeft,
          scrollTop = _document$body.scrollTop;

    switch (this.props.progressDirection) {
      case 'down':
        return (pageY - top - scrollTop) / height;

      case 'left':
        return 1 - (pageX - left - scrollLeft) / width;

      case 'up':
        return 1 - (pageY - top - scrollTop) / height;

      case 'right':
      default:
        return (pageX - left - scrollLeft) / width;
    }
  }

  handleAdjustProgress(event) {
    const _props = this.props,
          readonly = _props.readonly,
          adjusting = _props.adjusting,
          onAdjustProgress = _props.onAdjustProgress;

    if (readonly) {
      return;
    } // make sure we don't select stuff in the background


    if (event.type === 'mousedown' || event.type === 'touchstart') {
      document.body.classList.add('rrap__noselect');
    } else if (!adjusting) {
      return;
    }
    /* we don't want mouse handlers to receive the event
     * after touch handlers, so prevent default
     */


    event.preventDefault();
    const isTouch = event.type.slice(0, 5) === 'touch';
    const pageX = isTouch ? event.targetTouches.item(0).pageX : event.pageX;
    const pageY = isTouch ? event.targetTouches.item(0).pageY : event.pageY;
    const progress = this.getProgressFromPageCoordinates(pageX, pageY);
    const progressInBounds = utils_convertToNumberWithinIntervalBounds(progress, 0, 1);
    onAdjustProgress(progressInBounds);
  }

  handleAdjustComplete(event) {
    const _props2 = this.props,
          adjusting = _props2.adjusting,
          onAdjustComplete = _props2.onAdjustComplete;
    /* this function is activated when the user lets go of
     * the mouse, so if .rrap__noselect was applied
     * to the document body, get rid of it.
     */

    document.body.classList.remove('rrap__noselect');

    if (!adjusting) {
      return;
    }
    /* we don't want mouse handlers to receive the event
     * after touch handlers, so prevent default
     */


    event.preventDefault();

    if (typeof onAdjustComplete === 'function') {
      onAdjustComplete();
    }
  }

  render() {
    const _props3 = this.props,
          className = _props3.className,
          progressClassName = _props3.progressClassName,
          style = _props3.style,
          progressStyle = _props3.progressStyle,
          progress = _props3.progress,
          progressDirection = _props3.progressDirection,
          handle = _props3.handle;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_ProgressBarDisplay, {
      progressBarRef: this.setProgressContainerRef,
      className: className,
      progressClassName: progressClassName,
      style: style,
      progressStyle: progressStyle,
      progress: progress,
      progressDirection: progressDirection,
      handle: handle,
      onMouseDown: this.handleAdjustProgress,
      onTouchStart: this.handleAdjustProgress
    });
  }

}

ProgressBar_ProgressBar.propTypes = {
  className: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  progressClassName: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  style: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object,
  progressStyle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object,
  progress: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  // progressDirection: PlayerPropTypes.progressDirection.isRequired,
  progressDirection: PlayerPropTypes_progressDirection,
  handle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.element,
  adjusting: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  readonly: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  onAdjustProgress: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired,
  onAdjustComplete: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
ProgressBar_ProgressBar.defaultProps = {
  readonly: false
};
/* harmony default export */ var common_ProgressBar = (ProgressBar_ProgressBar);
// CONCATENATED MODULE: ./src/utils/getVolumeIconClassName.js
function getVolumeIconClassName(volume, muted) {
  if (muted) {
    return 'icono-volumeMute';
  }

  if (volume === 0) {
    return 'icono-volume';
  }

  if (volume <= 1 / 3) {
    return 'icono-volumeLow';
  }

  if (volume <= 2 / 3) {
    return 'icono-volumeMedium';
  }

  return 'icono-volumeHigh';
}

/* harmony default export */ var utils_getVolumeIconClassName = (getVolumeIconClassName);
// CONCATENATED MODULE: ./src/utils/getVolumeBarDirectionFromPosition.js
function getVolumeBarDirectionFromPosition(volumeBarPosition) {
  switch (volumeBarPosition) {
    case 'rightabove':
    case 'rightbelow':
      return 'right';

    case 'hiddenup':
    case 'upabove':
    default:
      return 'up';
  }
}

/* harmony default export */ var utils_getVolumeBarDirectionFromPosition = (getVolumeBarDirectionFromPosition);
// CONCATENATED MODULE: ./src/utils/reactStopPropagation.js
function stopPropagation(e) {
  e.stopPropagation();
  e.nativeEvent.stopImmediatePropagation();
}

/* harmony default export */ var reactStopPropagation = (stopPropagation);
// CONCATENATED MODULE: ./src/controls/VolumeControl.js









const VolumeControl_volumeControlStyle = {
  touchAction: 'none'
};

class VolumeControl_VolumeControl extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  static getDerivedStateFromProps(nextProps, prevState) {
    const hover = prevState.hover,
          volumeBarPosition = prevState.volumeBarPosition;

    if (volumeBarPosition && !hover && !nextProps.setVolumeInProgress) {
      return {
        volumeBarPosition: null
      };
    }

    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      // null | 'hiddenup' | 'hiddenright' | 'upabove' | 'rightabove' | 'rightbelow'
      volumeBarPosition: null
    };
    this.volumeControlRef = null;
    this.muteToggleRef = null;
    this.volumeBarContainerRef = null; // bind methods fired on React events

    this.setVolumeControlRef = this.setVolumeControlRef.bind(this);
    this.setMuteToggleRef = this.setMuteToggleRef.bind(this);
    this.setVolumeBarContainerRef = this.setVolumeBarContainerRef.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this); // bind listeners to add on mount and remove on unmount

    this.handleMuteToggleTouchStart = this.handleMuteToggleTouchStart.bind(this);
  }

  componentDidMount() {
    /* this should be a normal React listener but there seems to be a bug
     * in React preventing that from working as expected:
     * https://github.com/facebook/react/issues/9809
     */
    this.muteToggleRef.addEventListener('touchstart', this.handleMuteToggleTouchStart);
    /* since touchstart bubbling from inside this component is canceled
     * we need to manually trigger mouseleave for touch devices
     */

    document.addEventListener('touchstart', this.handleMouseLeave);
  }

  componentDidUpdate() {
    /* if we've applied a hidden class to our volume bar, it's because
     * we need to measure the element dimensions in order to figure out
     * where and in which direction to position it. if there isn't enough
     * vertical space above the control button, then we'll position the
     * bar hidden and left-to-right to measure it again on the next
     * componentDidUpdate. then if there's room we'll place it either
     * above or below (there's no good way to vertically position the
     * volume bar below the control button, so we skip that option).
     * granted - it's certainly not ideal to need to check dom dimensions
     * before placing an element, but a user could have applied unanticipated
     * styles we won't know about unless we check.
     */
    const volumeBarPosition = this.state.volumeBarPosition;

    if (volumeBarPosition === 'hiddenup' || volumeBarPosition === 'hiddenright') {
      const volumeControlRect = this.volumeControlRef.getBoundingClientRect();
      const top = volumeControlRect.top;
      const volumeBarContainerHeight = this.volumeBarContainerRef.offsetHeight;
      let newPosition;

      if (volumeBarPosition === 'hiddenup') {
        newPosition = volumeBarContainerHeight <= top ? 'upabove' : 'hiddenright';
      } else {
        if (volumeBarContainerHeight <= top) {
          newPosition = 'rightabove';
        } else {
          const viewportHeight = document.documentElement.clientHeight;
          const bottom = viewportHeight - volumeControlRect.bottom;
          newPosition = volumeBarContainerHeight <= bottom ? 'rightbelow' : null;
        }
      }

      this.setState({
        volumeBarPosition: newPosition
      });
    }
  }

  componentWillUnmount() {
    this.muteToggleRef.removeEventListener('touchstart', this.handleMuteToggleTouchStart);
    document.removeEventListener('touchstart', this.handleMouseLeave);
  }

  setVolumeControlRef(ref) {
    this.volumeControlRef = ref;
  }

  setMuteToggleRef(ref) {
    this.muteToggleRef = ref;
  }

  setVolumeBarContainerRef(ref) {
    this.volumeBarContainerRef = ref;
  }

  handleMouseEnter() {
    this.setState({
      hover: true,
      volumeBarPosition: this.state.volumeBarPosition || 'hiddenup'
    });
  }

  handleMouseLeave() {
    this.setState({
      hover: false,
      volumeBarPosition: this.props.setVolumeInProgress ? this.state.volumeBarPosition : null
    });
  }

  handleMuteToggleTouchStart(e) {
    if (!this.state.hover) {
      e.preventDefault();
      this.handleMouseEnter();
    }
  }

  renderHandle() {
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: classnames_default()('handle', {
        highlight: this.props.setVolumeInProgress
      })
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", null));
  }

  render() {
    const _props = this.props,
          volume = _props.volume,
          muted = _props.muted,
          setVolumeInProgress = _props.setVolumeInProgress,
          onSetVolume = _props.onSetVolume,
          onSetVolumeComplete = _props.onSetVolumeComplete,
          onToggleMuted = _props.onToggleMuted;
    const _state = this.state,
          hover = _state.hover,
          volumeBarPosition = _state.volumeBarPosition;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      ref: this.setVolumeControlRef,
      className: "rrap__volume_control",
      style: VolumeControl_volumeControlStyle,
      onMouseEnter: this.handleMouseEnter,
      onMouseLeave: this.handleMouseLeave,
      onTouchStart: reactStopPropagation
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("button", {
      ref: this.setMuteToggleRef,
      className: classnames_default()('button rrap__audio_button', {
        highlight: hover
      }),
      onClick: onToggleMuted
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: classnames_default()('foreground', utils_getVolumeIconClassName(volume, muted))
    })), volumeBarPosition && external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      ref: this.setVolumeBarContainerRef,
      className: classnames_default()('rrap__volume_control__volume_bar_container', volumeBarPosition)
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_ProgressBar, {
      className: classnames_default()('rrap__volume_control__volume_bar', volumeBarPosition),
      progressClassName: "volume",
      progress: muted ? 0 : volume,
      progressDirection: utils_getVolumeBarDirectionFromPosition(volumeBarPosition),
      handle: this.renderHandle(),
      adjusting: setVolumeInProgress,
      onAdjustProgress: onSetVolume,
      onAdjustComplete: onSetVolumeComplete
    })));
  }

}

VolumeControl_VolumeControl.propTypes = {
  volume: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  muted: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  setVolumeInProgress: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  onSetVolume: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired,
  onSetVolumeComplete: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired,
  onToggleMuted: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const VolumeControl_renderVolumeControl = factories_createControlRenderProp(VolumeControl_VolumeControl, ['volume', 'muted', 'setVolumeInProgress', 'onSetVolume', 'onSetVolumeComplete', 'onToggleMuted']);
react_lifecycles_compat_default()(VolumeControl_VolumeControl);
/* harmony default export */ var controls_VolumeControl = (VolumeControl_VolumeControl);
// EXTERNAL MODULE: ./node_modules/svg-react-loader/lib/loader.js?name=RepeatIcon!./node_modules/material-design-icons/av/svg/design/ic_repeat_48px.svg?
var ic_repeat_48px = __webpack_require__(8);
var ic_repeat_48px_default = /*#__PURE__*/__webpack_require__.n(ic_repeat_48px);

// EXTERNAL MODULE: ./node_modules/svg-react-loader/lib/loader.js?name=RepeatOneIcon!./node_modules/material-design-icons/av/svg/design/ic_repeat_one_48px.svg?
var ic_repeat_one_48px = __webpack_require__(7);
var ic_repeat_one_48px_default = /*#__PURE__*/__webpack_require__.n(ic_repeat_one_48px);

// CONCATENATED MODULE: ./src/controls/RepeatButton.js









function RepeatButton_getNextRepeatStrategy(repeatStrategy) {
  let nextIndex = repeatStrategyOptions.indexOf(repeatStrategy) + 1;

  if (nextIndex >= repeatStrategyOptions.length) {
    nextIndex = 0;
  }

  return repeatStrategyOptions[nextIndex];
}

class RepeatButton_RepeatButton extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  constructor(props) {
    super(props); // bind methods fired on React events

    this.handleNextRepeatStrategy = this.handleNextRepeatStrategy.bind(this);
  }

  handleNextRepeatStrategy() {
    this.props.onSetRepeatStrategy(RepeatButton_getNextRepeatStrategy(this.props.repeatStrategy));
  }

  render() {
    const repeatStrategy = this.props.repeatStrategy;
    const Icon = repeatStrategy === 'track' ? ic_repeat_one_48px_default.a : ic_repeat_48px_default.a;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("button", {
      className: classnames_default()('rrap__material_toggle rrap__audio_button rrap__repeat_btn', {
        on: repeatStrategy !== 'none'
      }),
      onClick: this.handleNextRepeatStrategy
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "inner foreground"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(Icon, {
      width: "100%",
      height: "100%"
    })));
  }

}

RepeatButton_RepeatButton.propTypes = {
  repeatStrategy: PlayerPropTypes_repeatStrategy.isRequired,
  onSetRepeatStrategy: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const RepeatButton_renderRepeatButton = factories_createControlRenderProp(RepeatButton_RepeatButton, ['repeatStrategy', 'onSetRepeatStrategy']);
/* harmony default export */ var controls_RepeatButton = (RepeatButton_RepeatButton);
// EXTERNAL MODULE: ./node_modules/svg-react-loader/lib/loader.js?name=ShuffleIcon!./node_modules/material-design-icons/av/svg/design/ic_shuffle_48px.svg?
var ic_shuffle_48px = __webpack_require__(6);
var ic_shuffle_48px_default = /*#__PURE__*/__webpack_require__.n(ic_shuffle_48px);

// CONCATENATED MODULE: ./src/controls/ShuffleButton.js






class ShuffleButton_ShuffleButton extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    const _props = this.props,
          shuffle = _props.shuffle,
          onToggleShuffle = _props.onToggleShuffle;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("button", {
      className: classnames_default()('rrap__material_toggle rrap__audio_button rrap__shuffle_btn', {
        on: shuffle
      }),
      onClick: onToggleShuffle
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "inner foreground"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(ic_shuffle_48px_default.a, {
      width: "100%",
      height: "100%"
    })));
  }

}

ShuffleButton_ShuffleButton.propTypes = {
  shuffle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  onToggleShuffle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const ShuffleButton_renderShuffleButton = factories_createControlRenderProp(ShuffleButton_ShuffleButton, ['shuffle', 'onToggleShuffle']);
/* harmony default export */ var controls_ShuffleButton = (ShuffleButton_ShuffleButton);
// EXTERNAL MODULE: external {"root":"ResizeObserver","commonjs":"resize-observer-polyfill","commonjs2":"resize-observer-polyfill","amd":"resize-observer-polyfill"}
var external_root_ResizeObserver_commonjs_resize_observer_polyfill_commonjs2_resize_observer_polyfill_amd_resize_observer_polyfill_ = __webpack_require__(5);
var external_root_ResizeObserver_commonjs_resize_observer_polyfill_commonjs2_resize_observer_polyfill_amd_resize_observer_polyfill_default = /*#__PURE__*/__webpack_require__.n(external_root_ResizeObserver_commonjs_resize_observer_polyfill_commonjs2_resize_observer_polyfill_amd_resize_observer_polyfill_);

// CONCATENATED MODULE: ./src/utils/getStatusBarSizeClassName.js
function getClassNameForRange(min, max) {
  return 'rrap__audio_info__' + min + (max ? '-' + max : '');
}

function getStatusBarSizeClassName(width) {
  if (width < 166) return null;
  if (width < 246) return getClassNameForRange(166, 245);
  if (width < 346) return getClassNameForRange(246, 345);
  if (width < 446) return getClassNameForRange(346, 445);
  if (width < 516) return getClassNameForRange(446, 515);
  return getClassNameForRange(516);
}

/* harmony default export */ var utils_getStatusBarSizeClassName = (getStatusBarSizeClassName);
// CONCATENATED MODULE: ./src/controls/common/AudioStatusBar.js






class AudioStatusBar_AudioStatusBar extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  constructor(props) {
    super(props);
    this.state = {
      elementSizeClassName: null
    };
    this.statusBarRef = null;
    this.statusBarResizeObserver = null; // bind methods fired on React events

    this.setStatusBarRef = this.setStatusBarRef.bind(this); // bind listeners to add on mount and remove on unmount

    this.handleResize = this.handleResize.bind(this);
  }

  componentDidMount() {
    this.statusBarResizeObserver = new external_root_ResizeObserver_commonjs_resize_observer_polyfill_commonjs2_resize_observer_polyfill_amd_resize_observer_polyfill_default.a(this.handleResize);
    this.statusBarResizeObserver.observe(this.statusBarRef);
  }

  componentWillUnmount() {
    this.statusBarResizeObserver.disconnect();
  }

  setStatusBarRef(ref) {
    this.statusBarRef = ref;
  }

  handleResize(entries) {
    for (const entry of entries) {
      if (entry.target === this.statusBarRef) {
        const width = entry.contentRect.width;
        this.setState({
          elementSizeClassName: utils_getStatusBarSizeClassName(width)
        });
        return;
      }
    }
  }

  render() {
    const _props = this.props,
          className = _props.className,
          style = _props.style,
          displayText = _props.displayText,
          displayTime = _props.displayTime;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      ref: this.setStatusBarRef,
      className: classnames_default()('rrap__audio_status_bar', className),
      style: style
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "rrap__audio_info_marquee"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: classnames_default()('rrap__audio_info', this.state.elementSizeClassName)
    }, displayText)), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "rrap__audio_time_progress"
    }, displayTime));
  }

}

AudioStatusBar_AudioStatusBar.propTypes = {
  className: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string,
  style: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object,
  displayText: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.any.isRequired,
  displayTime: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.string.isRequired
};
/* harmony default export */ var common_AudioStatusBar = (AudioStatusBar_AudioStatusBar);
// CONCATENATED MODULE: ./src/utils/convertToTime.js
/* converts given number of seconds to standard time display format
 * http://goo.gl/kEvnKn
 */
function convertToTime(number) {
  const mins = Math.floor(number / 60);
  const secs = (number % 60).toFixed();
  return `${mins < 10 ? '0' : ''}${mins}:${secs < 10 ? '0' : ''}${secs}`;
}

/* harmony default export */ var utils_convertToTime = (convertToTime);
// CONCATENATED MODULE: ./src/controls/AudioProgress.js









const AudioProgress_audioStatusBarStyle = {
  pointerEvents: 'none',
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0
};

class AudioProgress_AudioProgress extends external_root_React_commonjs_react_commonjs2_react_amd_react_["Component"] {
  constructor(props) {
    super(props); // bind methods fired on React events

    this.handleSeekPreview = this.handleSeekPreview.bind(this);
  }

  handleSeekPreview(progress) {
    this.props.onSeekPreview(progress * this.props.duration);
  }

  render() {
    const _props = this.props,
          playlist = _props.playlist,
          activeTrackIndex = _props.activeTrackIndex,
          currentTime = _props.currentTime,
          seekPreviewTime = _props.seekPreviewTime,
          seekInProgress = _props.seekInProgress,
          duration = _props.duration,
          onSeekComplete = _props.onSeekComplete;
    const time = seekInProgress ? seekPreviewTime : currentTime;
    const displayedProgress = duration ? time / duration : 0; // let timeView = (!isFinite(duration) || isNaN(duration)) ? `${convertToTime(time)}` : `${convertToTime(time)} / ${convertToTime(duration)}`;

    let timeView = `${utils_convertToTime(time)}`;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "rrap__audio_progress_container"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_ProgressBar, {
      className: "rrap__audio_progress_bar",
      progressClassName: "progress",
      progress: displayedProgress,
      progressDirection: "right",
      adjusting: seekInProgress,
      readonly: !utils_isPlaylistValid(playlist),
      onAdjustProgress: this.handleSeekPreview,
      onAdjustComplete: onSeekComplete
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_AudioStatusBar, {
      style: AudioProgress_audioStatusBarStyle,
      displayText: utils_getDisplayText(playlist[activeTrackIndex], false) || '',
      displayTime: timeView || ''
    }));
  }

}

AudioProgress_AudioProgress.propTypes = {
  playlist: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_track.isRequired).isRequired,
  activeTrackIndex: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  currentTime: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  seekPreviewTime: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  seekInProgress: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool.isRequired,
  duration: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  onSeekPreview: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired,
  onSeekComplete: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired
};
const AudioProgress_renderAudioProgress = factories_createControlRenderProp(AudioProgress_AudioProgress, ['playlist', 'activeTrackIndex', 'currentTime', 'seekPreviewTime', 'seekInProgress', 'duration', 'onSeekPreview', 'onSeekComplete']);
/* harmony default export */ var controls_AudioProgress = (AudioProgress_AudioProgress);
// CONCATENATED MODULE: ./src/controls/AudioProgressDisplay.js








const AudioProgressDisplay_audioStatusBarStyle = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0
};

class AudioProgressDisplay_AudioProgressDisplay extends external_root_React_commonjs_react_commonjs2_react_amd_react_["Component"] {
  render() {
    const _props = this.props,
          playlist = _props.playlist,
          activeTrackIndex = _props.activeTrackIndex,
          currentTime = _props.currentTime,
          duration = _props.duration;
    const progress = duration ? currentTime / duration : 0; // console.log(getDisplayText(playlist[activeTrackIndex]));
    // let timeView = (!isFinite(duration) || isNaN(duration)) ? `${convertToTime(currentTime)}` : `${convertToTime(currentTime)} / ${convertToTime(duration)}`;

    let timeView = `${utils_convertToTime(currentTime)}`;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "rrap__audio_progress_container"
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_ProgressBarDisplay, {
      className: "rrap__audio_progress_bar",
      progressClassName: "progress",
      progress: progress
    }), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(common_AudioStatusBar, {
      style: AudioProgressDisplay_audioStatusBarStyle,
      displayText: utils_getDisplayText(playlist[activeTrackIndex], false) || '',
      displayTime: timeView || ''
    }));
  }

}

AudioProgressDisplay_AudioProgressDisplay.propTypes = {
  playlist: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_track.isRequired).isRequired,
  activeTrackIndex: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  currentTime: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  duration: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired
};
const AudioProgressDisplay_renderAudioProgressDisplay = factories_createControlRenderProp(AudioProgressDisplay_AudioProgressDisplay, ['playlist', 'activeTrackIndex', 'currentTime', 'duration']);
/* harmony default export */ var controls_AudioProgressDisplay = (AudioProgressDisplay_AudioProgressDisplay);
// CONCATENATED MODULE: ./src/controls/Spacer.js



class Spacer_Spacer extends external_root_React_commonjs_react_commonjs2_react_amd_react_["PureComponent"] {
  render() {
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      className: "rrap__spacer"
    });
  }

}

Spacer_Spacer.propTypes = {
  style: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object
};
const Spacer_renderSpacer = () => external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(Spacer_Spacer, null);
/* harmony default export */ var controls_Spacer = (Spacer_Spacer);
// CONCATENATED MODULE: ./src/utils/getControlRenderProp.js










function getControlRenderProp_getControlRenderProp(control) {
  if (typeof control === 'function') {
    return control;
  }

  if (typeof control === 'string') {
    switch (control) {
      case 'playpause':
        return PlayPauseButton_renderPlayPauseButton;

      case 'backskip':
        return BackSkipButton_renderBackSkipButton;

      case 'forwardskip':
        return ForwardSkipButton_renderForwardSkipButton;

      case 'volume':
        return VolumeControl_renderVolumeControl;

      case 'repeat':
        return RepeatButton_renderRepeatButton;

      case 'shuffle':
        return ShuffleButton_renderShuffleButton;

      case 'progress':
        return AudioProgress_renderAudioProgress;

      case 'progressdisplay':
        return AudioProgressDisplay_renderAudioProgressDisplay;

      case 'spacer':
        return Spacer_renderSpacer;

      default:
        return null;
    }
  }

  return null;
}

/* harmony default export */ var utils_getControlRenderProp = (getControlRenderProp_getControlRenderProp);
// EXTERNAL MODULE: ./src/styles/index.scss
var styles = __webpack_require__(12);

// CONCATENATED MODULE: ./src/AudioPlayer.js
function AudioPlayer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { AudioPlayer_defineProperty(target, key, source[key]); }); } return target; }

function AudioPlayer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






















let AudioPlayer_nextControlKey = 0;

function AudioPlayer_getNextControlKey() {
  return (AudioPlayer_nextControlKey++).toString();
}

function AudioPlayer_playErrorHandler(err) {
  logError(err);

  if (err.name === 'NotAllowedError') {
    const warningMessage = 'Audio playback failed at ' + new Date().toLocaleTimeString() + '! (Perhaps autoplay is disabled in this browser.)';
    logWarning(warningMessage);
  }
} // Existing Media Session API implementations have default handlers
// for play/pause, and may yield unexpected behavior if custom
// play/pause handlers are defined - so let's leave them be.


const AudioPlayer_supportableMediaSessionActions = ['previoustrack', 'nexttrack', 'seekbackward', 'seekforward'];
const AudioPlayer_defaultState = {
  // indicates whether audio player should be paused
  paused: true,
  // elapsed time for active track, in seconds
  currentTime: 0,
  // The most recent targeted time, in seconds, for seek preview
  seekPreviewTime: 0,

  /* true if the user is currently dragging the mouse
   * to seek a new track position
   */
  seekInProgress: false,

  /* true if audio was playing when seek previewing began,
   * it was paused, and it should be resumed on seek
   * complete
   */
  awaitingResumeOnSeekComplete: false,
  // the duration in seconds of the loaded track
  duration: 0,

  /* the TimeRanges object representing the buffered sections of the
   * loaded track
   */
  buffered: null,

  /* the TimeRanges object representing the played sections of the
   * loaded track
   */
  played: null,
  // true if the audio is currently stalled pending data buffering
  stalled: false,
  // true if the active track should play on the next componentDidUpdate
  awaitingPlay: false
}; // assumes playlist is valid

function AudioPlayer_getGoToTrackState(prevState, index, shouldPlay = true) {
  const isNewTrack = prevState.activeTrackIndex !== index;
  return {
    activeTrackIndex: index,
    trackLoading: isNewTrack,
    currentTime: 0,
    loop: isNewTrack ? false : prevState.loop,
    awaitingPlay: Boolean(shouldPlay),
    paused: !shouldPlay
  };
}

class AudioPlayer_AudioPlayer extends external_root_React_commonjs_react_commonjs2_react_amd_react_["Component"] {
  constructor(props) {
    super(props);
    this.state = AudioPlayer_objectSpread({}, AudioPlayer_defaultState, {
      // index matching requested track (whether track has loaded or not)
      activeTrackIndex: utils_convertToNumberWithinIntervalBounds(props.startingTrackIndex, 0),
      // whether we're waiting on loading metadata for the active track
      trackLoading: utils_isPlaylistValid(props.playlist),
      // the current timestamp on the active track in seconds
      currentTime: utils_convertToNumberWithinIntervalBounds(props.startingTime, 0),
      // the latest volume of the audio, between 0 and 1.
      volume: utils_convertToNumberWithinIntervalBounds(props.defaultVolume, 0, 1),
      // true if the audio has been muted
      muted: props.defaultMuted,
      // whether to loop the active track
      loop: props.defaultRepeatStrategy === 'track',
      // true if playlist should continue at start after completion
      cycle: props.defaultRepeatStrategy === 'playlist',
      // whether to randomly pick next track from playlist after one finishes
      shuffle: props.defaultShuffle,
      // Rate at which audio should be played. 1.0 is normal speed.
      playbackRate: props.defaultPlaybackRate,
      // true if user is currently dragging mouse to change the volume
      setVolumeInProgress: false,
      // playlist prop copied to state (for getDerivedStateFromProps)
      __playlist__: props.playlist
    }); // volume at last time we were unmuted and not actively setting volume

    this.lastStableVolume = this.state.volume; // cache of keys to use in controls render
    // (to maintain state in case order changes)

    this.controlKeys = new Map(); // used to keep track of play history when we are shuffling

    this.shuffler = new utils_ShuffleManager(utils_getSourceList(props.playlist), {
      allowBackShuffle: props.allowBackShuffle
    }); // html audio element used for playback

    this.audio = null; // bind methods fired on React events

    this.setAudioElementRef = this.setAudioElementRef.bind(this);
    this.togglePause = this.togglePause.bind(this);
    this.selectTrackIndex = this.selectTrackIndex.bind(this);
    this.forwardSkip = this.forwardSkip.bind(this);
    this.backSkip = this.backSkip.bind(this);
    this.seekPreview = this.seekPreview.bind(this);
    this.seekComplete = this.seekComplete.bind(this);
    this.setVolume = this.setVolume.bind(this);
    this.setVolumeComplete = this.setVolumeComplete.bind(this);
    this.toggleMuted = this.toggleMuted.bind(this);
    this.toggleShuffle = this.toggleShuffle.bind(this);
    this.setRepeatStrategy = this.setRepeatStrategy.bind(this);
    this.setPlaybackRate = this.setPlaybackRate.bind(this); // bind audio event listeners to add on mount and remove on unmount

    this.handleAudioPlay = this.handleAudioPlay.bind(this);
    this.handleAudioPause = this.handleAudioPause.bind(this);
    this.handleAudioSrcrequest = this.handleAudioSrcrequest.bind(this);
    this.handleAudioEnded = this.handleAudioEnded.bind(this);
    this.handleAudioStalled = this.handleAudioStalled.bind(this);
    this.handleAudioCanplaythrough = this.handleAudioCanplaythrough.bind(this);
    this.handleAudioTimeupdate = this.handleAudioTimeupdate.bind(this);
    this.handleAudioLoadedmetadata = this.handleAudioLoadedmetadata.bind(this);
    this.handleAudioVolumechange = this.handleAudioVolumechange.bind(this);
    this.handleAudioDurationchange = this.handleAudioDurationchange.bind(this);
    this.handleAudioProgress = this.handleAudioProgress.bind(this);
    this.handleAudioLoopchange = this.handleAudioLoopchange.bind(this);
    this.handleAudioRatechange = this.handleAudioRatechange.bind(this);
  }

  componentDidMount() {
    const audio = this.audio = factories_createCustomAudioElement(this.audio); // initialize audio properties

    audio.currentTime = this.state.currentTime;
    audio.volume = this.state.volume;
    audio.muted = this.state.muted;
    audio.defaultPlaybackRate = this.props.defaultPlaybackRate;
    audio.playbackRate = this.state.playbackRate; // add event listeners on the audio element

    audio.addEventListener('play', this.handleAudioPlay);
    audio.addEventListener('pause', this.handleAudioPause);
    audio.addEventListener('srcrequest', this.handleAudioSrcrequest);
    audio.addEventListener('ended', this.handleAudioEnded);
    audio.addEventListener('stalled', this.handleAudioStalled);
    audio.addEventListener('canplaythrough', this.handleAudioCanplaythrough);
    audio.addEventListener('timeupdate', this.handleAudioTimeupdate);
    audio.addEventListener('loadedmetadata', this.handleAudioLoadedmetadata);
    audio.addEventListener('volumechange', this.handleAudioVolumechange);
    audio.addEventListener('durationchange', this.handleAudioDurationchange);
    audio.addEventListener('progress', this.handleAudioProgress);
    audio.addEventListener('loopchange', this.handleAudioLoopchange);
    audio.addEventListener('ratechange', this.handleAudioRatechange);
    this.addMediaEventListeners(this.props.onMediaEvent);

    if (utils_isPlaylistValid(this.props.playlist) && this.props.autoplay) {
      clearTimeout(this.delayTimeout);
      this.delayTimeout = setTimeout(() => {
        this.togglePause(false);
      }, this.props.autoplayDelayInSeconds * 1000);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const newPlaylist = nextProps.playlist;
    const baseNewState = {
      __playlist__: newPlaylist
    }; // check if the new playlist is invalid

    if (!utils_isPlaylistValid(newPlaylist)) {
      return AudioPlayer_objectSpread({}, AudioPlayer_defaultState, baseNewState, {
        activeTrackIndex: 0,
        trackLoading: false
      });
    } // check if the activeTrackIndex doesn't need to be updated


    const prevSources = utils_getTrackSources(prevState.__playlist__, prevState.activeTrackIndex); // the sources if we stay on the same track index

    const currentSources = utils_getTrackSources(newPlaylist, prevState.activeTrackIndex); // non-comprehensive but probably accurate check

    if (prevSources[0].src === currentSources[0].src) {
      // our active track index already matches
      return baseNewState;
    }
    /* if the track we're already playing is in the new playlist, update the
     * activeTrackIndex.
     */


    const newTrackIndex = utils_findTrackIndexByUrl(newPlaylist, prevSources[0].src);

    if (newTrackIndex !== -1) {
      return AudioPlayer_objectSpread({}, baseNewState, {
        activeTrackIndex: newTrackIndex
      });
    } // if not, then load the first track in the new playlist, and pause.


    return AudioPlayer_objectSpread({}, baseNewState, AudioPlayer_getGoToTrackState(prevState, 0, false));
  }

  componentDidUpdate(prevProps, prevState) {
    this.audio.defaultPlaybackRate = this.props.defaultPlaybackRate; // Update media event listeners that may have changed

    this.removeMediaEventListeners(prevProps.onMediaEvent);
    this.addMediaEventListeners(this.props.onMediaEvent);
    this.shuffler.setList(utils_getSourceList(this.props.playlist));
    this.shuffler.setOptions({
      allowBackShuffle: this.props.allowBackShuffle
    });
    const prevSources = utils_getTrackSources(prevProps.playlist, prevState.activeTrackIndex);
    const newSources = utils_getTrackSources(this.props.playlist, this.state.activeTrackIndex);

    if (prevSources[0].src !== newSources[0].src) {
      // cancel playback and re-scan current sources
      this.audio.load();

      if (!this.state.shuffle) {
        // after toggling off shuffle, we defer clearing the shuffle
        // history until we actually change tracks - if the user quickly
        // toggles  shuffle off then back on again, we don't want to have
        // lost our history.
        this.shuffler.clear();
      }
    }

    if (prevProps !== this.props && !this.audio.paused) {
      // update running media session based on new props
      this.stealMediaSession();
    }

    if (this.state.activeTrackIndex !== prevState.activeTrackIndex && typeof this.props.onActiveTrackUpdate === 'function') {
      this.props.onActiveTrackUpdate(this.state.activeTrackIndex);
    }

    if (this.state.awaitingPlay) {
      this.setState({
        awaitingPlay: false
      });
      this.togglePause(false);
    }
  }

  componentWillUnmount() {
    const audio = this.audio; // remove event listeners on the audio element

    audio.removeEventListener('play', this.handleAudioPlay);
    audio.removeEventListener('pause', this.handleAudioPause);
    audio.removeEventListener('srcrequest', this.handleAudioSrcrequest);
    audio.removeEventListener('ended', this.handleAudioEnded);
    audio.removeEventListener('stalled', this.handleAudioStalled);
    audio.removeEventListener('canplaythrough', this.handleAudioCanplaythrough);
    audio.removeEventListener('timeupdate', this.handleAudioTimeupdate);
    audio.removeEventListener('loadedmetadata', this.handleAudioLoadedmetadata);
    audio.removeEventListener('volumechange', this.handleAudioVolumechange);
    audio.removeEventListener('durationchange', this.handleAudioDurationchange);
    audio.removeEventListener('progress', this.handleAudioProgress);
    audio.removeEventListener('loopchange', this.handleAudioLoopchange);
    audio.removeEventListener('ratechange', this.handleAudioRatechange);
    removeMediaEventListeners(this.props.onMediaEvent);
    clearTimeout(this.gapLengthTimeout);
    clearTimeout(this.delayTimeout); // pause the audio element before we unmount

    audio.pause();
  }

  setAudioElementRef(ref) {
    this.audio = ref;

    if (typeof this.props.audioElementRef === 'function') {
      this.props.audioElementRef(ref);
    }
  }

  addMediaEventListeners(mediaEvents) {
    if (!mediaEvents) {
      return;
    }

    Object.keys(mediaEvents).forEach(type => {
      if (typeof mediaEvents[type] !== 'function') {
        return;
      }

      this.audio.addEventListener(type, mediaEvents[type]);
    });
  }

  removeMediaEventListeners(mediaEvents) {
    if (!mediaEvents) {
      return;
    }

    Object.keys(mediaEvents).forEach(type => {
      if (typeof mediaEvents[type] !== 'function') {
        return;
      }

      this.audio.removeEventListener(type, mediaEvents[type]);
    });
  }

  stealMediaSession() {
    if (!(window.MediaSession && navigator.mediaSession instanceof MediaSession)) {
      return;
    }

    navigator.mediaSession.metadata = new MediaMetadata(this.props.playlist[this.state.activeTrackIndex]);
    AudioPlayer_supportableMediaSessionActions.map(action => {
      if (this.props.supportedMediaSessionActions.indexOf(action) === -1) {
        return null;
      }

      const seekLength = this.props.mediaSessionSeekLengthInSeconds;

      switch (action) {
        case 'play':
          return this.togglePause.bind(this, false);

        case 'pause':
          return this.togglePause.bind(this, true);

        case 'previoustrack':
          return this.backSkip;

        case 'nexttrack':
          return this.forwardSkip;

        case 'seekbackward':
          return () => this.audio.currentTime -= seekLength;

        case 'seekforward':
          return () => this.audio.currentTime += seekLength;

        default:
          return undefined;
      }
    }).forEach((handler, i) => {
      navigator.mediaSession.setActionHandler(AudioPlayer_supportableMediaSessionActions[i], handler);
    });
  }

  handleAudioPlay() {
    this.setState(state => state.paused === false ? null : {
      paused: false
    });
    this.stealMediaSession();
  }

  handleAudioPause() {
    this.setState(state => state.paused === true ? null : {
      paused: true
    });
  }

  handleAudioSrcrequest(e) {
    const playlist = this.props.playlist;
    const sources = utils_getTrackSources(playlist, this.state.activeTrackIndex);

    if (array_find_index_default()(sources, s => s.src === e.srcRequested) !== -1) {
      // we're good! nothing to update.
      return;
    } // looks like 'src' was set from outside our component.
    // let's see if we can use it.


    const newTrackIndex = utils_findTrackIndexByUrl(playlist, e.srcRequested);

    if (newTrackIndex === -1) {
      logError(`Source '${newSrc}' does not exist in the loaded playlist. ` + `Make sure you've updated the 'playlist' prop to AudioPlayer ` + `before you select this track!`);
      return;
    }

    this.selectTrackIndex(newTrackIndex);
  }

  handleAudioEnded() {
    clearTimeout(this.gapLengthTimeout);
    const _props = this.props,
          playlist = _props.playlist,
          loadFirstTrackOnPlaylistComplete = _props.loadFirstTrackOnPlaylistComplete;

    if (!utils_isPlaylistValid(playlist)) {
      return;
    }

    const _state = this.state,
          cycle = _state.cycle,
          activeTrackIndex = _state.activeTrackIndex;

    if (!cycle && activeTrackIndex + 1 >= playlist.length) {
      if (loadFirstTrackOnPlaylistComplete) {
        this.goToTrack(0, false);
      }

      return;
    }

    this.gapLengthTimeout = setTimeout(this.forwardSkip, this.props.gapLengthInSeconds * 1000);
  }

  handleAudioStalled() {
    this.setState(state => state.stalled === true ? null : {
      stalled: true
    });
  }

  handleAudioCanplaythrough() {
    this.setState(state => state.stalled === false ? null : {
      stalled: false
    });
  }

  handleAudioTimeupdate() {
    const _audio = this.audio,
          currentTime = _audio.currentTime,
          played = _audio.played;
    this.setState({
      currentTime,
      played
    });
  }

  handleAudioLoadedmetadata() {
    this.setState(state => state.trackLoading === false ? null : {
      trackLoading: false
    });
  }

  handleAudioVolumechange() {
    const _audio2 = this.audio,
          volume = _audio2.volume,
          muted = _audio2.muted;
    this.setState({
      volume,
      muted
    });
  }

  handleAudioDurationchange() {
    const duration = this.audio.duration;
    this.setState({
      duration
    });
  }

  handleAudioProgress() {
    const buffered = this.audio.buffered;
    this.setState({
      buffered
    });
  }

  handleAudioLoopchange() {
    const loop = this.audio.loop;
    this.setState(state => state.loop === loop ? null : {
      loop
    });
  }

  handleAudioRatechange() {
    const playbackRate = this.audio.playbackRate;
    this.setState(state => state.playbackRate === playbackRate ? null : {
      playbackRate
    });
  }

  togglePause(value) {
    const pause = typeof value === 'boolean' ? value : !this.state.paused;

    if (pause) {
      this.audio.pause();
      return;
    }

    if (!this.audio.currentSrc) {
      return;
    }

    try {
      const playPromise = this.audio.play();

      if (playPromise && typeof playPromise.catch === 'function') {
        playPromise.catch(err => {
          // AbortError is pretty much always called because we're skipping
          // tracks quickly or hitting pause before a track has a chance to
          // play. It's pretty safe to just ignore these error messages.
          if (err.name !== 'AbortError') {
            return Promise.reject(err);
          }
        }).catch(AudioPlayer_playErrorHandler);
      }
    } catch (err) {
      AudioPlayer_playErrorHandler(err);
    }
  } // assumes playlist is valid - don't call without checking


  goToTrack(index, shouldPlay = true) {
    this.setState(state => AudioPlayer_getGoToTrackState(state, index, shouldPlay));
  }

  selectTrackIndex(index) {
    const playlist = this.props.playlist;

    if (!utils_isPlaylistValid(playlist)) {
      return;
    }

    if (index < 0 || index > playlist.length) {
      logWarning(`Playlist index ${index} is out of bounds!`);
      return;
    }

    if (this.state.shuffle) {
      this.shuffler.pickNextItem(index, this.state.activeTrackIndex);
    }

    this.goToTrack(index);
  }

  backSkip() {
    const _props2 = this.props,
          playlist = _props2.playlist,
          stayOnBackSkipThreshold = _props2.stayOnBackSkipThreshold;
    const audio = this.audio;
    const _state2 = this.state,
          cycle = _state2.cycle,
          activeTrackIndex = _state2.activeTrackIndex,
          shuffle = _state2.shuffle;

    if (!utils_isPlaylistValid(playlist) || audio.currentTime >= stayOnBackSkipThreshold || !cycle && activeTrackIndex < 1) {
      audio.currentTime = 0;
      return;
    }

    let index;

    if (shuffle) {
      const previousItem = this.shuffler.findPreviousItem(activeTrackIndex);

      if (previousItem === undefined) {
        // if we aren't allowing backShuffle then we'll hit a stopping point.
        audio.currentTime = 0;
        return;
      }

      index = utils_findTrackIndexByUrl(playlist, previousItem);
    } else {
      index = activeTrackIndex - 1;

      if (index < 0) {
        index = playlist.length - 1;
      }
    }

    this.goToTrack(index);
  }

  forwardSkip() {
    const playlist = this.props.playlist;
    const _state3 = this.state,
          cycle = _state3.cycle,
          activeTrackIndex = _state3.activeTrackIndex,
          shuffle = _state3.shuffle;

    if (!utils_isPlaylistValid(playlist) || !cycle && activeTrackIndex + 1 >= playlist.length) {
      return;
    }

    let index;

    if (shuffle) {
      index = utils_findTrackIndexByUrl(playlist, this.shuffler.findNextItem(activeTrackIndex));
    } else {
      index = activeTrackIndex + 1;

      if (index >= playlist.length) {
        index = 0;
      }
    }

    this.goToTrack(index);
  }

  seekPreview(targetTime) {
    if (!utils_isPlaylistValid(this.props.playlist)) {
      return;
    }

    const _state4 = this.state,
          paused = _state4.paused,
          awaitingResumeOnSeekComplete = _state4.awaitingResumeOnSeekComplete;

    if (!paused && this.props.pauseOnSeekPreview && !awaitingResumeOnSeekComplete) {
      this.setState({
        awaitingResumeOnSeekComplete: true
      });
      this.togglePause(true);
    }

    this.setState({
      seekPreviewTime: targetTime,
      seekInProgress: true
    });
  }

  seekComplete() {
    const _state5 = this.state,
          seekPreviewTime = _state5.seekPreviewTime,
          awaitingResumeOnSeekComplete = _state5.awaitingResumeOnSeekComplete;
    this.setState({
      seekInProgress: false
    });

    if (isNaN(seekPreviewTime)) {
      return;
    }

    this.setState({
      /* we'll update currentTime on the audio listener hook anyway,
       * but the optimistic update helps avoid a visual glitch in
       * the progress bar, if seekInProgress changes before currentTime.
       */
      currentTime: seekPreviewTime
    });
    const audio = this.audio;
    audio.currentTime = seekPreviewTime;

    if (awaitingResumeOnSeekComplete) {
      this.togglePause(false);
      this.setState({
        awaitingResumeOnSeekComplete: false
      });
    }
  }

  setVolume(volume) {
    if (!this.state.setVolumeInProgress) {
      this.setState({
        setVolumeInProgress: true
      });
    }

    const volumeInBounds = utils_convertToNumberWithinIntervalBounds(volume, 0, 1);
    this.audio.muted = volumeInBounds === 0 ? true : false;
    this.audio.volume = volumeInBounds;
  }

  setVolumeComplete() {
    this.setState({
      setVolumeInProgress: false
    });

    if (!this.audio.muted) {
      this.lastStableVolume = this.audio.volume;
    }
  }

  toggleMuted(value) {
    const muted = typeof value === 'boolean' ? value : !this.state.muted;
    this.audio.muted = muted;

    if (!muted) {
      this.audio.volume = this.lastStableVolume;
    }
  }

  toggleShuffle(value) {
    const shuffle = typeof value === 'boolean' ? value : !this.state.shuffle;
    this.setState({
      shuffle
    });

    if (typeof this.props.onShuffleUpdate === 'function') {
      this.props.onShuffleUpdate(shuffle);
    }
  }

  setRepeatStrategy(repeatStrategy) {
    if (repeatStrategyOptions.indexOf(repeatStrategy) === -1) {
      logWarning('repeatStrategy "' + repeatStrategy + '" is not one of: ' + repeatStrategyOptions.split(', ') + '.');
      return;
    }

    const prevRepeatStrategy = utils_getRepeatStrategy(this.state.loop, this.state.cycle);
    this.setState(() => {
      switch (repeatStrategy) {
        case 'track':
          return {
            loop: true
          };

        case 'playlist':
          return {
            loop: false,
            cycle: true
          };

        case 'none':
          return {
            loop: false,
            cycle: false
          };

        default:
          return null;
      }
    });

    if (typeof this.props.onRepeatStrategyUpdate === 'function' && prevRepeatStrategy !== repeatStrategy) {
      this.props.onRepeatStrategyUpdate(repeatStrategy);
    }
  }

  setPlaybackRate(rate) {
    this.audio.playbackRate = rate;
  }

  getControlProps() {
    const props = this.props,
          state = this.state;
    return {
      playlist: props.playlist,
      activeTrackIndex: state.activeTrackIndex,
      trackLoading: state.trackLoading,
      paused: state.paused,
      currentTime: state.currentTime,
      seekPreviewTime: state.seekPreviewTime,
      seekInProgress: state.seekInProgress,
      awaitingResumeOnSeekComplete: state.awaitingResumeOnSeekComplete,
      duration: state.duration,
      buffered: state.buffered,
      played: state.played,
      volume: state.volume,
      muted: state.muted,
      shuffle: state.shuffle,
      stalled: state.stalled,
      playbackRate: state.playbackRate,
      setVolumeInProgress: state.setVolumeInProgress,
      repeatStrategy: utils_getRepeatStrategy(state.loop, state.cycle),
      onTogglePause: this.togglePause,
      onSelectTrackIndex: this.selectTrackIndex,
      onBackSkip: this.backSkip,
      onForwardSkip: this.forwardSkip,
      onSeekPreview: this.seekPreview,
      onSeekComplete: this.seekComplete,
      onSetVolume: this.setVolume,
      onSetVolumeComplete: this.setVolumeComplete,
      onToggleMuted: this.toggleMuted,
      onToggleShuffle: this.toggleShuffle,
      onSetRepeatStrategy: this.setRepeatStrategy,
      onSetPlaybackRate: this.setPlaybackRate
    };
  }

  getKeyedChildren(elements) {
    // counts of rendered elements by type
    const elementsRendered = new Map();
    return elements.map(element => {
      // support React | Preact | Inferno
      const type = element.type || element.nodeName || element.tag || ''; // index within list of keys by type

      const keyIndex = elementsRendered.get(type) || 0;
      elementsRendered.set(type, keyIndex + 1);
      const keysForType = this.controlKeys.get(type) || [];
      let key;

      if (keysForType[keyIndex]) {
        key = keysForType[keyIndex];
      } else {
        key = AudioPlayer_getNextControlKey();
        this.controlKeys.set(type, keysForType.concat(key));
      }

      return element && external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.cloneElement(element, {
        key
      });
    });
  }

  render() {
    const hasChildren = Boolean(external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.Children.count(this.props.children));
    const sources = utils_getTrackSources(this.props.playlist, this.state.activeTrackIndex);
    const ControlWrapper = this.props.controlWrapper;
    return external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("div", {
      style: this.props.style
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("audio", {
      ref: this.setAudioElementRef,
      crossOrigin: this.props.crossOrigin,
      preload: "metadata",
      loop: this.state.loop
    }, sources.map(source => external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement("source", {
      key: source.src,
      src: source.src,
      type: source.type
    }))), external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(src_PlayerContext.Provider, {
      value: this.getControlProps()
    }, hasChildren && this.props.children, !hasChildren && external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(ControlWrapper, {
      title: this.props.getDisplayText(this.props.playlist[this.state.activeTrackIndex]) || ''
    }, external_root_React_commonjs_react_commonjs2_react_amd_react_default.a.createElement(src_PlayerContext.Consumer, null, controlProps => this.getKeyedChildren(this.props.controls.map((control, index) => {
      const renderControl = utils_getControlRenderProp(control);
      return renderControl && renderControl(controlProps);
    }))))));
  }

}

AudioPlayer_AudioPlayer.propTypes = {
  playlist: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_track.isRequired).isRequired,
  controls: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_control.isRequired).isRequired,
  controlWrapper: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired,
  autoplay: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  autoplayDelayInSeconds: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  gapLengthInSeconds: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  crossOrigin: PlayerPropTypes_crossOriginAttribute,
  defaultVolume: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  defaultMuted: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  defaultRepeatStrategy: PlayerPropTypes_repeatStrategy.isRequired,
  defaultShuffle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  defaultPlaybackRate: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  startingTime: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  startingTrackIndex: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  loadFirstTrackOnPlaylistComplete: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  pauseOnSeekPreview: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  allowBackShuffle: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.bool,
  stayOnBackSkipThreshold: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  supportedMediaSessionActions: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.arrayOf(PlayerPropTypes_mediaSessionAction.isRequired).isRequired,
  mediaSessionSeekLengthInSeconds: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.number.isRequired,
  getDisplayText: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired,
  style: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.object,
  onActiveTrackUpdate: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func,
  onRepeatStrategyUpdate: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func,
  onShuffleUpdate: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func,
  onMediaEvent: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.objectOf(external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func.isRequired),
  audioElementRef: external_root_PropTypes_commonjs_prop_types_commonjs2_prop_types_amd_prop_types_default.a.func
};
AudioPlayer_AudioPlayer.defaultProps = {
  controls: ['spacer', 'backskip', 'playpause', 'forwardskip', 'spacer', 'progress'],
  controlWrapper: controls_AudioControlBar,
  autoplay: false,
  autoplayDelayInSeconds: 0,
  gapLengthInSeconds: 0,
  defaultVolume: 1,
  defaultMuted: false,
  defaultRepeatStrategy: 'playlist',
  defaultShuffle: false,
  defaultPlaybackRate: 1,
  startingTime: 0,
  startingTrackIndex: 0,
  loadFirstTrackOnPlaylistComplete: true,
  pauseOnSeekPreview: false,
  maintainPlaybackRate: false,
  allowBackShuffle: false,
  stayOnBackSkipThreshold: 5,
  supportedMediaSessionActions: ['play', 'pause', 'previoustrack', 'nexttrack'],
  mediaSessionSeekLengthInSeconds: 10,
  getDisplayText: utils_getDisplayText
};
react_lifecycles_compat_default()(AudioPlayer_AudioPlayer);
/* harmony default export */ var src_AudioPlayer = (AudioPlayer_AudioPlayer);
// CONCATENATED MODULE: ./src/index.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerContextProvider", function() { return src_PlayerContextProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerContextConsumer", function() { return src_PlayerContextConsumer; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "AudioPlayer", function() { return src_AudioPlayer; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderPlayPauseButton", function() { return PlayPauseButton_renderPlayPauseButton; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderBackSkipButton", function() { return BackSkipButton_renderBackSkipButton; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderForwardSkipButton", function() { return ForwardSkipButton_renderForwardSkipButton; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderVolumeControl", function() { return VolumeControl_renderVolumeControl; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderRepeatButton", function() { return RepeatButton_renderRepeatButton; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderShuffleButton", function() { return ShuffleButton_renderShuffleButton; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "renderAudioProgress", function() { return AudioProgress_renderAudioProgress; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "ProgressBar", function() { return common_ProgressBar; });
/* concated harmony reexport */__webpack_require__.d(__webpack_exports__, "ProgressBarDisplay", function() { return common_ProgressBarDisplay; });











/* harmony default export */ var src = __webpack_exports__["default"] = (src_AudioPlayer);

const src_PlayerContextProvider = src_AudioPlayer;
const src_PlayerContextConsumer = src_PlayerContext.Consumer;








 // for browser script tag (global var) usage

src_AudioPlayer.PlayerContextProvider = src_AudioPlayer;
src_AudioPlayer.PlayerContextConsumer = src_PlayerContext.Consumer;
src_AudioPlayer.renderPlayPauseButton = PlayPauseButton_renderPlayPauseButton;
src_AudioPlayer.renderBackSkipButton = BackSkipButton_renderBackSkipButton;
src_AudioPlayer.renderForwardSkipButton = ForwardSkipButton_renderForwardSkipButton;
src_AudioPlayer.renderVolumeControl = VolumeControl_renderVolumeControl;
src_AudioPlayer.renderRepeatButton = RepeatButton_renderRepeatButton;
src_AudioPlayer.renderShuffleButton = ShuffleButton_renderShuffleButton;
src_AudioPlayer.renderAudioProgress = AudioProgress_renderAudioProgress;
src_AudioPlayer.ProgressBar = common_ProgressBar;
src_AudioPlayer.ProgressBarDisplay = common_ProgressBarDisplay;

/***/ }),
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

if (true) {
  var React = __webpack_require__(1);
}

var Fragment = React.Fragment || function (_React$Component) {
  _inherits(_Fragment, _React$Component);

  function _Fragment(props) {
    _classCallCheck(this, _Fragment);

    var _this = _possibleConstructorReturn(this, (_Fragment.__proto__ || Object.getPrototypeOf(_Fragment)).call(this, props));

    _this.refFn = _this.refFn.bind(_this);
    _this.orphans = [];
    return _this;
  }

  _createClass(_Fragment, [{
    key: 'refFn',
    value: function refFn(div) {
      this.div = div;
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.unwrapChildren();
      this.div.style.display = 'none';
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps() {
      this.rewrapChildren();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.unwrapChildren();
    }
  }, {
    key: 'unwrapChildren',
    value: function unwrapChildren() {
      var _this2 = this;

      // defer first to style calculation to ensure CSS transitions can happen:
      // https://stackoverflow.com/a/24195559/4956731
      setTimeout(function () {
        // plain js rocks!
        // https://plainjs.com/javascript/manipulation/unwrap-a-dom-element-35/
        if (!_this2.div.parentNode) {
          return;
        }
        _this2.orphans = [];
        while (_this2.div.firstChild) {
          _this2.orphans.push(_this2.div.firstChild);
          _this2.div.parentNode.insertBefore(_this2.div.firstChild, _this2.div);
        }
      });
    }
  }, {
    key: 'rewrapChildren',
    value: function rewrapChildren() {
      if (!this.div.parentNode) {
        return;
      }
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.orphans[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var orphan = _step.value;

          this.div.appendChild(orphan);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return React.createElement('div', { ref: this.refFn }, this.props.children);
    }
  }]);

  return _Fragment;
}(React.Component);

if (typeof module !== 'undefined' && module) {
  module.exports = Fragment;
}

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(13)(module)))

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 */

function makeEmptyFunction(arg) {
  return function () {
    return arg;
  };
}

/**
 * This function accepts and discards inputs; it has no side effects. This is
 * primarily useful idiomatically for overridable function endpoints which
 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
 */
var emptyFunction = function emptyFunction() {};

emptyFunction.thatReturns = makeEmptyFunction;
emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
emptyFunction.thatReturnsNull = makeEmptyFunction(null);
emptyFunction.thatReturnsThis = function () {
  return this;
};
emptyFunction.thatReturnsArgument = function (arg) {
  return arg;
};

module.exports = emptyFunction;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var emptyFunction = __webpack_require__(15);

/**
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var warning = emptyFunction;

if (false) { var printWarning; }

module.exports = warning;

/***/ }),
/* 17 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1, eval)("this");
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {// @flow


var key = '__global_unique_id__';

module.exports = function() {
  return global[key] = (global[key] || 0) + 1;
};

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(17)))

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _gud = __webpack_require__(18);

var _gud2 = _interopRequireDefault(_gud);

var _warning = __webpack_require__(16);

var _warning2 = _interopRequireDefault(_warning);

var _reactDotFragment = __webpack_require__(14);

var _reactDotFragment2 = _interopRequireDefault(_reactDotFragment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MAX_SIGNED_31_BIT_INT = 1073741823;

// Inlined Object.is polyfill.
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
function objectIs(x, y) {
  if (x === y) {
    return x !== 0 || 1 / x === 1 / y;
  } else {
    return x !== x && y !== y;
  }
}

function createEventEmitter(value) {
  var handlers = [];
  return {
    on: function on(handler) {
      handlers.push(handler);
    },
    off: function off(handler) {
      handlers = handlers.filter(function (h) {
        return h !== handler;
      });
    },
    get: function get() {
      return value;
    },
    set: function set(newValue, changedBits) {
      value = newValue;
      handlers.forEach(function (handler) {
        return handler(value, changedBits);
      });
    }
  };
}

function onlyChild(children) {
  return Array.isArray(children) ? children[0] : children;
}

function createReactContext(defaultValue, calculateChangedBits) {
  var _Provider$childContex, _Consumer$contextType;

  var contextProp = '__create-react-context-' + (0, _gud2.default)() + '__';

  var Provider = function (_Component) {
    _inherits(Provider, _Component);

    function Provider() {
      var _temp, _this, _ret;

      _classCallCheck(this, Provider);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = _possibleConstructorReturn(this, _Component.call.apply(_Component, [this].concat(args))), _this), _this.emitter = createEventEmitter(_this.props.value), _temp), _possibleConstructorReturn(_this, _ret);
    }

    Provider.prototype.getChildContext = function getChildContext() {
      var _ref;

      return _ref = {}, _ref[contextProp] = this.emitter, _ref;
    };

    Provider.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
      if (this.props.value !== nextProps.value) {
        var oldValue = this.props.value;
        var newValue = nextProps.value;
        var changedBits = void 0;

        if (objectIs(oldValue, newValue)) {
          changedBits = 0; // No change
        } else {
          changedBits = typeof calculateChangedBits === 'function' ? calculateChangedBits(oldValue, newValue) : MAX_SIGNED_31_BIT_INT;
          if (false) {}

          changedBits |= 0;

          if (changedBits !== 0) {
            this.emitter.set(nextProps.value, changedBits);
          }
        }
      }
    };

    Provider.prototype.render = function render() {
      return _react2.default.createElement(
        _reactDotFragment2.default,
        null,
        this.props.children
      );
    };

    return Provider;
  }(_react.Component);

  Provider.childContextTypes = (_Provider$childContex = {}, _Provider$childContex[contextProp] = _propTypes2.default.object.isRequired, _Provider$childContex);

  var Consumer = function (_Component2) {
    _inherits(Consumer, _Component2);

    function Consumer() {
      var _temp2, _this2, _ret2;

      _classCallCheck(this, Consumer);

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return _ret2 = (_temp2 = (_this2 = _possibleConstructorReturn(this, _Component2.call.apply(_Component2, [this].concat(args))), _this2), _this2.state = {
        value: _this2.getValue()
      }, _this2.onUpdate = function (newValue, changedBits) {
        var observedBits = _this2.observedBits | 0;
        if ((observedBits & changedBits) !== 0) {
          _this2.setState({ value: _this2.getValue() });
        }
      }, _temp2), _possibleConstructorReturn(_this2, _ret2);
    }

    Consumer.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
      var observedBits = nextProps.observedBits;

      this.observedBits = observedBits === undefined || observedBits === null ? MAX_SIGNED_31_BIT_INT // Subscribe to all changes by default
      : observedBits;
    };

    Consumer.prototype.componentDidMount = function componentDidMount() {
      if (this.context[contextProp]) {
        this.context[contextProp].on(this.onUpdate);
      }
      var observedBits = this.props.observedBits;

      this.observedBits = observedBits === undefined || observedBits === null ? MAX_SIGNED_31_BIT_INT // Subscribe to all changes by default
      : observedBits;
    };

    Consumer.prototype.componentWillUnmount = function componentWillUnmount() {
      if (this.context[contextProp]) {
        this.context[contextProp].off(this.onUpdate);
      }
    };

    Consumer.prototype.getValue = function getValue() {
      if (this.context[contextProp]) {
        return this.context[contextProp].get();
      } else {
        return defaultValue;
      }
    };

    Consumer.prototype.render = function render() {
      return _react2.default.createElement(
        _reactDotFragment2.default,
        null,
        onlyChild(this.props.children)(this.state.value)
      );
    };

    return Consumer;
  }(_react.Component);

  Consumer.contextTypes = (_Consumer$contextType = {}, _Consumer$contextType[contextProp] = _propTypes2.default.object, _Consumer$contextType);


  return {
    Provider: Provider,
    Consumer: Consumer
  };
}

exports.default = _react2.default.createContext || createReactContext;
module.exports = exports['default'];

/***/ })
/******/ ])["default"];
});
//# sourceMappingURL=audioplayer.js.map