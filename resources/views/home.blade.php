<!doctype html>
<html lang="{{ app()->getLocale() }}"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">  
        @if(isset($meta) && !empty($meta))
            @foreach ($meta as $key => $me)
                <meta property="{{ $key }}" content="{{ $me }}" />  
            @endforeach
        @else
                <meta property="fb:app_id" content="251127288963211" />  
                <meta property="og:type" content="website" />  
                <meta property="og:url" content="{{env("APP_URL")}}" />  
                <meta property="og:title" content="{{ env("APP_NAME") }}" />  
                <meta property="og:image" content="{{ env("APP_URL")."/images/forestbridge.jpg" }}" /> 
                <meta property="og:description" content="Atrask kažką naujo" /> 
                <meta property="twitter:title" content="{{ env("APP_NAME") }}" />  
                <meta property="twitter:image" content="{{ env("APP_URL")."/images/forestbridge.jpg" }}" />  
                <meta property="twitter:description" content="Atrask kažką naujo" /> 
        @endif
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <title>{{ config('app.name') }}</title>
        @if(env("APP_DEBUG") !== true)
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119778911-1"></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-119778911-1');
            </script>
        @endif

    </head>
    <body>
        <div id='app'></div>  
        {{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
        <!-- IE11 fix -->
        <script src="{{ asset('js/bluebird.min.js') }}"></script> 
        <script src="{{ asset('js/ie.js') }}"></script>
        <!-- IE11 fix -->
        <script src="{{ asset('js/app.js') }}"></script>
 
    </body>
</html>
