<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/audioplayer.css">
<style>
body, html {
    height: 100%;
    margin: 0;
}

.bgimg {
    background-image: url('/forestbridge.jpg');
    height: 100%;
    background-position: center;
    background-size: cover;
    position: relative;
    color: white;
    font-family: "Courier New", Courier, monospace;
    font-size: 25px;
}

.topleft {
    position: absolute;
    top: 0;
    left: 16px;
}

.bottomleft {
    position: absolute;
    bottom: 0;
    left: 16px;
}

.middle {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-align: center;
}

hr {
    margin: auto;
    width: 40%;
}
a {
    text-decoration: none;
    color: #fff;
    font-size: 14px;
}
.rrap__audio_info_marquee {
    overflow: hidden;
    width: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    flex-direction: row;
    -webkit-box-align: center;
    align-items: center;
    margin-left: 10px;
}
</style>
<title>enDI</title>
<body>

<div class="bgimg">
  <div class="topleft">
    <h1>enDI</h1>
  </div>
  <div class="middle">
    <h2>Jau greitai</h2>
    <!-- <h3 id="radio"></h3> -->
    <hr>
    <p id="countdown" style="font-size:30px">- d - h - m - s</p>
  </div>
  <div class="bottomleft">
    <!-- <p>Some text</p> -->
  </div>
</div>

<div id="audio_player_container"></div>  

<script src="https://unpkg.com/react@16.3.2/umd/react.production.min.js"></script> 
<script src="https://unpkg.com/react-dom@16.3.2/umd/react-dom.production.min.js"></script>
<script src="https://unpkg.com/prop-types/prop-types.js"></script>
<script src="https://unpkg.com/resize-observer-polyfill"></script>
<script type="module" src="js/esm/audioplayer.js"></script>
<script nomodule src="js/es5/audioplayer.js"></script>
<script>
// Set the date we're counting down to
var countDownDate = new Date("May 24, 2018 00:00:00").getTime();

// Update the count down every 1 second
var countdownfunction = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)                                                                                                                                                             );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById("countdown").innerHTML = days + " d " + hours + " h                                                                                                                                                              "
    + minutes + " m " + seconds + " s ";

    // If the count down is over, write some text
    if (distance < 0) {
        clearInterval(countdownfunction);
        document.getElementById("countdown").innerHTML = " ";
    }
}, 1000);

// Implement Radio 
        window.onload = function() {
          var playlist = [ 
            {
              sources: [
                {src:'http://radio.audiomastering.lt:8000/live', type:'audio/mpeg'},
                {src:'http://radio.audiomastering.lt:8000/ogg', type:'audio/ogg'}
              ],
              title: '♪♫•*¨*•.¸¸♫♪ radio.audiomastering.lt ♪♫•*¨*•.¸¸♫♪',
              web: 'http://radio.audiomastering.lt'
            },
            {
              sources: [
                {src:'http://82.135.234.195:8000/opus3.mp3', type:'audio/mpeg'},
                {src:'http://82.135.234.195:8000/opus3.aac', type:'audio/mpeg'}
              ],
              title: '♪♫•*¨*•.¸¸♫♪ LRT Opus ♪♫•*¨*•.¸¸♫♪',
              web: 'http://lrt.lt/mediateka/tiesiogiai/lrt-opus'
            }
          ]; 
          var muted = false;
          var control; 
          if(playlist.length > 1) control = [ 
                  'playpause', 
                  'volume',  
                  'spacer',
                  'forwardskip',
                  'spacer',
                  // 'progress'
                  'progressdisplay'
          ]; else control = [ 
                  'playpause', 
                  'volume',   
                  'spacer',
                  'spacer',
                  // 'progress'
                  'progressdisplay'
          ];

          ReactDOM.render(
            React.createElement(React.StrictMode, {},
              React.createElement(AudioPlayer, {
                playlist: playlist,
                style: { position: 'fixed', bottom: 0, width: '100%' },
                autoplay: false, 
                controls: control, 
                // defaultMuted: muted,
                defaultVolume: 0.5,
                defaultShuffle: false, 
                onMediaEvent: {
                  volumechange: function (e) {
                    if (muted !== e.target.muted) {
                      muted = e.target.muted;
                      console.log(muted ? 'muted!' : 'unmuted!');
                    }
                    clearTimeout(muteMessageTimeout);
                    muteMessage.classList.add('hidden');
                  }
                }
              })
            ),
            document.getElementById('audio_player_container')
          );
        }  
</script>
</body>
</html>
