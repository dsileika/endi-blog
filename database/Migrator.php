<?php 
namespace App\Database;

use \Schema;
use \DB;
use App\User; 
use App\Models\Posts; 
use App\Models\Category; 

class Migrator {
    
    public function migrate()
    {
        $this->migrateUsers(); 
        $this->migrateResetPassword(); 
        $this->migratePost(); 
        $this->migrateCategory(); 
        $this->migratePostComment(); 
        $this->migrateVoter();  
    }

    /**
    * Sukuriama vartotojų lentelė
    */
    public function migrateUsers(){
        if (!Schema::hasTable('users')){
            Schema::create('users', function ($table) {
                $table->increments('id')->unsigned()->index();
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password');
                $table->tinyInteger('isActive')->default(1)->comment = "0 - Neaktyvus, 1 - Aktyvus";
                $table->tinyInteger('isAdmin')->default(0)->comment = "0 - Vartotojas, 1 - Admin";
                // add this to your users_table migration
	            // $table->string('api_token', 60)->unique()->nullable();
                $table->rememberToken();
                $table->timestamps();
            }); 
        }

        // $user = new \App\User;
        /**
        * Sukurimas system vartotojas
        */
        // if(!$user->where('email', 'LIKE', 'system@endi.lt')->first()){
        //     $user->name = ucfirst(strtolower('system')); 
        //     $user->password = \Hash::make(str_random(60));
        //     $user->email = 'system@endi.lt'; 
        //     $user->isAdmin = 1;
        //     $user->isActive = 1;
        //     $user->api_token = str_random(60); 
        //     $user->save();
        // }
        
    }

    public function migrateResetPassword(){
        if (!Schema::hasTable('password_resets')) 
            Schema::create('password_resets', function ($table) {
                $table->string('email')->index();
                $table->string('token');
                $table->timestamp('created_at')->nullable();
            });
        
    }

    public function migratePost(){
        
        // if (Schema::hasTable('post'))  exit("HI");
        // print_r(Posts::hasTable());

        if (!Schema::hasTable('post')) 
            Schema::create('post', function ($table) {
                $table->increments('id')->unsigned();
                $table->integer('author_id')->unsigned();
                $table->integer('category_id')->unsigned();
                $table->index(['id','author_id','category_id']);
                $table->dropPrimary( 'id' );
                $table->primary(['id','author_id','category_id']);
                
                // $table->foreign('author_id')
                // ->references('id')->on('users')
                // ->onDelete('cascade');
                // $table->foreign('category_id')
                // ->references('id')->on('category');

                $table->string('title');
                $table->text('note')->nullable();
                $table->string('bgColor')->default('#6c757d');
                $table->text('bgImage')->nullable();
                $table->text('content');
                $table->tinyInteger('isPublic')->default(0);
                $table->integer('votes')->default(0);
                $table->timestamps();
            });
    }

    public function migrateCategory(){
        if (!Schema::hasTable('category')) 
            Schema::create('category', function ($table) {
                $table->increments('id')->unsigned()->index();
                $table->string('title');
                $table->text('description')->nullable();
                $table->string('bgColor')->default('#6c757d');
                $table->text('bgImage')->nullable();
                $table->timestamps();
            });
         /**
        * Creating default categorys
        */
        
        $cat_list = [
            ['id' => 1,'title' => 'Ne į temą', 'dec' => ""],
            ['id' => 2,'title' => 'Matematika', 'dec' => ""],
            ['id' => 3,'title' => 'Kelionės', 'dec' => ""],
            ['id' => 4,'title' => 'Programavimas', 'dec' => ""],
            ['id' => 5,'title' => 'Mokslas', 'dec' => ""],
            ['id' => 6,'title' => 'Ekonomika', 'dec' => ""],
            ['id' => 7,'title' => 'Technologijos', 'dec' => ""],
        ];
        foreach($cat_list as $key => $cat){
            if(!Category::where('title', '=', $cat['title'])->exists()){ 
                    $category = new Category;
                    $category->id = $cat['id']; 
                    $category->title = $cat['title']; 
                    $category->description = $cat['dec']; 
                    $category->save();
            }
        }
 
    } 

    public function migratePostComment(){
        if (!Schema::hasTable('post_comment')) 
            Schema::create('post_comment', function ($table) {
                $table->increments('id')->unsigned();
                $table->integer('post_id')->unsigned();
                $table->index(['id','post_id']);
                $table->dropPrimary( 'id' );
                $table->primary(['id','post_id']);
                $table->foreign('post_id')
                ->references('id')->on('post')
                ->onDelete('cascade');
                $table->text('comment');
                $table->tinyInteger('isVerify')->default(0);
                $table->integer('votes')->default(0);
                $table->timestamps();
            });
    }

    public function migrateVoter(){
        if (!Schema::hasTable('post_voter')) 
            Schema::create('post_voter', function ($table) {
                $table->increments('user_id')->unsigned();
                $table->integer('post_id')->unsigned();
                $table->index(['user_id','post_id']);
                $table->dropPrimary( 'user_id' );
                $table->primary(['user_id','post_id']);
                $table->foreign('post_id')
                ->references('id')->on('post')
                ->onDelete('cascade'); 
                $table->timestamps();
            });
            if (!Schema::hasTable('post_comment_voter')) 
            Schema::create('post_comment_voter', function ($table) {
                $table->increments('user_id')->unsigned();
                $table->integer('comment_id')->unsigned();
                $table->index(['user_id','comment_id']);
                $table->dropPrimary( 'user_id' );
                $table->primary(['user_id','comment_id']);
                $table->foreign('comment_id')
                ->references('id')->on('post_comment')
                ->onDelete('cascade'); 
                $table->timestamps();
            });
    }
}
